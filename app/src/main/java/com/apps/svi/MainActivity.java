package com.apps.svi;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.apps.svi.Fragment.ListNasabahFragment;
import com.apps.svi.Fragment.ListSiteVisitFragment;
import com.apps.svi.Model.DataNasabah;
import com.apps.svi.Model.NasabahEntry;
import com.apps.svi.Model.SearchNasabahResponse;
import com.apps.svi.Rest.ApiConfig;
import com.apps.svi.Rest.AppConfig;
import com.apps.svi.Utils.Method;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.io.IOException;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    EditText etCifNo, etNama, etKTP, etDOB, etNPWP;

    TextView tvDOB;

    public ApiConfig restAPI;

    ProgressBar progressBar;

    View llSearch;

    private static final String TAG = "MainActivity";

    public static final int DIALOG_QUEST_CODE = 101;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initToolbar(getString(R.string.app_name));

        initView();

        restAPI =  AppConfig.getRetrofit(this).create(ApiConfig.class);
    }

    void initToolbar(String title) {
        TextView tv = findViewById(R.id.tvTitle);
        tv.setText(title);
    }

    void initView(){
        etCifNo = findViewById(R.id.etCIF);
        etNama = findViewById(R.id.etNama);
        etKTP = findViewById(R.id.etKTP);
        etDOB = findViewById(R.id.etDOB);
        etNPWP = findViewById(R.id.etNPWP);

        tvDOB = findViewById(R.id.tvDOB);

        llSearch = findViewById(R.id.llSearch);
        progressBar = findViewById(R.id.d_progress);
    }

    public void searchNasabah(View v){
        Log.i(TAG, "searchNasabah:1C |"+etCifNo.getText().toString().equals("")+"|");
        Log.i(TAG, "searchNasabah:1N |"+etNama.getText().toString().equals("")+"|");
        Log.i(TAG, "searchNasabah:2 |"+etNama.getText().toString()+"|");
        Log.i(TAG, "searchNasabah:3 |"+etKTP.getText().toString()+"|");
        Log.i(TAG, "searchNasabah:4 |"+etDOB.getText().toString()+"|");
        Log.i(TAG, "searchNasabah:5 |"+etNPWP.getText().toString()+"|");
        if (etCifNo.getText().toString().equals("") && etNama.getText().toString().equals("") &&
                etKTP.getText().toString().equals("") && etDOB.getText().toString().equals("") &&
                etNPWP.getText().toString().equals("") ){
            DoDialog("Mohon input salah satu data. . .");
        } else {
            showProgress(true);
            NasabahEntry nasabahEntry = new NasabahEntry(etCifNo.getText().toString(), etNama.getText().toString(),
                    etKTP.getText().toString(), etDOB.getText().toString(), etNPWP.getText().toString());
            restAPI.searchNasabah(nasabahEntry).enqueue(new Callback<SearchNasabahResponse>() {
                @Override
                public void onResponse(Call<SearchNasabahResponse> call, Response<SearchNasabahResponse> response) {
                    Log.i(TAG, "onResponse: H " + response.headers().toString());
                    Log.i(TAG, "onResponse: M " + response.message());
                    if (response.code() == 400) {
                        Gson gson = new GsonBuilder().create();
                        try {
                            SearchNasabahResponse  mError = gson.fromJson(response.errorBody().string(), SearchNasabahResponse.class);
                            if (mError.getResponseText() != null) {
                                if (mError.getResponseText().contains("ID CARD NUMBER")){
                                    DoDialog("No. KTP sudah didaftarkan sebelumnya");
                                } else
                                    DoDialog(mError.getResponseText());
                            } else
                                DoDialog("Koneksi bermasalah, silahkan coba lagi");
                            Log.v("Error code 400",response.errorBody().string());
                        } catch (IOException e) {
                            e.printStackTrace();
                            showProgress(false);
                        }
                    } else if (response.code() == 200) {
                        if (response.body() != null) {
                            Log.i(TAG, "onResponse: B " + response.body().toString());
                            if (response.body().getResponse().equals("1")) {
//                                DoDialog(response.body().getResponseText()+"\n"+response.body().getDataNasabah().get(0).getNAMAPEMOHON());
                                if (response.body().getDataNasabah() != null && response.body().getDataNasabah().size() > 0){
                                    showDialogFullscreen(response.body().getDataNasabah());
                                } else
                                    DoDialog(response.body().getResponseText());
                            } else {
                                DoDialog(response.body().getResponseText());
                            }
                        } else
                            DoDialog("Koneksi bermasalah, silahkan coba lagi");
                    } else {
                        showProgress(false);
                    }
                }

                @Override
                public void onFailure(Call<SearchNasabahResponse> call, Throwable t) {
                    Log.e(TAG, "onFailure: " + t.getMessage());
                    DoDialog("Tidak terhubung dengan internet, silahkan coba lagi.");
                    showProgress(false);
                }
            });
        }
    }

    private void showDialogFullscreen(List<DataNasabah> dataNasabah) {
        showProgress(false);
        FragmentManager fragmentManager = getSupportFragmentManager();
        ListNasabahFragment newFragment = new ListNasabahFragment(dataNasabah);
//        newFragment.setRequestCode(DIALOG_QUEST_CODE);
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.add(android.R.id.content, newFragment).addToBackStack(null).commit();

    }

    public void DoDialog(String str) {
        showProgress(false);
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.app_name));
        builder.setMessage(str);
        builder.setCancelable(false);
        builder.setPositiveButton("OK", new
                DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                }).show();
    }

    void showProgress(boolean shows){
        llSearch.setVisibility(shows ? View.GONE : View.VISIBLE);
        progressBar.setVisibility(shows ? View.VISIBLE : View.GONE);
    }

    public void dialogDatePickerLight(View v) {
        Calendar cur_calender = Calendar.getInstance();
        cur_calender.set(Calendar.YEAR, 1960);
        DatePickerDialog datePicker = DatePickerDialog.newInstance(
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                        Calendar calendar = Calendar.getInstance();
                        calendar.set(Calendar.YEAR, year);
                        calendar.set(Calendar.MONTH, monthOfYear);
                        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        long date_ship_millis = calendar.getTimeInMillis();
                        etDOB.setText(Method.getFormattedDate(date_ship_millis));
                        tvDOB.setText(Method.getFormattedDateShow(date_ship_millis));
                    }
                },
                cur_calender.get(Calendar.YEAR),
                cur_calender.get(Calendar.MONTH),
                cur_calender.get(Calendar.DAY_OF_MONTH)
        );
        //set dark light
        datePicker.setThemeDark(false);
        datePicker.setAccentColor(getResources().getColor(R.color.colorPrimary));
        datePicker.show(getFragmentManager(), "Datepickerdialog");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.i(TAG, "onActivityResult: req "+requestCode);
        Log.i(TAG, "onActivityResult: res "+resultCode);
        if (requestCode == 10){
            if (resultCode == RESULT_OK){
//                ListSiteVisitFragment fragment = (ListSiteVisitFragment) getFragmentManager().findFragmentById(android.R.id.content);
                FragmentManager manager = getSupportFragmentManager();
                ListSiteVisitFragment fragments = (ListSiteVisitFragment)manager.findFragmentByTag("LIST");
                fragments.getSiteVisit(fragments.dataNasabah.getCUREF());
            }
        }
    }
}
