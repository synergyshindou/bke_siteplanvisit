package com.apps.svi;

import android.Manifest;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.apps.svi.Model.CompanySiteVisitEntry;
import com.apps.svi.Model.DataApplication;
import com.apps.svi.Model.DataHome;
import com.apps.svi.Model.GlobalResponse;
import com.apps.svi.Model.HomeStayResponse;
import com.apps.svi.Model.PersonnalSiteVisitEntry;
import com.apps.svi.Model.SearchApplicationResponse;
import com.apps.svi.Model.UploadImageEntry;
import com.apps.svi.Rest.ApiConfig;
import com.apps.svi.Rest.AppConfig;
import com.apps.svi.Rest.ProgressRequestBody;
import com.apps.svi.Utils.Method;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.livinglifetechway.quickpermissions.annotations.WithPermissions;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import fr.ganfra.materialspinner.MaterialSpinner;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FormVisitActivity extends AppCompatActivity {

    private static final String TAG = "FormVisitActivity";

    public ApiConfig restAPI;

    MaterialSpinner spinAppReg, spinAppReg2, spinHome;

    List<DataApplication> dataApplications;
    List<DataHome> dataHomes;

    TextView tvCUREF, tvDate, tvCUREF2, tvDate2;

    EditText etDATE, etNameWork, etYearWork, etMarginWork, etNamaPemberi;

    String idAPPREG = "", idHOME = "", sType = "", idCUREF = "";

    boolean isImage = false;

    View llPerusahaan, llPerorangan;

    android.app.AlertDialog b;

    android.app.AlertDialog.Builder dialogBuilder;

    Button btnApply, btnApply2;

    private final int PICK_IMAGE_REQUEST = 101;

    ImageView imgUp, imgUps;

    byte[] byteArray;

    Bitmap myBitmap;

    TextView tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_visit);
        sType = getIntent().getStringExtra("TYPE");
        idCUREF = getIntent().getStringExtra("CUREF");
        Log.w(TAG, "onCreate: TYPE " + sType);

        initToolbar("Form Visit");
        initView();

        restAPI = AppConfig.getRetrofit(this).create(ApiConfig.class);
        getDataApplication();
        if (sType.equals("02"))
            getHome();
    }

    void initToolbar(String title) {
        TextView tv = findViewById(R.id.tvTitle);
        tv.setText(title);
    }

    void initView() {

        llPerusahaan = findViewById(R.id.llFormPerusahaan);
        llPerorangan = findViewById(R.id.llFormPerorangan);

        tvCUREF = findViewById(R.id.tvCUREF);
        tvCUREF2 = findViewById(R.id.tvCUREF2);
        tvDate = findViewById(R.id.tvDOB);
        tvDate2 = findViewById(R.id.tvDOB2);
        etDATE = findViewById(R.id.etDOB);
        etNameWork = findViewById(R.id.etNameWork);
        etYearWork = findViewById(R.id.etYearWork);
        etMarginWork = findViewById(R.id.etMargin);
        etNamaPemberi = findViewById(R.id.etNameBer);

        imgUp = findViewById(R.id.imgUp);
        imgUps = findViewById(R.id.imgUps);

        tvDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogDatePickerLight();
            }
        });

        tvDate2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogDatePickerLight();
            }
        });

        tvCUREF.setText(getIntent().getStringExtra("CUREF"));
        tvCUREF2.setText(getIntent().getStringExtra("CUREF"));

        spinAppReg = findViewById(R.id.spinAppReg);
        spinAppReg.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (dataApplications.size() > 0 && position >= 0) {
                    idAPPREG = dataApplications.get(position).getAPREGNO();
                    spinAppReg.setEnableErrorLabel(false);
                } else {
                    idAPPREG = "";
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinAppReg2 = findViewById(R.id.spinAppReg2);
        spinAppReg2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (dataApplications.size() > 0 && position >= 0) {
                    idAPPREG = dataApplications.get(position).getAPREGNO();
                    spinAppReg2.setEnableErrorLabel(false);
                } else {
                    idAPPREG = "";
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinHome = findViewById(R.id.spinHome);
        spinHome.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (dataHomes.size() > 0 && position >= 0) {
                    idHOME = String.valueOf(dataHomes.get(position).getHMCODE());
                    spinHome.setEnableErrorLabel(false);
                } else {
                    idHOME = "";
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btnApply = findViewById(R.id.btnApply);
        btnApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doApply();
            }
        });

        btnApply2 = findViewById(R.id.btnApply2);
        btnApply2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doApply();
            }
        });

        Calendar calendar = Calendar.getInstance();
        long date_ship_millis = calendar.getTimeInMillis();
        etDATE.setText(Method.getFormattedDate(date_ship_millis));

        if (sType.equals("01")) {
            llPerusahaan.setVisibility(View.VISIBLE);
            tvDate.setText(Method.getFormattedDateShow(date_ship_millis));
        } else {
            llPerorangan.setVisibility(View.VISIBLE);
            tvDate2.setText(Method.getFormattedDateShow(date_ship_millis));
        }
    }

    void getDataApplication() {
        restAPI.searchApplication(getIntent().getStringExtra("CUREF")).enqueue(new Callback<SearchApplicationResponse>() {
            @Override
            public void onResponse(Call<SearchApplicationResponse> call, Response<SearchApplicationResponse> response) {
                Log.i(TAG, "onResponse: H " + response.headers().toString());
                Log.i(TAG, "onResponse: M " + response.message());
                if (response.body() != null) {
                    Log.i(TAG, "onResponse: B " + response.body().toString());
                    if (response.body().getDataApplication() != null) {
                        if (response.body().getDataApplication().size() > 0) {

                            dataApplications = response.body().getDataApplication();
                            ArrayList<String> list = new ArrayList<>();

                            for (int i = 0; i < dataApplications.size(); i++) {
                                list.add(dataApplications.get(i).getAPREGNO());
                            }

                            if (sType.equals("01")) {
                                ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(FormVisitActivity.this,
                                        android.R.layout.simple_spinner_item, list);
                                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spinAppReg.setAdapter(dataAdapter);
                            } else {

                                ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(FormVisitActivity.this,
                                        android.R.layout.simple_spinner_item, list);
                                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spinAppReg2.setAdapter(dataAdapter);
                            }

                        }
                    } else {
                        DoDialog(response.body().getResponseText());
                    }
                }
            }

            @Override
            public void onFailure(Call<SearchApplicationResponse> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                if (t.getMessage().contains("timeout") || t.getMessage().contains("timed out"))
                    getDataApplication();
            }
        });
    }

    void getHome() {
        restAPI.getHomestay().enqueue(new Callback<HomeStayResponse>() {
            @Override
            public void onResponse(Call<HomeStayResponse> call, Response<HomeStayResponse> response) {
                Log.i(TAG, "onResponse: H " + response.headers().toString());
                Log.i(TAG, "onResponse: M " + response.message());
                if (response.body() != null) {
                    Log.i(TAG, "onResponse: B " + response.body().toString());
                    if (response.body().getDataHome() != null) {
                        if (response.body().getDataHome().size() > 0) {

                            dataHomes = response.body().getDataHome();
                            ArrayList<String> list = new ArrayList<>();

                            for (int i = 0; i < dataHomes.size(); i++) {
                                list.add(dataHomes.get(i).getHMDESC());
                            }

                            ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(FormVisitActivity.this,
                                    android.R.layout.simple_spinner_item, list);
                            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spinHome.setAdapter(dataAdapter);
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<HomeStayResponse> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                if (t != null)
                    if (t.getMessage().contains("timeout") || t.getMessage().contains("timed out"))
                        getHome();
            }
        });
    }

    void doApply() {
        if (sType.equals("01")) {
            if (idAPPREG.equals("")) {
                spinAppReg.setEnableErrorLabel(true);
                spinAppReg.setError("Mohon dipilih!");
            } else if (etNameWork.getText().toString().equals("")) {
                etNameWork.setText("Mohon diisi!");
                etNameWork.requestFocus();

            } else if (etYearWork.getText().toString().equals("")) {
                etYearWork.setText("Mohon diisi!");
                etYearWork.requestFocus();

            } else if (etMarginWork.getText().toString().equals("")) {
                etMarginWork.setText("Mohon diisi!");
                etMarginWork.requestFocus();
            } else if (!isImage) {
                DoDialogC("Mohon lampirkan gambar!");
            } else {
                ShowProgressDialog("Applying Data. . .");
                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        // This method will be executed once the timer is over
                        // Start your app main activity
                        uploadImage();
                    }
                }, 500);
            }
        } else {
            if (idAPPREG.equals("")) {
                spinAppReg2.setEnableErrorLabel(true);
                spinAppReg2.setError("Mohon dipilih!");
            } else if (etNamaPemberi.getText().toString().equals("")) {
                etNamaPemberi.setText("Mohon diisi!");
                etNamaPemberi.requestFocus();

            } else if (idHOME.equals("")) {
                spinHome.setEnableErrorLabel(true);
                spinHome.setError("Mohon dipilih!");
            } else if (!isImage) {
                DoDialogC("Mohon lampirkan gambar!");
            } else {
                ShowProgressDialog("Applying Data. . .");
                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        // This method will be executed once the timer is over
                        // Start your app main activity
                        uploadImage();
                    }
                }, 500);

            }
        }
    }

    void uploadImage() {
        if (sType.equals("01")) {
            BitmapDrawable drawable = (BitmapDrawable) imgUps.getDrawable();
            myBitmap = drawable.getBitmap();
        } else {
            BitmapDrawable drawable = (BitmapDrawable) imgUp.getDrawable();
            myBitmap = drawable.getBitmap();
        }

//        RequestBody appreg = RequestBody.create(MediaType.parse("multipart/form-data"), idAPPREG);
//        RequestBody curef = RequestBody.create(MediaType.parse("multipart/form-data"), idCUREF);
//
//        File filei = createTempFile(myBitmap, idAPPREG, idCUREF);
//        ProgressRequestBody fileBodyi = new ProgressRequestBody(filei, new ProgressRequestBody.UploadCallbacks() {
//            @Override
//            public void onProgressUpdate(int percentage) {
//                tv.setText(percentage + "%");
//
//            }
//
//            @Override
//            public void onError() {
//                tv.setText("Uploaded Failed!");
//
//            }
//
//            @Override
//            public void onFinish() {
//                tv.setText("Uploaded Successfully");
//
//            }
//
//            @Override
//            public void uploadStart() {
//                tv.setText("0%");
//            }
//        });
//        MultipartBody.Part fileParti = MultipartBody.Part.createFormData("FILES", filei.getName(), fileBodyi);

//        restAPI.uploadImage(appreg, curef, fileParti).enqueue(new Callback<GlobalResponse>() {
        UploadImageEntry entry = new UploadImageEntry();
        entry.setAP_REGNO(idAPPREG);
        entry.setBASE64FILES(b64(myBitmap));
        entry.setNAMEFILE("FILE_PHOTO");
        entry.setFU_USERID("");

        restAPI.uploadImageb64(entry).enqueue(new Callback<GlobalResponse>() {
            @Override
            public void onResponse(Call<GlobalResponse> call, Response<GlobalResponse> response) {
                Log.i(TAG, "onResponse: H " + response.headers().toString());
                Log.i(TAG, "onResponse: M " + response.message().toString());
                HideProgressDialog();
                if (response.code() == 400) {
                    Gson gson = new GsonBuilder().create();
                    try {
                        GlobalResponse mError = gson.fromJson(response.errorBody().string(), GlobalResponse.class);
                        if (mError.getResponseText() != null) {
                            DoDialogC(mError.getResponseText());
                        } else
                            DoDialogC("Koneksi bermasalah, silahkan coba lagi");
                        Log.v("Error code 400", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else if (response.code() == 200) {
                    if (response.body() != null) {
                        Log.i(TAG, "onResponse: B " + response.body().toString());
                        if (response.body().getResponse().equals("1")) {
//                            DoDialogC(response.body().getResponseText());
                            if (sType.equals("01"))
                                uploadDataPerusahaan();
                            else
                                uploadDataOrang();
                        } else {
                            DoDialogC(response.body().getResponseText());
                        }
                    } else
                        DoDialogC("Koneksi bermasalah, silahkan coba lagi");
                } else
                    DoDialogC("Koneksi bermasalah, silahkan coba lagi");
            }

            @Override
            public void onFailure(Call<GlobalResponse> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                DoDialogC("Tidak terhubung dengan internet, silahkan coba lagi.");
                HideProgressDialog();

            }
        });
    }

    void uploadDataOrang() {
        PersonnalSiteVisitEntry p = new PersonnalSiteVisitEntry();
        p.setAP_REGNO(idAPPREG);
        p.setCU_REF(idCUREF);
        p.setSV_DATE(etDATE.getText().toString());
        p.setSV_HS_PEMBERI_KET1(etNamaPemberi.getText().toString());
        p.setSV_HS_STS_HOME_STAY(idHOME);
        restAPI.applyPersonnal(p).enqueue(new Callback<GlobalResponse>() {
            @Override
            public void onResponse(Call<GlobalResponse> call, Response<GlobalResponse> response) {
                Log.i(TAG, "onResponse: H " + response.headers().toString());
                Log.i(TAG, "onResponse: M " + response.message());
                HideProgressDialog();
                if (response.code() == 400) {
                    Gson gson = new GsonBuilder().create();
                    try {
                        GlobalResponse mError = gson.fromJson(response.errorBody().string(), GlobalResponse.class);
                        if (mError.getResponseText() != null) {
                            DoDialogC(mError.getResponseText());
                        } else
                            DoDialogC("Koneksi bermasalah, silahkan coba lagi");
                        Log.v("Error code 400", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else if (response.code() == 200) {
                    if (response.body() != null) {
                        Log.i(TAG, "onResponse: B " + response.body().toString());
                        if (response.body().getResponse().equals("1")) {
                            DoDialog(response.body().getResponseText());
                        } else {
                            DoDialogC(response.body().getResponseText());
                        }
                    } else
                        DoDialogC("Koneksi bermasalah, silahkan coba lagi");
                } else
                    DoDialogC("Koneksi bermasalah, silahkan coba lagi");
            }

            @Override
            public void onFailure(Call<GlobalResponse> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                DoDialogC("Tidak terhubung dengan internet, silahkan coba lagi.");
                HideProgressDialog();
            }
        });
    }

    void uploadDataPerusahaan() {
        CompanySiteVisitEntry c = new CompanySiteVisitEntry();
        c.setAP_REGNO(idAPPREG);
        c.setCU_REF(idCUREF);
        c.setSV_DATE(etDATE.getText().toString());
        c.setSV_OFC_NAME_WORK(etNameWork.getText().toString());
        c.setSV_OFC_YEAR_WORK(etYearWork.getText().toString());
        c.setSV_OFC_INCOME_MARGIN(Double.parseDouble(etMarginWork.getText().toString()));
        restAPI.applyCompany(c).enqueue(new Callback<GlobalResponse>() {
            @Override
            public void onResponse(Call<GlobalResponse> call, Response<GlobalResponse> response) {
                Log.i(TAG, "onResponse: H " + response.headers().toString());
                Log.i(TAG, "onResponse: M " + response.message());
                HideProgressDialog();
                if (response.code() == 400) {
                    Gson gson = new GsonBuilder().create();
                    try {
                        GlobalResponse mError = gson.fromJson(response.errorBody().string(), GlobalResponse.class);
                        if (mError.getResponseText() != null) {
                            DoDialogC(mError.getResponseText());
                        } else
                            DoDialogC("Koneksi bermasalah, silahkan coba lagi");
                        Log.v("Error code 400", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else if (response.code() == 200) {
                    if (response.body() != null) {
                        Log.i(TAG, "onResponse: B " + response.body().toString());
                        if (response.body().getResponse().equals("1")) {
                            DoDialog(response.body().getResponseText());
                        } else {
                            DoDialogC(response.body().getResponseText());
                        }
                    } else
                        DoDialogC("Koneksi bermasalah, silahkan coba lagi");
                } else
                    DoDialogC("Koneksi bermasalah, silahkan coba lagi");
            }

            @Override
            public void onFailure(Call<GlobalResponse> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                DoDialogC("Tidak terhubung dengan internet, silahkan coba lagi.");
                HideProgressDialog();
            }
        });
    }

    @WithPermissions(
            permissions = {Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE}
    )
    public void methodWithPermissions() {

        startActivityForResult(getPickImageChooserIntent(), PICK_IMAGE_REQUEST);
    }

    public void chooseImage(View v) {
        methodWithPermissions();
    }

    public Intent getPickImageChooserIntent() {

        Uri outputFileUri = getCaptureImageOutputUri();

        List<Intent> allIntents = new ArrayList<>();
        PackageManager packageManager = getPackageManager();

        Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            if (outputFileUri != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            }
            allIntents.add(intent);
        }

        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        List<ResolveInfo> listGallery = packageManager.queryIntentActivities(galleryIntent, 0);
        for (ResolveInfo res : listGallery) {
            Intent intent = new Intent(galleryIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            allIntents.add(intent);
        }

        Intent mainIntent = allIntents.get(allIntents.size() - 1);
        for (Intent intent : allIntents) {
            if (intent.getComponent().getClassName().equals("com.android.documentsui.DocumentsActivity")) {
                mainIntent = intent;
                break;
            }
        }
        allIntents.remove(mainIntent);

        Intent chooserIntent = Intent.createChooser(mainIntent, "Select source");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, allIntents.toArray(new Parcelable[allIntents.size()]));

        return chooserIntent;
    }

    private String getImageFromFilePath(Intent data) {
        boolean isCamera = data == null || data.getData() == null;

        if (isCamera) return getCaptureImageOutputUri().getPath();
        else return getPathFromURI(data.getData());

    }

    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        File getImage = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getPath(), "profile.png"));
        }
        return outputFileUri;
    }

    public String getImageFilePath(Intent data) {
        return getImageFromFilePath(data);
    }

    private String getPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Audio.Media.DATA};
        Cursor cursor = getContentResolver().query(contentUri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK) {
            String filePath = getImageFilePath(data);
            if (filePath != null) {
                Bitmap mBitmap;
                mBitmap = BitmapFactory.decodeFile(filePath);
                getByteArrayInBackground(mBitmap);

                Log.i(TAG, "onActivityResult: type " + sType);
                if (sType.equals("01")) {
                    setImage(mBitmap, imgUps);
                } else {
                    setImage(mBitmap, imgUp);
                }
                isImage = true;
            }
        }

    }

    public void getByteArrayInBackground(final Bitmap mBitmap) {

        Thread thread = new Thread() {
            @Override
            public void run() {
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                mBitmap.compress(Bitmap.CompressFormat.PNG, 0, bos);
                byteArray = bos.toByteArray();
            }
        };
        thread.start();
    }

    public void setImage(Bitmap bitmap, ImageView imageView) {
        imageView.setImageBitmap(bitmap);
    }

    public void DoDialog(String str) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.app_name));
        builder.setMessage(str);
        builder.setCancelable(false);
        builder.setPositiveButton("OK", new
                DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        doDelete(idAPPREG, idCUREF);
                        Intent returnIntent = new Intent();
                        returnIntent.putExtra("result", "a");

                        setResult(Activity.RESULT_OK, returnIntent);
                        finish();
                    }
                }).show();
    }

    public void DoDialogC(String str) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.app_name));
        builder.setMessage(str);
        builder.setCancelable(false);
        builder.setPositiveButton("OK", new
                DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        doDelete(idAPPREG, idCUREF);
                    }
                }).show();
    }

    public void dialogDatePickerLight() {
        Calendar cur_calender = Calendar.getInstance();
        DatePickerDialog datePicker = DatePickerDialog.newInstance(
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                        Calendar calendar = Calendar.getInstance();
                        calendar.set(Calendar.YEAR, year);
                        calendar.set(Calendar.MONTH, monthOfYear);
                        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        long date_ship_millis = calendar.getTimeInMillis();
                        etDATE.setText(Method.getFormattedDate(date_ship_millis));
                        tvDate.setText(Method.getFormattedDateShow(date_ship_millis));
                        tvDate2.setText(Method.getFormattedDateShow(date_ship_millis));
                    }
                },
                cur_calender.get(Calendar.YEAR),
                cur_calender.get(Calendar.MONTH),
                cur_calender.get(Calendar.DAY_OF_MONTH)
        );
        //set dark light
        datePicker.setThemeDark(false);
        datePicker.setAccentColor(getResources().getColor(R.color.colorPrimary));
        datePicker.show(getFragmentManager(), "Datepickerdialog");
    }

    public void ShowProgressDialog(String text) {
        dialogBuilder = new android.app.AlertDialog.Builder(FormVisitActivity.this);
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialogView = inflater.inflate(R.layout.progress_dialog_layout, null);
        tv = dialogView.findViewById(R.id.textViewDialog);
        tv.setText(text);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);
        b = dialogBuilder.create();
        b.show();
    }

    public void HideProgressDialog() {
        b.dismiss();
    }

    private File createTempFile(Bitmap bitmap, String idappreg, String idcuref) {
        File file = new File(this.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
                , idappreg + idcuref + "_image.jpg");
        Log.w(TAG, "createTempFile: " + file.getAbsolutePath().toString());

//        ByteArrayOutputStream bos = new ByteArrayOutputStream();
//
//        bitmap.compress(Bitmap.CompressFormat.PNG, 0, bos);
//        byte[] bitmapdata = bos.toByteArray();
//        //write the bytes in file

        //convert the decoded bitmap to stream
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        /*compress bitmap into byteArrayOutputStream
            Bitmap.compress(Format, Quality, OutputStream)
            Where Quality ranges from 1 - 100.
         */
        bitmap.compress(Bitmap.CompressFormat.JPEG, 80, byteArrayOutputStream);

        try {
            FileOutputStream fos = new FileOutputStream(file);
//            fos.write(bitmapdata);
            fos.write(byteArrayOutputStream.toByteArray());
            fos.flush();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
            Log.wtf(TAG, "Error occured", e.fillInStackTrace());
        }
        return file;
    }

    public String b64(Bitmap myBitmap){
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 80, bytes);
        byte[] ba = bytes.toByteArray();
        return Base64.encodeToString(ba, Base64.NO_WRAP).replaceAll(" ", "+");
    }

    private void doDelete(String idappreg, String idcuref) {
        File fdelete = new File(this.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
                , idappreg + idcuref + "_image.jpg");
        File fdeletes = new File(this.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
                , "profile.png");
        if (fdelete.exists()) {
            if (fdelete.delete()) {
                System.out.println("file Deleted :" + fdelete.getPath());
            } else {
                System.out.println("file not Deleted :" + fdelete.getPath());
            }
        }
        if (fdeletes.exists()) {
            if (fdeletes.delete()) {
                System.out.println("file Deleteds :" + fdeletes.getPath());
            } else {
                System.out.println("file not Deleteds :" + fdeletes.getPath());
            }
        }
    }
}
