
package com.apps.svi.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class LoginResponse implements Parcelable
{

    @SerializedName("data")
    @Expose
    private List<DataLogin> dataLogin = new ArrayList<DataLogin>();
    @SerializedName("response")
    @Expose
    private String response;
    @SerializedName("responseText")
    @Expose
    private String responseText;
    public final static Creator<LoginResponse> CREATOR = new Creator<LoginResponse>() {


        @SuppressWarnings({
            "unchecked"
        })
        public LoginResponse createFromParcel(Parcel in) {
            LoginResponse instance = new LoginResponse();
            in.readList(instance.dataLogin, (com.apps.svi.Model.DataLogin.class.getClassLoader()));
            instance.response = ((String) in.readValue((String.class.getClassLoader())));
            instance.responseText = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public LoginResponse[] newArray(int size) {
            return (new LoginResponse[size]);
        }

    }
    ;

    /**
     * 
     * @return
     *     The dataLogin
     */
    public List<DataLogin> getDataLogin() {
        return dataLogin;
    }

    /**
     * 
     * @param dataLogin
     *     The dataLogin
     */
    public void setDataLogin(List<DataLogin> dataLogin) {
        this.dataLogin = dataLogin;
    }

    /**
     * 
     * @return
     *     The response
     */
    public String getResponse() {
        return response;
    }

    /**
     * 
     * @param response
     *     The response
     */
    public void setResponse(String response) {
        this.response = response;
    }

    /**
     * 
     * @return
     *     The responseText
     */
    public String getResponseText() {
        return responseText;
    }

    /**
     * 
     * @param responseText
     *     The responseText
     */
    public void setResponseText(String responseText) {
        this.responseText = responseText;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(dataLogin);
        dest.writeValue(response);
        dest.writeValue(responseText);
    }

    public int describeContents() {
        return  0;
    }

}
