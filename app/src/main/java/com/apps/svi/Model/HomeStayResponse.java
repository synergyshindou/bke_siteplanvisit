package com.apps.svi.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class HomeStayResponse implements Parcelable {

    public final static Creator<HomeStayResponse> CREATOR = new Creator<HomeStayResponse>() {


        @SuppressWarnings({
                "unchecked"
        })
        public HomeStayResponse createFromParcel(Parcel in) {
            HomeStayResponse instance = new HomeStayResponse();
            in.readList(instance.dataHome, (com.apps.svi.Model.DataHome.class.getClassLoader()));
            instance.response = ((String) in.readValue((String.class.getClassLoader())));
            instance.responseText = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public HomeStayResponse[] newArray(int size) {
            return (new HomeStayResponse[size]);
        }

    };
    @SerializedName("data")
    @Expose
    private List<DataHome> dataHome = new ArrayList<DataHome>();
    @SerializedName("response")
    @Expose
    private String response;
    @SerializedName("responseText")
    @Expose
    private String responseText;

    /**
     * @return The dataHome
     */
    public List<DataHome> getDataHome() {
        return dataHome;
    }

    /**
     * @param dataHome The dataHome
     */
    public void setDataHome(List<DataHome> dataHome) {
        this.dataHome = dataHome;
    }

    /**
     * @return The response
     */
    public String getResponse() {
        return response;
    }

    /**
     * @param response The response
     */
    public void setResponse(String response) {
        this.response = response;
    }

    /**
     * @return The responseText
     */
    public String getResponseText() {
        return responseText;
    }

    /**
     * @param responseText The responseText
     */
    public void setResponseText(String responseText) {
        this.responseText = responseText;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(dataHome);
        dest.writeValue(response);
        dest.writeValue(responseText);
    }

    public int describeContents() {
        return 0;
    }

}
