package com.apps.svi.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class SearchSiteVisitResponse implements Parcelable {

    public final static Parcelable.Creator<SearchSiteVisitResponse> CREATOR = new Creator<SearchSiteVisitResponse>() {


        @SuppressWarnings({
                "unchecked"
        })
        public SearchSiteVisitResponse createFromParcel(Parcel in) {
            SearchSiteVisitResponse instance = new SearchSiteVisitResponse();
            in.readList(instance.dataSiteVisit, (com.apps.svi.Model.DataSiteVisit.class.getClassLoader()));
            instance.response = ((String) in.readValue((String.class.getClassLoader())));
            instance.responseText = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public SearchSiteVisitResponse[] newArray(int size) {
            return (new SearchSiteVisitResponse[size]);
        }

    };
    @SerializedName("data")
    @Expose
    private List<DataSiteVisit> dataSiteVisit = new ArrayList<DataSiteVisit>();
    @SerializedName("response")
    @Expose
    private String response;
    @SerializedName("responseText")
    @Expose
    private String responseText;

    /**
     * @return The dataSiteVisit
     */
    public List<DataSiteVisit> getDataSiteVisit() {
        return dataSiteVisit;
    }

    /**
     * @param dataSiteVisit The dataSiteVisit
     */
    public void setDataSiteVisit(List<DataSiteVisit> dataSiteVisit) {
        this.dataSiteVisit = dataSiteVisit;
    }

    /**
     * @return The response
     */
    public String getResponse() {
        return response;
    }

    /**
     * @param response The response
     */
    public void setResponse(String response) {
        this.response = response;
    }

    /**
     * @return The responseText
     */
    public String getResponseText() {
        return responseText;
    }

    /**
     * @param responseText The responseText
     */
    public void setResponseText(String responseText) {
        this.responseText = responseText;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(dataSiteVisit);
        dest.writeValue(response);
        dest.writeValue(responseText);
    }

    public int describeContents() {
        return 0;
    }

}
