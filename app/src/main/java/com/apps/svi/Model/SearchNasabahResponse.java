package com.apps.svi.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class SearchNasabahResponse implements Parcelable {

    public final static Parcelable.Creator<SearchNasabahResponse> CREATOR = new Creator<SearchNasabahResponse>() {


        @SuppressWarnings({
                "unchecked"
        })
        public SearchNasabahResponse createFromParcel(Parcel in) {
            SearchNasabahResponse instance = new SearchNasabahResponse();
            in.readList(instance.dataNasabah, (com.apps.svi.Model.DataNasabah.class.getClassLoader()));
            instance.response = ((String) in.readValue((String.class.getClassLoader())));
            instance.responseText = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public SearchNasabahResponse[] newArray(int size) {
            return (new SearchNasabahResponse[size]);
        }

    };
    @SerializedName("data")
    @Expose
    private List<DataNasabah> dataNasabah = new ArrayList<DataNasabah>();
    @SerializedName("response")
    @Expose
    private String response;
    @SerializedName("responseText")
    @Expose
    private String responseText;

    /**
     * @return The dataNasabah
     */
    public List<DataNasabah> getDataNasabah() {
        return dataNasabah;
    }

    /**
     * @param dataNasabah The dataNasabah
     */
    public void setDataNasabah(List<DataNasabah> dataNasabah) {
        this.dataNasabah = dataNasabah;
    }

    /**
     * @return The response
     */
    public String getResponse() {
        return response;
    }

    /**
     * @param response The response
     */
    public void setResponse(String response) {
        this.response = response;
    }

    /**
     * @return The responseText
     */
    public String getResponseText() {
        return responseText;
    }

    /**
     * @param responseText The responseText
     */
    public void setResponseText(String responseText) {
        this.responseText = responseText;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(dataNasabah);
        dest.writeValue(response);
        dest.writeValue(responseText);
    }

    public int describeContents() {
        return 0;
    }

}
