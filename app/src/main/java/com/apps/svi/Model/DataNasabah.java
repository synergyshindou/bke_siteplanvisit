package com.apps.svi.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataNasabah implements Parcelable {

    public final static Parcelable.Creator<DataNasabah> CREATOR = new Creator<DataNasabah>() {


        @SuppressWarnings({
                "unchecked"
        })
        public DataNasabah createFromParcel(Parcel in) {
            DataNasabah instance = new DataNasabah();
            instance.CUREF = ((String) in.readValue((String.class.getClassLoader())));
            instance.CIFNO = ((String) in.readValue((String.class.getClassLoader())));
            instance.NAMAPEMOHON = ((String) in.readValue((String.class.getClassLoader())));
            instance.NPWP = ((String) in.readValue((String.class.getClassLoader())));
            instance.CUSTTYPE = ((String) in.readValue((String.class.getClassLoader())));
            instance.JENISNASABAH = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public DataNasabah[] newArray(int size) {
            return (new DataNasabah[size]);
        }

    };
    @SerializedName("CU_REF")
    @Expose
    private String CUREF;
    @SerializedName("CIF_NO")
    @Expose
    private String CIFNO;
    @SerializedName("NAMAPEMOHON")
    @Expose
    private String NAMAPEMOHON;
    @SerializedName("NPWP")
    @Expose
    private String NPWP;
    @SerializedName("CUSTTYPE")
    @Expose
    private String CUSTTYPE;
    @SerializedName("JENISNASABAH")
    @Expose
    private String JENISNASABAH;

    /**
     * @return The CUREF
     */
    public String getCUREF() {
        return CUREF;
    }

    /**
     * @param CUREF The CU_REF
     */
    public void setCUREF(String CUREF) {
        this.CUREF = CUREF;
    }

    /**
     * @return The CIFNO
     */
    public String getCIFNO() {
        return CIFNO;
    }

    /**
     * @param CIFNO The CIF_NO
     */
    public void setCIFNO(String CIFNO) {
        this.CIFNO = CIFNO;
    }

    /**
     * @return The NAMAPEMOHON
     */
    public String getNAMAPEMOHON() {
        return NAMAPEMOHON;
    }

    /**
     * @param NAMAPEMOHON The NAMAPEMOHON
     */
    public void setNAMAPEMOHON(String NAMAPEMOHON) {
        this.NAMAPEMOHON = NAMAPEMOHON;
    }

    /**
     * @return The NPWP
     */
    public String getNPWP() {
        return NPWP;
    }

    /**
     * @param NPWP The NPWP
     */
    public void setNPWP(String NPWP) {
        this.NPWP = NPWP;
    }

    /**
     * @return The CUSTTYPE
     */
    public String getCUSTTYPE() {
        return CUSTTYPE;
    }

    /**
     * @param CUSTTYPE The CUSTTYPE
     */
    public void setCUSTTYPE(String CUSTTYPE) {
        this.CUSTTYPE = CUSTTYPE;
    }

    /**
     * @return The JENISNASABAH
     */
    public String getJENISNASABAH() {
        return JENISNASABAH;
    }

    /**
     * @param JENISNASABAH The JENISNASABAH
     */
    public void setJENISNASABAH(String JENISNASABAH) {
        this.JENISNASABAH = JENISNASABAH;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(CUREF);
        dest.writeValue(CIFNO);
        dest.writeValue(NAMAPEMOHON);
        dest.writeValue(NPWP);
        dest.writeValue(CUSTTYPE);
        dest.writeValue(JENISNASABAH);
    }

    public int describeContents() {
        return 0;
    }

}
