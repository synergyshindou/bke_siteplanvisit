
package com.apps.svi.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class GlobalResponse implements Parcelable
{

    @SerializedName("data")
    @Expose
    private List<Object> dataResponse = new ArrayList<Object>();
    @SerializedName("response")
    @Expose
    private String response;
    @SerializedName("responseText")
    @Expose
    private String responseText;
    public final static Creator<GlobalResponse> CREATOR = new Creator<GlobalResponse>() {


        @SuppressWarnings({
            "unchecked"
        })
        public GlobalResponse createFromParcel(Parcel in) {
            GlobalResponse instance = new GlobalResponse();
            in.readList(instance.dataResponse, (Object.class.getClassLoader()));
            instance.response = ((String) in.readValue((String.class.getClassLoader())));
            instance.responseText = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public GlobalResponse[] newArray(int size) {
            return (new GlobalResponse[size]);
        }

    }
    ;

    /**
     * 
     * @return
     *     The dataResponse
     */
    public List<Object> getDataResponse() {
        return dataResponse;
    }

    /**
     * 
     * @param dataResponse
     *     The dataResponse
     */
    public void setDataResponse(List<Object> dataResponse) {
        this.dataResponse = dataResponse;
    }

    /**
     * 
     * @return
     *     The response
     */
    public String getResponse() {
        return response;
    }

    /**
     * 
     * @param response
     *     The response
     */
    public void setResponse(String response) {
        this.response = response;
    }

    /**
     * 
     * @return
     *     The responseText
     */
    public String getResponseText() {
        return responseText;
    }

    /**
     * 
     * @param responseText
     *     The responseText
     */
    public void setResponseText(String responseText) {
        this.responseText = responseText;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(dataResponse);
        dest.writeValue(response);
        dest.writeValue(responseText);
    }

    public int describeContents() {
        return  0;
    }

    @Override
    public String toString() {
        return "GlobalResponse{" +
                "dataResponse=" + dataResponse +
                ", response='" + response + '\'' +
                ", responseText='" + responseText + '\'' +
                '}';
    }
}
