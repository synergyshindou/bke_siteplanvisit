package com.apps.svi.Model;

public class UploadImageEntry {
    String AP_REGNO;
    String BASE64FILES;
    String NAMEFILE;
    String FU_USERID;

    public UploadImageEntry() {
    }

    @Override
    public String toString() {
        return "UploadImageEntry{" +
                "AP_REGNO='" + AP_REGNO + '\'' +
                ", BASE64FILES='" + BASE64FILES + '\'' +
                ", NAMEFILE='" + NAMEFILE + '\'' +
                ", FU_USERID='" + FU_USERID + '\'' +
                '}';
    }

    public String getAP_REGNO() {
        return AP_REGNO;
    }

    public void setAP_REGNO(String AP_REGNO) {
        this.AP_REGNO = AP_REGNO;
    }

    public String getBASE64FILES() {
        return BASE64FILES;
    }

    public void setBASE64FILES(String BASE64FILES) {
        this.BASE64FILES = BASE64FILES;
    }

    public String getNAMEFILE() {
        return NAMEFILE;
    }

    public void setNAMEFILE(String NAMEFILE) {
        this.NAMEFILE = NAMEFILE;
    }

    public String getFU_USERID() {
        return FU_USERID;
    }

    public void setFU_USERID(String FU_USERID) {
        this.FU_USERID = FU_USERID;
    }
}
