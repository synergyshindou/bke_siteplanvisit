package com.apps.svi.Model;

/**
 * Created by Dell_Cleva on 18/04/2019.
 */

public class NasabahEntry {
    //    {
//        "CifNo": "",
//            "NamaPemohon": "ya",
//            "KtpNumber": "",
//            "TanggalLahir": "",
//            "Npwp": ""
//    }
    String CifNo;
    String NamaPemohon;
    String KtpNumber;
    String TanggalLahir;
    String Npwp;

    public NasabahEntry(String cifNo, String namaPemohon, String ktpNumber, String tanggalLahir, String npwp) {
        CifNo = cifNo;
        NamaPemohon = namaPemohon;
        KtpNumber = ktpNumber;
        TanggalLahir = tanggalLahir;
        Npwp = npwp;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("NasabahEntry{");
        sb.append("CifNo='").append(CifNo).append('\'');
        sb.append(", NamaPemohon='").append(NamaPemohon).append('\'');
        sb.append(", KtpNumber='").append(KtpNumber).append('\'');
        sb.append(", TanggalLahir='").append(TanggalLahir).append('\'');
        sb.append(", Npwp='").append(Npwp).append('\'');
        sb.append('}');
        return sb.toString();
    }

    public String getCifNo() {
        return CifNo;
    }

    public void setCifNo(String cifNo) {
        CifNo = cifNo;
    }

    public String getNamaPemohon() {
        return NamaPemohon;
    }

    public void setNamaPemohon(String namaPemohon) {
        NamaPemohon = namaPemohon;
    }

    public String getKtpNumber() {
        return KtpNumber;
    }

    public void setKtpNumber(String ktpNumber) {
        KtpNumber = ktpNumber;
    }

    public String getTanggalLahir() {
        return TanggalLahir;
    }

    public void setTanggalLahir(String tanggalLahir) {
        TanggalLahir = tanggalLahir;
    }

    public String getNpwp() {
        return Npwp;
    }

    public void setNpwp(String npwp) {
        Npwp = npwp;
    }
}
