package com.apps.svi.Model;

/**
 * Created by Dell_Cleva on 22/04/2019.
 */

public class PersonnalSiteVisitEntry {
//    {
//            "AP_REGNO": "19092017100000002",
//            "CU_REF": "19092017100000001",
//            "SV_DATE": "2019-04-18",
//            "SV_HS_PEMBERI_KET1": "ADAM",
//            "SV_HS_STS_HOME_STAY": "1"
//    }
    String AP_REGNO;
    String CU_REF;
    String SV_DATE;
    String SV_HS_PEMBERI_KET1;
    String SV_HS_STS_HOME_STAY;

    public PersonnalSiteVisitEntry() {
    }

    public String getAP_REGNO() {
        return AP_REGNO;
    }

    public void setAP_REGNO(String AP_REGNO) {
        this.AP_REGNO = AP_REGNO;
    }

    public String getCU_REF() {
        return CU_REF;
    }

    public void setCU_REF(String CU_REF) {
        this.CU_REF = CU_REF;
    }

    public String getSV_DATE() {
        return SV_DATE;
    }

    public void setSV_DATE(String SV_DATE) {
        this.SV_DATE = SV_DATE;
    }

    public String getSV_HS_PEMBERI_KET1() {
        return SV_HS_PEMBERI_KET1;
    }

    public void setSV_HS_PEMBERI_KET1(String SV_HS_PEMBERI_KET1) {
        this.SV_HS_PEMBERI_KET1 = SV_HS_PEMBERI_KET1;
    }

    public String getSV_HS_STS_HOME_STAY() {
        return SV_HS_STS_HOME_STAY;
    }

    public void setSV_HS_STS_HOME_STAY(String SV_HS_STS_HOME_STAY) {
        this.SV_HS_STS_HOME_STAY = SV_HS_STS_HOME_STAY;
    }
}
