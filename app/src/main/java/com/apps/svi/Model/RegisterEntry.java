package com.apps.svi.Model;

/**
 * Created by Dell_Cleva on 23/04/2019.
 */

public class RegisterEntry {

    String Username;
    String Email;
    String Password;
    String ConfirmPassword;

    public RegisterEntry(String username, String email, String password, String confirmPassword) {
        Username = username;
        Email = email;
        Password = password;
        ConfirmPassword = confirmPassword;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String username) {
        Username = username;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getConfirmPassword() {
        return ConfirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        ConfirmPassword = confirmPassword;
    }
}
