package com.apps.svi.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataApplication implements Parcelable {

    public final static Parcelable.Creator<DataApplication> CREATOR = new Creator<DataApplication>() {


        @SuppressWarnings({
                "unchecked"
        })
        public DataApplication createFromParcel(Parcel in) {
            DataApplication instance = new DataApplication();
            instance.APREGNO = ((String) in.readValue((String.class.getClassLoader())));
            instance.CUREF = ((String) in.readValue((String.class.getClassLoader())));
            instance.PROGCODE = ((String) in.readValue((String.class.getClassLoader())));
            instance.AREAID = ((String) in.readValue((String.class.getClassLoader())));
            instance.BRANCHCODE = ((String) in.readValue((String.class.getClassLoader())));
            instance.CHANNELCODE = ((String) in.readValue((String.class.getClassLoader())));
            instance.AANO = ((String) in.readValue((String.class.getClassLoader())));
            instance.APSRCCODE = ((String) in.readValue((String.class.getClassLoader())));
            instance.APGROSSSALES = ((String) in.readValue((String.class.getClassLoader())));
            instance.APSIGNDATE = ((String) in.readValue((String.class.getClassLoader())));
            instance.APRECVDATE = ((String) in.readValue((String.class.getClassLoader())));
            instance.APDATALNGKP = ((String) in.readValue((String.class.getClassLoader())));
            instance.APRELMNGR = ((String) in.readValue((String.class.getClassLoader())));
            instance.APSALESAGENCY = ((String) in.readValue((String.class.getClassLoader())));
            instance.APSALESSUPERV = ((String) in.readValue((String.class.getClassLoader())));
            instance.APSALESEXEC = ((String) in.readValue((String.class.getClassLoader())));
            instance.APLIMITEXPOSURE = ((String) in.readValue((String.class.getClassLoader())));
            instance.APUSERNAME = ((String) in.readValue((String.class.getClassLoader())));
            instance.APCANCEL = ((String) in.readValue((String.class.getClassLoader())));
            instance.APREJECT = ((String) in.readValue((String.class.getClassLoader())));
            instance.APAPPRSTATUS = ((String) in.readValue((String.class.getClassLoader())));
            instance.APSITEVISITSTA = ((String) in.readValue((String.class.getClassLoader())));
            instance.APISAPPEAL = ((String) in.readValue((String.class.getClassLoader())));
            instance.APISAPPEALBY = ((String) in.readValue((String.class.getClassLoader())));
            instance.APISAPPEALDATE = ((String) in.readValue((String.class.getClassLoader())));
            instance.APCOMPLEVEL = ((String) in.readValue((String.class.getClassLoader())));
            instance.APGROUPCOMPANY = ((String) in.readValue((String.class.getClassLoader())));
            instance.APAPRVNEXTBY = ((String) in.readValue((String.class.getClassLoader())));
            instance.APAPPEAL = ((String) in.readValue((String.class.getClassLoader())));
            instance.APJOINAPPROVAL = ((String) in.readValue((String.class.getClassLoader())));
            instance.APJOINAPRVSEQ = ((String) in.readValue((String.class.getClassLoader())));
            instance.APAPRVUP = ((String) in.readValue((String.class.getClassLoader())));
            instance.APCONFIRMBOOK = ((String) in.readValue((String.class.getClassLoader())));
            instance.APBOOKFLAG = ((String) in.readValue((String.class.getClassLoader())));
            instance.APPRRKBY = ((String) in.readValue((String.class.getClassLoader())));
            instance.APBLMEMO = ((String) in.readValue((String.class.getClassLoader())));
            instance.APACQINFO = ((String) in.readValue((String.class.getClassLoader())));
            instance.APBUSINESSUNIT = ((String) in.readValue((String.class.getClassLoader())));
            instance.APBOOKINGBRANCH = ((String) in.readValue((String.class.getClassLoader())));
            instance.APTEAMLEADER = ((String) in.readValue((String.class.getClassLoader())));
            instance.APAPRVUNTIL = ((String) in.readValue((String.class.getClassLoader())));
            instance.APAPRVEYES = ((String) in.readValue((String.class.getClassLoader())));
            instance.APAPRVREMARK = ((String) in.readValue((String.class.getClassLoader())));
            instance.APCHECKBI = ((String) in.readValue((String.class.getClassLoader())));
            instance.APCO = ((String) in.readValue((String.class.getClassLoader())));
            instance.APCA = ((String) in.readValue((String.class.getClassLoader())));
            instance.APACQINFOBY = ((String) in.readValue((String.class.getClassLoader())));
            instance.APACQINFODATE = ((String) in.readValue((String.class.getClassLoader())));
            instance.APMAINAPREGNO = ((String) in.readValue((String.class.getClassLoader())));
            instance.APBLBMPEMILIK = ((String) in.readValue((String.class.getClassLoader())));
            instance.APBLBMMGMT = ((String) in.readValue((String.class.getClassLoader())));
            instance.APBLBMUSAHA = ((String) in.readValue((String.class.getClassLoader())));
            instance.APBLBIPEMILIK = ((String) in.readValue((String.class.getClassLoader())));
            instance.APBLBIMGMT = ((String) in.readValue((String.class.getClassLoader())));
            instance.APBLBIUSAHA = ((String) in.readValue((String.class.getClassLoader())));
            instance.APCHECKBIGROUP = ((String) in.readValue((String.class.getClassLoader())));
            instance.APBLBIPERNAH = ((String) in.readValue((String.class.getClassLoader())));
            instance.APBLBMPERNAH = ((String) in.readValue((String.class.getClassLoader())));
            instance.APAPRVCOMMITEE = ((String) in.readValue((String.class.getClassLoader())));
            instance.APBLBIACCBK = ((String) in.readValue((String.class.getClassLoader())));
            instance.APBLBIOCBK = ((String) in.readValue((String.class.getClassLoader())));
            instance.APBLBIMCBKS = ((String) in.readValue((String.class.getClassLoader())));
            instance.APGROSSSALESCURR = ((String) in.readValue((String.class.getClassLoader())));
            instance.APMIGRATIONVER = ((String) in.readValue((String.class.getClassLoader())));
            instance.APCCOBRANCH = ((String) in.readValue((String.class.getClassLoader())));
            instance.APLASTFWDBUBY = ((String) in.readValue((String.class.getClassLoader())));
            instance.APSKBNGPASAR = ((String) in.readValue((String.class.getClassLoader())));
            instance.APSKBNGDIMINTA = ((String) in.readValue((String.class.getClassLoader())));
            instance.APBLBIACCBK12BLN = ((String) in.readValue((String.class.getClassLoader())));
            instance.APBLBIOCBK12BLN = ((String) in.readValue((String.class.getClassLoader())));
            instance.APBLBIMCBKS12BLN = ((String) in.readValue((String.class.getClassLoader())));
            instance.APSRCNAME = ((String) in.readValue((String.class.getClassLoader())));
            instance.CBI = ((String) in.readValue((String.class.getClassLoader())));
            instance.APSURATNSBNO = ((String) in.readValue((String.class.getClassLoader())));
            instance.APSURATNSBTGL = ((String) in.readValue((String.class.getClassLoader())));
            instance.APSURATNSBTGLTRM = ((String) in.readValue((String.class.getClassLoader())));
            instance.APREGNOINDUK = ((String) in.readValue((String.class.getClassLoader())));
            instance.PRODUCTIDINDUK = ((String) in.readValue((String.class.getClassLoader())));
            instance.AANOINDUK = ((String) in.readValue((String.class.getClassLoader())));
            instance.ACCSEQINDUK = ((String) in.readValue((String.class.getClassLoader())));
            instance.PRODSEQINDUK = ((String) in.readValue((String.class.getClassLoader())));
            instance.APAPRVCOND = ((String) in.readValue((String.class.getClassLoader())));
            instance.APAPRVCONDBY = ((String) in.readValue((String.class.getClassLoader())));
            instance.APLOWLINE = ((String) in.readValue((String.class.getClassLoader())));
            instance.APRISKMNGR = ((String) in.readValue((String.class.getClassLoader())));
            instance.APISREADYNAK = ((String) in.readValue((String.class.getClassLoader())));
            instance.APNERACALASTDATE = ((String) in.readValue((String.class.getClassLoader())));
            instance.APLABARUGILASTDATE = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public DataApplication[] newArray(int size) {
            return (new DataApplication[size]);
        }

    };
    @SerializedName("AP_REGNO")
    @Expose
    private String APREGNO;
    @SerializedName("CU_REF")
    @Expose
    private String CUREF;
    @SerializedName("PROG_CODE")
    @Expose
    private String PROGCODE;
    @SerializedName("AREAID")
    @Expose
    private String AREAID;
    @SerializedName("BRANCH_CODE")
    @Expose
    private String BRANCHCODE;
    @SerializedName("CHANNEL_CODE")
    @Expose
    private String CHANNELCODE;
    @SerializedName("AA_NO")
    @Expose
    private String AANO;
    @SerializedName("AP_SRCCODE")
    @Expose
    private String APSRCCODE;
    @SerializedName("AP_GROSSSALES")
    @Expose
    private String APGROSSSALES;
    @SerializedName("AP_SIGNDATE")
    @Expose
    private String APSIGNDATE;
    @SerializedName("AP_RECVDATE")
    @Expose
    private String APRECVDATE;
    @SerializedName("AP_DATALNGKP")
    @Expose
    private String APDATALNGKP;
    @SerializedName("AP_RELMNGR")
    @Expose
    private String APRELMNGR;
    @SerializedName("AP_SALESAGENCY")
    @Expose
    private String APSALESAGENCY;
    @SerializedName("AP_SALESSUPERV")
    @Expose
    private String APSALESSUPERV;
    @SerializedName("AP_SALESEXEC")
    @Expose
    private String APSALESEXEC;
    @SerializedName("AP_LIMITEXPOSURE")
    @Expose
    private String APLIMITEXPOSURE;
    @SerializedName("AP_USERNAME")
    @Expose
    private String APUSERNAME;
    @SerializedName("AP_CANCEL")
    @Expose
    private String APCANCEL;
    @SerializedName("AP_REJECT")
    @Expose
    private String APREJECT;
    @SerializedName("AP_APPRSTATUS")
    @Expose
    private String APAPPRSTATUS;
    @SerializedName("AP_SITEVISITSTA")
    @Expose
    private String APSITEVISITSTA;
    @SerializedName("AP_ISAPPEAL")
    @Expose
    private String APISAPPEAL;
    @SerializedName("AP_ISAPPEALBY")
    @Expose
    private String APISAPPEALBY;
    @SerializedName("AP_ISAPPEALDATE")
    @Expose
    private String APISAPPEALDATE;
    @SerializedName("AP_COMPLEVEL")
    @Expose
    private String APCOMPLEVEL;
    @SerializedName("AP_GROUPCOMPANY")
    @Expose
    private String APGROUPCOMPANY;
    @SerializedName("AP_APRVNEXTBY")
    @Expose
    private String APAPRVNEXTBY;
    @SerializedName("AP_APPEAL")
    @Expose
    private String APAPPEAL;
    @SerializedName("AP_JOINAPPROVAL")
    @Expose
    private String APJOINAPPROVAL;
    @SerializedName("AP_JOINAPRVSEQ")
    @Expose
    private String APJOINAPRVSEQ;
    @SerializedName("AP_APRVUP")
    @Expose
    private String APAPRVUP;
    @SerializedName("AP_CONFIRMBOOK")
    @Expose
    private String APCONFIRMBOOK;
    @SerializedName("AP_BOOKFLAG")
    @Expose
    private String APBOOKFLAG;
    @SerializedName("AP_PRRKBY")
    @Expose
    private String APPRRKBY;
    @SerializedName("AP_BLMEMO")
    @Expose
    private String APBLMEMO;
    @SerializedName("AP_ACQINFO")
    @Expose
    private String APACQINFO;
    @SerializedName("AP_BUSINESSUNIT")
    @Expose
    private String APBUSINESSUNIT;
    @SerializedName("AP_BOOKINGBRANCH")
    @Expose
    private String APBOOKINGBRANCH;
    @SerializedName("AP_TEAMLEADER")
    @Expose
    private String APTEAMLEADER;
    @SerializedName("AP_APRVUNTIL")
    @Expose
    private String APAPRVUNTIL;
    @SerializedName("AP_APRVEYES")
    @Expose
    private String APAPRVEYES;
    @SerializedName("AP_APRVREMARK")
    @Expose
    private String APAPRVREMARK;
    @SerializedName("AP_CHECKBI")
    @Expose
    private String APCHECKBI;
    @SerializedName("AP_CO")
    @Expose
    private String APCO;
    @SerializedName("AP_CA")
    @Expose
    private String APCA;
    @SerializedName("AP_ACQINFOBY")
    @Expose
    private String APACQINFOBY;
    @SerializedName("AP_ACQINFODATE")
    @Expose
    private String APACQINFODATE;
    @SerializedName("AP_MAINAPREGNO")
    @Expose
    private String APMAINAPREGNO;
    @SerializedName("AP_BLBMPEMILIK")
    @Expose
    private String APBLBMPEMILIK;
    @SerializedName("AP_BLBMMGMT")
    @Expose
    private String APBLBMMGMT;
    @SerializedName("AP_BLBMUSAHA")
    @Expose
    private String APBLBMUSAHA;
    @SerializedName("AP_BLBIPEMILIK")
    @Expose
    private String APBLBIPEMILIK;
    @SerializedName("AP_BLBIMGMT")
    @Expose
    private String APBLBIMGMT;
    @SerializedName("AP_BLBIUSAHA")
    @Expose
    private String APBLBIUSAHA;
    @SerializedName("AP_CHECKBIGROUP")
    @Expose
    private String APCHECKBIGROUP;
    @SerializedName("AP_BLBIPERNAH")
    @Expose
    private String APBLBIPERNAH;
    @SerializedName("AP_BLBMPERNAH")
    @Expose
    private String APBLBMPERNAH;
    @SerializedName("AP_APRVCOMMITEE")
    @Expose
    private String APAPRVCOMMITEE;
    @SerializedName("AP_BLBIACCBK")
    @Expose
    private String APBLBIACCBK;
    @SerializedName("AP_BLBIOCBK")
    @Expose
    private String APBLBIOCBK;
    @SerializedName("AP_BLBIMCBKS")
    @Expose
    private String APBLBIMCBKS;
    @SerializedName("AP_GROSSSALESCURR")
    @Expose
    private String APGROSSSALESCURR;
    @SerializedName("AP_MIGRATIONVER")
    @Expose
    private String APMIGRATIONVER;
    @SerializedName("AP_CCOBRANCH")
    @Expose
    private String APCCOBRANCH;
    @SerializedName("AP_LASTFWDBUBY")
    @Expose
    private String APLASTFWDBUBY;
    @SerializedName("AP_SKBNGPASAR")
    @Expose
    private String APSKBNGPASAR;
    @SerializedName("AP_SKBNGDIMINTA")
    @Expose
    private String APSKBNGDIMINTA;
    @SerializedName("AP_BLBIACCBK12BLN")
    @Expose
    private String APBLBIACCBK12BLN;
    @SerializedName("AP_BLBIOCBK12BLN")
    @Expose
    private String APBLBIOCBK12BLN;
    @SerializedName("AP_BLBIMCBKS12BLN")
    @Expose
    private String APBLBIMCBKS12BLN;
    @SerializedName("AP_SRCNAME")
    @Expose
    private String APSRCNAME;
    @SerializedName("CBI")
    @Expose
    private String CBI;
    @SerializedName("AP_SURATNSBNO")
    @Expose
    private String APSURATNSBNO;
    @SerializedName("AP_SURATNSBTGL")
    @Expose
    private String APSURATNSBTGL;
    @SerializedName("AP_SURATNSBTGLTRM")
    @Expose
    private String APSURATNSBTGLTRM;
    @SerializedName("APREGNO_INDUK")
    @Expose
    private String APREGNOINDUK;
    @SerializedName("PRODUCTID_INDUK")
    @Expose
    private String PRODUCTIDINDUK;
    @SerializedName("AANO_INDUK")
    @Expose
    private String AANOINDUK;
    @SerializedName("ACCSEQ_INDUK")
    @Expose
    private String ACCSEQINDUK;
    @SerializedName("PRODSEQ_INDUK")
    @Expose
    private String PRODSEQINDUK;
    @SerializedName("AP_APRVCOND")
    @Expose
    private String APAPRVCOND;
    @SerializedName("AP_APRVCONDBY")
    @Expose
    private String APAPRVCONDBY;
    @SerializedName("AP_LOWLINE")
    @Expose
    private String APLOWLINE;
    @SerializedName("AP_RISKMNGR")
    @Expose
    private String APRISKMNGR;
    @SerializedName("AP_ISREADYNAK")
    @Expose
    private String APISREADYNAK;
    @SerializedName("AP_NERACA_LASTDATE")
    @Expose
    private String APNERACALASTDATE;
    @SerializedName("AP_LABARUGI_LASTDATE")
    @Expose
    private String APLABARUGILASTDATE;

    /**
     * @return The APREGNO
     */
    public String getAPREGNO() {
        return APREGNO;
    }

    /**
     * @param APREGNO The AP_REGNO
     */
    public void setAPREGNO(String APREGNO) {
        this.APREGNO = APREGNO;
    }

    /**
     * @return The CUREF
     */
    public String getCUREF() {
        return CUREF;
    }

    /**
     * @param CUREF The CU_REF
     */
    public void setCUREF(String CUREF) {
        this.CUREF = CUREF;
    }

    /**
     * @return The PROGCODE
     */
    public String getPROGCODE() {
        return PROGCODE;
    }

    /**
     * @param PROGCODE The PROG_CODE
     */
    public void setPROGCODE(String PROGCODE) {
        this.PROGCODE = PROGCODE;
    }

    /**
     * @return The AREAID
     */
    public String getAREAID() {
        return AREAID;
    }

    /**
     * @param AREAID The AREAID
     */
    public void setAREAID(String AREAID) {
        this.AREAID = AREAID;
    }

    /**
     * @return The BRANCHCODE
     */
    public String getBRANCHCODE() {
        return BRANCHCODE;
    }

    /**
     * @param BRANCHCODE The BRANCH_CODE
     */
    public void setBRANCHCODE(String BRANCHCODE) {
        this.BRANCHCODE = BRANCHCODE;
    }

    /**
     * @return The CHANNELCODE
     */
    public String getCHANNELCODE() {
        return CHANNELCODE;
    }

    /**
     * @param CHANNELCODE The CHANNEL_CODE
     */
    public void setCHANNELCODE(String CHANNELCODE) {
        this.CHANNELCODE = CHANNELCODE;
    }

    /**
     * @return The AANO
     */
    public String getAANO() {
        return AANO;
    }

    /**
     * @param AANO The AA_NO
     */
    public void setAANO(String AANO) {
        this.AANO = AANO;
    }

    /**
     * @return The APSRCCODE
     */
    public String getAPSRCCODE() {
        return APSRCCODE;
    }

    /**
     * @param APSRCCODE The AP_SRCCODE
     */
    public void setAPSRCCODE(String APSRCCODE) {
        this.APSRCCODE = APSRCCODE;
    }

    /**
     * @return The APGROSSSALES
     */
    public String getAPGROSSSALES() {
        return APGROSSSALES;
    }

    /**
     * @param APGROSSSALES The AP_GROSSSALES
     */
    public void setAPGROSSSALES(String APGROSSSALES) {
        this.APGROSSSALES = APGROSSSALES;
    }

    /**
     * @return The APSIGNDATE
     */
    public String getAPSIGNDATE() {
        return APSIGNDATE;
    }

    /**
     * @param APSIGNDATE The AP_SIGNDATE
     */
    public void setAPSIGNDATE(String APSIGNDATE) {
        this.APSIGNDATE = APSIGNDATE;
    }

    /**
     * @return The APRECVDATE
     */
    public String getAPRECVDATE() {
        return APRECVDATE;
    }

    /**
     * @param APRECVDATE The AP_RECVDATE
     */
    public void setAPRECVDATE(String APRECVDATE) {
        this.APRECVDATE = APRECVDATE;
    }

    /**
     * @return The APDATALNGKP
     */
    public String getAPDATALNGKP() {
        return APDATALNGKP;
    }

    /**
     * @param APDATALNGKP The AP_DATALNGKP
     */
    public void setAPDATALNGKP(String APDATALNGKP) {
        this.APDATALNGKP = APDATALNGKP;
    }

    /**
     * @return The APRELMNGR
     */
    public String getAPRELMNGR() {
        return APRELMNGR;
    }

    /**
     * @param APRELMNGR The AP_RELMNGR
     */
    public void setAPRELMNGR(String APRELMNGR) {
        this.APRELMNGR = APRELMNGR;
    }

    /**
     * @return The APSALESAGENCY
     */
    public String getAPSALESAGENCY() {
        return APSALESAGENCY;
    }

    /**
     * @param APSALESAGENCY The AP_SALESAGENCY
     */
    public void setAPSALESAGENCY(String APSALESAGENCY) {
        this.APSALESAGENCY = APSALESAGENCY;
    }

    /**
     * @return The APSALESSUPERV
     */
    public String getAPSALESSUPERV() {
        return APSALESSUPERV;
    }

    /**
     * @param APSALESSUPERV The AP_SALESSUPERV
     */
    public void setAPSALESSUPERV(String APSALESSUPERV) {
        this.APSALESSUPERV = APSALESSUPERV;
    }

    /**
     * @return The APSALESEXEC
     */
    public String getAPSALESEXEC() {
        return APSALESEXEC;
    }

    /**
     * @param APSALESEXEC The AP_SALESEXEC
     */
    public void setAPSALESEXEC(String APSALESEXEC) {
        this.APSALESEXEC = APSALESEXEC;
    }

    /**
     * @return The APLIMITEXPOSURE
     */
    public String getAPLIMITEXPOSURE() {
        return APLIMITEXPOSURE;
    }

    /**
     * @param APLIMITEXPOSURE The AP_LIMITEXPOSURE
     */
    public void setAPLIMITEXPOSURE(String APLIMITEXPOSURE) {
        this.APLIMITEXPOSURE = APLIMITEXPOSURE;
    }

    /**
     * @return The APUSERNAME
     */
    public String getAPUSERNAME() {
        return APUSERNAME;
    }

    /**
     * @param APUSERNAME The AP_USERNAME
     */
    public void setAPUSERNAME(String APUSERNAME) {
        this.APUSERNAME = APUSERNAME;
    }

    /**
     * @return The APCANCEL
     */
    public String getAPCANCEL() {
        return APCANCEL;
    }

    /**
     * @param APCANCEL The AP_CANCEL
     */
    public void setAPCANCEL(String APCANCEL) {
        this.APCANCEL = APCANCEL;
    }

    /**
     * @return The APREJECT
     */
    public String getAPREJECT() {
        return APREJECT;
    }

    /**
     * @param APREJECT The AP_REJECT
     */
    public void setAPREJECT(String APREJECT) {
        this.APREJECT = APREJECT;
    }

    /**
     * @return The APAPPRSTATUS
     */
    public String getAPAPPRSTATUS() {
        return APAPPRSTATUS;
    }

    /**
     * @param APAPPRSTATUS The AP_APPRSTATUS
     */
    public void setAPAPPRSTATUS(String APAPPRSTATUS) {
        this.APAPPRSTATUS = APAPPRSTATUS;
    }

    /**
     * @return The APSITEVISITSTA
     */
    public String getAPSITEVISITSTA() {
        return APSITEVISITSTA;
    }

    /**
     * @param APSITEVISITSTA The AP_SITEVISITSTA
     */
    public void setAPSITEVISITSTA(String APSITEVISITSTA) {
        this.APSITEVISITSTA = APSITEVISITSTA;
    }

    /**
     * @return The APISAPPEAL
     */
    public String getAPISAPPEAL() {
        return APISAPPEAL;
    }

    /**
     * @param APISAPPEAL The AP_ISAPPEAL
     */
    public void setAPISAPPEAL(String APISAPPEAL) {
        this.APISAPPEAL = APISAPPEAL;
    }

    /**
     * @return The APISAPPEALBY
     */
    public String getAPISAPPEALBY() {
        return APISAPPEALBY;
    }

    /**
     * @param APISAPPEALBY The AP_ISAPPEALBY
     */
    public void setAPISAPPEALBY(String APISAPPEALBY) {
        this.APISAPPEALBY = APISAPPEALBY;
    }

    /**
     * @return The APISAPPEALDATE
     */
    public String getAPISAPPEALDATE() {
        return APISAPPEALDATE;
    }

    /**
     * @param APISAPPEALDATE The AP_ISAPPEALDATE
     */
    public void setAPISAPPEALDATE(String APISAPPEALDATE) {
        this.APISAPPEALDATE = APISAPPEALDATE;
    }

    /**
     * @return The APCOMPLEVEL
     */
    public String getAPCOMPLEVEL() {
        return APCOMPLEVEL;
    }

    /**
     * @param APCOMPLEVEL The AP_COMPLEVEL
     */
    public void setAPCOMPLEVEL(String APCOMPLEVEL) {
        this.APCOMPLEVEL = APCOMPLEVEL;
    }

    /**
     * @return The APGROUPCOMPANY
     */
    public String getAPGROUPCOMPANY() {
        return APGROUPCOMPANY;
    }

    /**
     * @param APGROUPCOMPANY The AP_GROUPCOMPANY
     */
    public void setAPGROUPCOMPANY(String APGROUPCOMPANY) {
        this.APGROUPCOMPANY = APGROUPCOMPANY;
    }

    /**
     * @return The APAPRVNEXTBY
     */
    public String getAPAPRVNEXTBY() {
        return APAPRVNEXTBY;
    }

    /**
     * @param APAPRVNEXTBY The AP_APRVNEXTBY
     */
    public void setAPAPRVNEXTBY(String APAPRVNEXTBY) {
        this.APAPRVNEXTBY = APAPRVNEXTBY;
    }

    /**
     * @return The APAPPEAL
     */
    public String getAPAPPEAL() {
        return APAPPEAL;
    }

    /**
     * @param APAPPEAL The AP_APPEAL
     */
    public void setAPAPPEAL(String APAPPEAL) {
        this.APAPPEAL = APAPPEAL;
    }

    /**
     * @return The APJOINAPPROVAL
     */
    public String getAPJOINAPPROVAL() {
        return APJOINAPPROVAL;
    }

    /**
     * @param APJOINAPPROVAL The AP_JOINAPPROVAL
     */
    public void setAPJOINAPPROVAL(String APJOINAPPROVAL) {
        this.APJOINAPPROVAL = APJOINAPPROVAL;
    }

    /**
     * @return The APJOINAPRVSEQ
     */
    public String getAPJOINAPRVSEQ() {
        return APJOINAPRVSEQ;
    }

    /**
     * @param APJOINAPRVSEQ The AP_JOINAPRVSEQ
     */
    public void setAPJOINAPRVSEQ(String APJOINAPRVSEQ) {
        this.APJOINAPRVSEQ = APJOINAPRVSEQ;
    }

    /**
     * @return The APAPRVUP
     */
    public String getAPAPRVUP() {
        return APAPRVUP;
    }

    /**
     * @param APAPRVUP The AP_APRVUP
     */
    public void setAPAPRVUP(String APAPRVUP) {
        this.APAPRVUP = APAPRVUP;
    }

    /**
     * @return The APCONFIRMBOOK
     */
    public String getAPCONFIRMBOOK() {
        return APCONFIRMBOOK;
    }

    /**
     * @param APCONFIRMBOOK The AP_CONFIRMBOOK
     */
    public void setAPCONFIRMBOOK(String APCONFIRMBOOK) {
        this.APCONFIRMBOOK = APCONFIRMBOOK;
    }

    /**
     * @return The APBOOKFLAG
     */
    public String getAPBOOKFLAG() {
        return APBOOKFLAG;
    }

    /**
     * @param APBOOKFLAG The AP_BOOKFLAG
     */
    public void setAPBOOKFLAG(String APBOOKFLAG) {
        this.APBOOKFLAG = APBOOKFLAG;
    }

    /**
     * @return The APPRRKBY
     */
    public String getAPPRRKBY() {
        return APPRRKBY;
    }

    /**
     * @param APPRRKBY The AP_PRRKBY
     */
    public void setAPPRRKBY(String APPRRKBY) {
        this.APPRRKBY = APPRRKBY;
    }

    /**
     * @return The APBLMEMO
     */
    public String getAPBLMEMO() {
        return APBLMEMO;
    }

    /**
     * @param APBLMEMO The AP_BLMEMO
     */
    public void setAPBLMEMO(String APBLMEMO) {
        this.APBLMEMO = APBLMEMO;
    }

    /**
     * @return The APACQINFO
     */
    public String getAPACQINFO() {
        return APACQINFO;
    }

    /**
     * @param APACQINFO The AP_ACQINFO
     */
    public void setAPACQINFO(String APACQINFO) {
        this.APACQINFO = APACQINFO;
    }

    /**
     * @return The APBUSINESSUNIT
     */
    public String getAPBUSINESSUNIT() {
        return APBUSINESSUNIT;
    }

    /**
     * @param APBUSINESSUNIT The AP_BUSINESSUNIT
     */
    public void setAPBUSINESSUNIT(String APBUSINESSUNIT) {
        this.APBUSINESSUNIT = APBUSINESSUNIT;
    }

    /**
     * @return The APBOOKINGBRANCH
     */
    public String getAPBOOKINGBRANCH() {
        return APBOOKINGBRANCH;
    }

    /**
     * @param APBOOKINGBRANCH The AP_BOOKINGBRANCH
     */
    public void setAPBOOKINGBRANCH(String APBOOKINGBRANCH) {
        this.APBOOKINGBRANCH = APBOOKINGBRANCH;
    }

    /**
     * @return The APTEAMLEADER
     */
    public String getAPTEAMLEADER() {
        return APTEAMLEADER;
    }

    /**
     * @param APTEAMLEADER The AP_TEAMLEADER
     */
    public void setAPTEAMLEADER(String APTEAMLEADER) {
        this.APTEAMLEADER = APTEAMLEADER;
    }

    /**
     * @return The APAPRVUNTIL
     */
    public String getAPAPRVUNTIL() {
        return APAPRVUNTIL;
    }

    /**
     * @param APAPRVUNTIL The AP_APRVUNTIL
     */
    public void setAPAPRVUNTIL(String APAPRVUNTIL) {
        this.APAPRVUNTIL = APAPRVUNTIL;
    }

    /**
     * @return The APAPRVEYES
     */
    public String getAPAPRVEYES() {
        return APAPRVEYES;
    }

    /**
     * @param APAPRVEYES The AP_APRVEYES
     */
    public void setAPAPRVEYES(String APAPRVEYES) {
        this.APAPRVEYES = APAPRVEYES;
    }

    /**
     * @return The APAPRVREMARK
     */
    public String getAPAPRVREMARK() {
        return APAPRVREMARK;
    }

    /**
     * @param APAPRVREMARK The AP_APRVREMARK
     */
    public void setAPAPRVREMARK(String APAPRVREMARK) {
        this.APAPRVREMARK = APAPRVREMARK;
    }

    /**
     * @return The APCHECKBI
     */
    public String getAPCHECKBI() {
        return APCHECKBI;
    }

    /**
     * @param APCHECKBI The AP_CHECKBI
     */
    public void setAPCHECKBI(String APCHECKBI) {
        this.APCHECKBI = APCHECKBI;
    }

    /**
     * @return The APCO
     */
    public String getAPCO() {
        return APCO;
    }

    /**
     * @param APCO The AP_CO
     */
    public void setAPCO(String APCO) {
        this.APCO = APCO;
    }

    /**
     * @return The APCA
     */
    public String getAPCA() {
        return APCA;
    }

    /**
     * @param APCA The AP_CA
     */
    public void setAPCA(String APCA) {
        this.APCA = APCA;
    }

    /**
     * @return The APACQINFOBY
     */
    public String getAPACQINFOBY() {
        return APACQINFOBY;
    }

    /**
     * @param APACQINFOBY The AP_ACQINFOBY
     */
    public void setAPACQINFOBY(String APACQINFOBY) {
        this.APACQINFOBY = APACQINFOBY;
    }

    /**
     * @return The APACQINFODATE
     */
    public String getAPACQINFODATE() {
        return APACQINFODATE;
    }

    /**
     * @param APACQINFODATE The AP_ACQINFODATE
     */
    public void setAPACQINFODATE(String APACQINFODATE) {
        this.APACQINFODATE = APACQINFODATE;
    }

    /**
     * @return The APMAINAPREGNO
     */
    public String getAPMAINAPREGNO() {
        return APMAINAPREGNO;
    }

    /**
     * @param APMAINAPREGNO The AP_MAINAPREGNO
     */
    public void setAPMAINAPREGNO(String APMAINAPREGNO) {
        this.APMAINAPREGNO = APMAINAPREGNO;
    }

    /**
     * @return The APBLBMPEMILIK
     */
    public String getAPBLBMPEMILIK() {
        return APBLBMPEMILIK;
    }

    /**
     * @param APBLBMPEMILIK The AP_BLBMPEMILIK
     */
    public void setAPBLBMPEMILIK(String APBLBMPEMILIK) {
        this.APBLBMPEMILIK = APBLBMPEMILIK;
    }

    /**
     * @return The APBLBMMGMT
     */
    public String getAPBLBMMGMT() {
        return APBLBMMGMT;
    }

    /**
     * @param APBLBMMGMT The AP_BLBMMGMT
     */
    public void setAPBLBMMGMT(String APBLBMMGMT) {
        this.APBLBMMGMT = APBLBMMGMT;
    }

    /**
     * @return The APBLBMUSAHA
     */
    public String getAPBLBMUSAHA() {
        return APBLBMUSAHA;
    }

    /**
     * @param APBLBMUSAHA The AP_BLBMUSAHA
     */
    public void setAPBLBMUSAHA(String APBLBMUSAHA) {
        this.APBLBMUSAHA = APBLBMUSAHA;
    }

    /**
     * @return The APBLBIPEMILIK
     */
    public String getAPBLBIPEMILIK() {
        return APBLBIPEMILIK;
    }

    /**
     * @param APBLBIPEMILIK The AP_BLBIPEMILIK
     */
    public void setAPBLBIPEMILIK(String APBLBIPEMILIK) {
        this.APBLBIPEMILIK = APBLBIPEMILIK;
    }

    /**
     * @return The APBLBIMGMT
     */
    public String getAPBLBIMGMT() {
        return APBLBIMGMT;
    }

    /**
     * @param APBLBIMGMT The AP_BLBIMGMT
     */
    public void setAPBLBIMGMT(String APBLBIMGMT) {
        this.APBLBIMGMT = APBLBIMGMT;
    }

    /**
     * @return The APBLBIUSAHA
     */
    public String getAPBLBIUSAHA() {
        return APBLBIUSAHA;
    }

    /**
     * @param APBLBIUSAHA The AP_BLBIUSAHA
     */
    public void setAPBLBIUSAHA(String APBLBIUSAHA) {
        this.APBLBIUSAHA = APBLBIUSAHA;
    }

    /**
     * @return The APCHECKBIGROUP
     */
    public String getAPCHECKBIGROUP() {
        return APCHECKBIGROUP;
    }

    /**
     * @param APCHECKBIGROUP The AP_CHECKBIGROUP
     */
    public void setAPCHECKBIGROUP(String APCHECKBIGROUP) {
        this.APCHECKBIGROUP = APCHECKBIGROUP;
    }

    /**
     * @return The APBLBIPERNAH
     */
    public String getAPBLBIPERNAH() {
        return APBLBIPERNAH;
    }

    /**
     * @param APBLBIPERNAH The AP_BLBIPERNAH
     */
    public void setAPBLBIPERNAH(String APBLBIPERNAH) {
        this.APBLBIPERNAH = APBLBIPERNAH;
    }

    /**
     * @return The APBLBMPERNAH
     */
    public String getAPBLBMPERNAH() {
        return APBLBMPERNAH;
    }

    /**
     * @param APBLBMPERNAH The AP_BLBMPERNAH
     */
    public void setAPBLBMPERNAH(String APBLBMPERNAH) {
        this.APBLBMPERNAH = APBLBMPERNAH;
    }

    /**
     * @return The APAPRVCOMMITEE
     */
    public String getAPAPRVCOMMITEE() {
        return APAPRVCOMMITEE;
    }

    /**
     * @param APAPRVCOMMITEE The AP_APRVCOMMITEE
     */
    public void setAPAPRVCOMMITEE(String APAPRVCOMMITEE) {
        this.APAPRVCOMMITEE = APAPRVCOMMITEE;
    }

    /**
     * @return The APBLBIACCBK
     */
    public String getAPBLBIACCBK() {
        return APBLBIACCBK;
    }

    /**
     * @param APBLBIACCBK The AP_BLBIACCBK
     */
    public void setAPBLBIACCBK(String APBLBIACCBK) {
        this.APBLBIACCBK = APBLBIACCBK;
    }

    /**
     * @return The APBLBIOCBK
     */
    public String getAPBLBIOCBK() {
        return APBLBIOCBK;
    }

    /**
     * @param APBLBIOCBK The AP_BLBIOCBK
     */
    public void setAPBLBIOCBK(String APBLBIOCBK) {
        this.APBLBIOCBK = APBLBIOCBK;
    }

    /**
     * @return The APBLBIMCBKS
     */
    public String getAPBLBIMCBKS() {
        return APBLBIMCBKS;
    }

    /**
     * @param APBLBIMCBKS The AP_BLBIMCBKS
     */
    public void setAPBLBIMCBKS(String APBLBIMCBKS) {
        this.APBLBIMCBKS = APBLBIMCBKS;
    }

    /**
     * @return The APGROSSSALESCURR
     */
    public String getAPGROSSSALESCURR() {
        return APGROSSSALESCURR;
    }

    /**
     * @param APGROSSSALESCURR The AP_GROSSSALESCURR
     */
    public void setAPGROSSSALESCURR(String APGROSSSALESCURR) {
        this.APGROSSSALESCURR = APGROSSSALESCURR;
    }

    /**
     * @return The APMIGRATIONVER
     */
    public String getAPMIGRATIONVER() {
        return APMIGRATIONVER;
    }

    /**
     * @param APMIGRATIONVER The AP_MIGRATIONVER
     */
    public void setAPMIGRATIONVER(String APMIGRATIONVER) {
        this.APMIGRATIONVER = APMIGRATIONVER;
    }

    /**
     * @return The APCCOBRANCH
     */
    public String getAPCCOBRANCH() {
        return APCCOBRANCH;
    }

    /**
     * @param APCCOBRANCH The AP_CCOBRANCH
     */
    public void setAPCCOBRANCH(String APCCOBRANCH) {
        this.APCCOBRANCH = APCCOBRANCH;
    }

    /**
     * @return The APLASTFWDBUBY
     */
    public String getAPLASTFWDBUBY() {
        return APLASTFWDBUBY;
    }

    /**
     * @param APLASTFWDBUBY The AP_LASTFWDBUBY
     */
    public void setAPLASTFWDBUBY(String APLASTFWDBUBY) {
        this.APLASTFWDBUBY = APLASTFWDBUBY;
    }

    /**
     * @return The APSKBNGPASAR
     */
    public String getAPSKBNGPASAR() {
        return APSKBNGPASAR;
    }

    /**
     * @param APSKBNGPASAR The AP_SKBNGPASAR
     */
    public void setAPSKBNGPASAR(String APSKBNGPASAR) {
        this.APSKBNGPASAR = APSKBNGPASAR;
    }

    /**
     * @return The APSKBNGDIMINTA
     */
    public String getAPSKBNGDIMINTA() {
        return APSKBNGDIMINTA;
    }

    /**
     * @param APSKBNGDIMINTA The AP_SKBNGDIMINTA
     */
    public void setAPSKBNGDIMINTA(String APSKBNGDIMINTA) {
        this.APSKBNGDIMINTA = APSKBNGDIMINTA;
    }

    /**
     * @return The APBLBIACCBK12BLN
     */
    public String getAPBLBIACCBK12BLN() {
        return APBLBIACCBK12BLN;
    }

    /**
     * @param APBLBIACCBK12BLN The AP_BLBIACCBK12BLN
     */
    public void setAPBLBIACCBK12BLN(String APBLBIACCBK12BLN) {
        this.APBLBIACCBK12BLN = APBLBIACCBK12BLN;
    }

    /**
     * @return The APBLBIOCBK12BLN
     */
    public String getAPBLBIOCBK12BLN() {
        return APBLBIOCBK12BLN;
    }

    /**
     * @param APBLBIOCBK12BLN The AP_BLBIOCBK12BLN
     */
    public void setAPBLBIOCBK12BLN(String APBLBIOCBK12BLN) {
        this.APBLBIOCBK12BLN = APBLBIOCBK12BLN;
    }

    /**
     * @return The APBLBIMCBKS12BLN
     */
    public String getAPBLBIMCBKS12BLN() {
        return APBLBIMCBKS12BLN;
    }

    /**
     * @param APBLBIMCBKS12BLN The AP_BLBIMCBKS12BLN
     */
    public void setAPBLBIMCBKS12BLN(String APBLBIMCBKS12BLN) {
        this.APBLBIMCBKS12BLN = APBLBIMCBKS12BLN;
    }

    /**
     * @return The APSRCNAME
     */
    public String getAPSRCNAME() {
        return APSRCNAME;
    }

    /**
     * @param APSRCNAME The AP_SRCNAME
     */
    public void setAPSRCNAME(String APSRCNAME) {
        this.APSRCNAME = APSRCNAME;
    }

    /**
     * @return The CBI
     */
    public String getCBI() {
        return CBI;
    }

    /**
     * @param CBI The CBI
     */
    public void setCBI(String CBI) {
        this.CBI = CBI;
    }

    /**
     * @return The APSURATNSBNO
     */
    public String getAPSURATNSBNO() {
        return APSURATNSBNO;
    }

    /**
     * @param APSURATNSBNO The AP_SURATNSBNO
     */
    public void setAPSURATNSBNO(String APSURATNSBNO) {
        this.APSURATNSBNO = APSURATNSBNO;
    }

    /**
     * @return The APSURATNSBTGL
     */
    public String getAPSURATNSBTGL() {
        return APSURATNSBTGL;
    }

    /**
     * @param APSURATNSBTGL The AP_SURATNSBTGL
     */
    public void setAPSURATNSBTGL(String APSURATNSBTGL) {
        this.APSURATNSBTGL = APSURATNSBTGL;
    }

    /**
     * @return The APSURATNSBTGLTRM
     */
    public String getAPSURATNSBTGLTRM() {
        return APSURATNSBTGLTRM;
    }

    /**
     * @param APSURATNSBTGLTRM The AP_SURATNSBTGLTRM
     */
    public void setAPSURATNSBTGLTRM(String APSURATNSBTGLTRM) {
        this.APSURATNSBTGLTRM = APSURATNSBTGLTRM;
    }

    /**
     * @return The APREGNOINDUK
     */
    public String getAPREGNOINDUK() {
        return APREGNOINDUK;
    }

    /**
     * @param APREGNOINDUK The APREGNO_INDUK
     */
    public void setAPREGNOINDUK(String APREGNOINDUK) {
        this.APREGNOINDUK = APREGNOINDUK;
    }

    /**
     * @return The PRODUCTIDINDUK
     */
    public String getPRODUCTIDINDUK() {
        return PRODUCTIDINDUK;
    }

    /**
     * @param PRODUCTIDINDUK The PRODUCTID_INDUK
     */
    public void setPRODUCTIDINDUK(String PRODUCTIDINDUK) {
        this.PRODUCTIDINDUK = PRODUCTIDINDUK;
    }

    /**
     * @return The AANOINDUK
     */
    public String getAANOINDUK() {
        return AANOINDUK;
    }

    /**
     * @param AANOINDUK The AANO_INDUK
     */
    public void setAANOINDUK(String AANOINDUK) {
        this.AANOINDUK = AANOINDUK;
    }

    /**
     * @return The ACCSEQINDUK
     */
    public String getACCSEQINDUK() {
        return ACCSEQINDUK;
    }

    /**
     * @param ACCSEQINDUK The ACCSEQ_INDUK
     */
    public void setACCSEQINDUK(String ACCSEQINDUK) {
        this.ACCSEQINDUK = ACCSEQINDUK;
    }

    /**
     * @return The PRODSEQINDUK
     */
    public String getPRODSEQINDUK() {
        return PRODSEQINDUK;
    }

    /**
     * @param PRODSEQINDUK The PRODSEQ_INDUK
     */
    public void setPRODSEQINDUK(String PRODSEQINDUK) {
        this.PRODSEQINDUK = PRODSEQINDUK;
    }

    /**
     * @return The APAPRVCOND
     */
    public String getAPAPRVCOND() {
        return APAPRVCOND;
    }

    /**
     * @param APAPRVCOND The AP_APRVCOND
     */
    public void setAPAPRVCOND(String APAPRVCOND) {
        this.APAPRVCOND = APAPRVCOND;
    }

    /**
     * @return The APAPRVCONDBY
     */
    public String getAPAPRVCONDBY() {
        return APAPRVCONDBY;
    }

    /**
     * @param APAPRVCONDBY The AP_APRVCONDBY
     */
    public void setAPAPRVCONDBY(String APAPRVCONDBY) {
        this.APAPRVCONDBY = APAPRVCONDBY;
    }

    /**
     * @return The APLOWLINE
     */
    public String getAPLOWLINE() {
        return APLOWLINE;
    }

    /**
     * @param APLOWLINE The AP_LOWLINE
     */
    public void setAPLOWLINE(String APLOWLINE) {
        this.APLOWLINE = APLOWLINE;
    }

    /**
     * @return The APRISKMNGR
     */
    public String getAPRISKMNGR() {
        return APRISKMNGR;
    }

    /**
     * @param APRISKMNGR The AP_RISKMNGR
     */
    public void setAPRISKMNGR(String APRISKMNGR) {
        this.APRISKMNGR = APRISKMNGR;
    }

    /**
     * @return The APISREADYNAK
     */
    public String getAPISREADYNAK() {
        return APISREADYNAK;
    }

    /**
     * @param APISREADYNAK The AP_ISREADYNAK
     */
    public void setAPISREADYNAK(String APISREADYNAK) {
        this.APISREADYNAK = APISREADYNAK;
    }

    /**
     * @return The APNERACALASTDATE
     */
    public String getAPNERACALASTDATE() {
        return APNERACALASTDATE;
    }

    /**
     * @param APNERACALASTDATE The AP_NERACA_LASTDATE
     */
    public void setAPNERACALASTDATE(String APNERACALASTDATE) {
        this.APNERACALASTDATE = APNERACALASTDATE;
    }

    /**
     * @return The APLABARUGILASTDATE
     */
    public String getAPLABARUGILASTDATE() {
        return APLABARUGILASTDATE;
    }

    /**
     * @param APLABARUGILASTDATE The AP_LABARUGI_LASTDATE
     */
    public void setAPLABARUGILASTDATE(String APLABARUGILASTDATE) {
        this.APLABARUGILASTDATE = APLABARUGILASTDATE;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(APREGNO);
        dest.writeValue(CUREF);
        dest.writeValue(PROGCODE);
        dest.writeValue(AREAID);
        dest.writeValue(BRANCHCODE);
        dest.writeValue(CHANNELCODE);
        dest.writeValue(AANO);
        dest.writeValue(APSRCCODE);
        dest.writeValue(APGROSSSALES);
        dest.writeValue(APSIGNDATE);
        dest.writeValue(APRECVDATE);
        dest.writeValue(APDATALNGKP);
        dest.writeValue(APRELMNGR);
        dest.writeValue(APSALESAGENCY);
        dest.writeValue(APSALESSUPERV);
        dest.writeValue(APSALESEXEC);
        dest.writeValue(APLIMITEXPOSURE);
        dest.writeValue(APUSERNAME);
        dest.writeValue(APCANCEL);
        dest.writeValue(APREJECT);
        dest.writeValue(APAPPRSTATUS);
        dest.writeValue(APSITEVISITSTA);
        dest.writeValue(APISAPPEAL);
        dest.writeValue(APISAPPEALBY);
        dest.writeValue(APISAPPEALDATE);
        dest.writeValue(APCOMPLEVEL);
        dest.writeValue(APGROUPCOMPANY);
        dest.writeValue(APAPRVNEXTBY);
        dest.writeValue(APAPPEAL);
        dest.writeValue(APJOINAPPROVAL);
        dest.writeValue(APJOINAPRVSEQ);
        dest.writeValue(APAPRVUP);
        dest.writeValue(APCONFIRMBOOK);
        dest.writeValue(APBOOKFLAG);
        dest.writeValue(APPRRKBY);
        dest.writeValue(APBLMEMO);
        dest.writeValue(APACQINFO);
        dest.writeValue(APBUSINESSUNIT);
        dest.writeValue(APBOOKINGBRANCH);
        dest.writeValue(APTEAMLEADER);
        dest.writeValue(APAPRVUNTIL);
        dest.writeValue(APAPRVEYES);
        dest.writeValue(APAPRVREMARK);
        dest.writeValue(APCHECKBI);
        dest.writeValue(APCO);
        dest.writeValue(APCA);
        dest.writeValue(APACQINFOBY);
        dest.writeValue(APACQINFODATE);
        dest.writeValue(APMAINAPREGNO);
        dest.writeValue(APBLBMPEMILIK);
        dest.writeValue(APBLBMMGMT);
        dest.writeValue(APBLBMUSAHA);
        dest.writeValue(APBLBIPEMILIK);
        dest.writeValue(APBLBIMGMT);
        dest.writeValue(APBLBIUSAHA);
        dest.writeValue(APCHECKBIGROUP);
        dest.writeValue(APBLBIPERNAH);
        dest.writeValue(APBLBMPERNAH);
        dest.writeValue(APAPRVCOMMITEE);
        dest.writeValue(APBLBIACCBK);
        dest.writeValue(APBLBIOCBK);
        dest.writeValue(APBLBIMCBKS);
        dest.writeValue(APGROSSSALESCURR);
        dest.writeValue(APMIGRATIONVER);
        dest.writeValue(APCCOBRANCH);
        dest.writeValue(APLASTFWDBUBY);
        dest.writeValue(APSKBNGPASAR);
        dest.writeValue(APSKBNGDIMINTA);
        dest.writeValue(APBLBIACCBK12BLN);
        dest.writeValue(APBLBIOCBK12BLN);
        dest.writeValue(APBLBIMCBKS12BLN);
        dest.writeValue(APSRCNAME);
        dest.writeValue(CBI);
        dest.writeValue(APSURATNSBNO);
        dest.writeValue(APSURATNSBTGL);
        dest.writeValue(APSURATNSBTGLTRM);
        dest.writeValue(APREGNOINDUK);
        dest.writeValue(PRODUCTIDINDUK);
        dest.writeValue(AANOINDUK);
        dest.writeValue(ACCSEQINDUK);
        dest.writeValue(PRODSEQINDUK);
        dest.writeValue(APAPRVCOND);
        dest.writeValue(APAPRVCONDBY);
        dest.writeValue(APLOWLINE);
        dest.writeValue(APRISKMNGR);
        dest.writeValue(APISREADYNAK);
        dest.writeValue(APNERACALASTDATE);
        dest.writeValue(APLABARUGILASTDATE);
    }

    public int describeContents() {
        return 0;
    }

}
