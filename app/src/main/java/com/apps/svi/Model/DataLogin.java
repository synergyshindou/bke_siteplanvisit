package com.apps.svi.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataLogin implements Parcelable {

    public final static Creator<DataLogin> CREATOR = new Creator<DataLogin>() {


        @SuppressWarnings({
                "unchecked"
        })
        public DataLogin createFromParcel(Parcel in) {
            DataLogin instance = new DataLogin();
            instance.Username = ((String) in.readValue((String.class.getClassLoader())));
            instance.GroupId = ((String) in.readValue((String.class.getClassLoader())));
            instance.Email = ((String) in.readValue((String.class.getClassLoader())));
            instance.TransactionToken = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public DataLogin[] newArray(int size) {
            return (new DataLogin[size]);
        }

    };
    @SerializedName("Username")
    @Expose
    private String Username;
    @SerializedName("GroupId")
    @Expose
    private String GroupId;
    @SerializedName("Email")
    @Expose
    private String Email;
    @SerializedName("TransactionToken")
    @Expose
    private String TransactionToken;

    /**
     * @return The Username
     */
    public String getUsername() {
        return Username;
    }

    /**
     * @param Username The Username
     */
    public void setUsername(String Username) {
        this.Username = Username;
    }

    /**
     * @return The GroupId
     */
    public String getGroupId() {
        return GroupId;
    }

    /**
     * @param GroupId The GroupId
     */
    public void setGroupId(String GroupId) {
        this.GroupId = GroupId;
    }

    /**
     * @return The Email
     */
    public String getEmail() {
        return Email;
    }

    /**
     * @param Email The Email
     */
    public void setEmail(String Email) {
        this.Email = Email;
    }

    /**
     * @return The TransactionToken
     */
    public String getTransactionToken() {
        return TransactionToken;
    }

    /**
     * @param TransactionToken The TransactionToken
     */
    public void setTransactionToken(String TransactionToken) {
        this.TransactionToken = TransactionToken;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(Username);
        dest.writeValue(GroupId);
        dest.writeValue(Email);
        dest.writeValue(TransactionToken);
    }

    public int describeContents() {
        return 0;
    }

}
