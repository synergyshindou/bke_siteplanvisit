package com.apps.svi.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class SearchApplicationResponse implements Parcelable {

    public final static Parcelable.Creator<SearchApplicationResponse> CREATOR = new Creator<SearchApplicationResponse>() {


        @SuppressWarnings({
                "unchecked"
        })
        public SearchApplicationResponse createFromParcel(Parcel in) {
            SearchApplicationResponse instance = new SearchApplicationResponse();
            in.readList(instance.dataApplication, (com.apps.svi.Model.DataApplication.class.getClassLoader()));
            instance.response = ((String) in.readValue((String.class.getClassLoader())));
            instance.responseText = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public SearchApplicationResponse[] newArray(int size) {
            return (new SearchApplicationResponse[size]);
        }

    };
    @SerializedName("data")
    @Expose
    private List<DataApplication> dataApplication = new ArrayList<DataApplication>();
    @SerializedName("response")
    @Expose
    private String response;
    @SerializedName("responseText")
    @Expose
    private String responseText;

    /**
     * @return The dataApplication
     */
    public List<DataApplication> getDataApplication() {
        return dataApplication;
    }

    /**
     * @param dataApplication The dataApplication
     */
    public void setDataApplication(List<DataApplication> dataApplication) {
        this.dataApplication = dataApplication;
    }

    /**
     * @return The response
     */
    public String getResponse() {
        return response;
    }

    /**
     * @param response The response
     */
    public void setResponse(String response) {
        this.response = response;
    }

    /**
     * @return The responseText
     */
    public String getResponseText() {
        return responseText;
    }

    /**
     * @param responseText The responseText
     */
    public void setResponseText(String responseText) {
        this.responseText = responseText;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(dataApplication);
        dest.writeValue(response);
        dest.writeValue(responseText);
    }

    public int describeContents() {
        return 0;
    }

}
