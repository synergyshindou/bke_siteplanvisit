package com.apps.svi.Model;

/**
 * Created by Dell_Cleva on 22/04/2019.
 */

public class CompanySiteVisitEntry {
//    {
//            "AP_REGNO": "03102006CBC10000001C",
//            "CU_REF": "03102006CBC10000001",
//            "SV_DATE": "2019-04-18",
//            "SV_OFC_NAME_WORK": "PT ADAM AIR",
//            "SV_OFC_YEAR_WORK": "5",
//            "SV_OFC_INCOME_MARGIN": 10
//    }

    String AP_REGNO;
    String CU_REF;
    String SV_DATE;
    String SV_OFC_NAME_WORK;
    String SV_OFC_YEAR_WORK;
    double SV_OFC_INCOME_MARGIN;

    public CompanySiteVisitEntry() {
    }

    public String getAP_REGNO() {
        return AP_REGNO;
    }

    public void setAP_REGNO(String AP_REGNO) {
        this.AP_REGNO = AP_REGNO;
    }

    public String getCU_REF() {
        return CU_REF;
    }

    public void setCU_REF(String CU_REF) {
        this.CU_REF = CU_REF;
    }

    public String getSV_DATE() {
        return SV_DATE;
    }

    public void setSV_DATE(String SV_DATE) {
        this.SV_DATE = SV_DATE;
    }

    public String getSV_OFC_NAME_WORK() {
        return SV_OFC_NAME_WORK;
    }

    public void setSV_OFC_NAME_WORK(String SV_OFC_NAME_WORK) {
        this.SV_OFC_NAME_WORK = SV_OFC_NAME_WORK;
    }

    public String getSV_OFC_YEAR_WORK() {
        return SV_OFC_YEAR_WORK;
    }

    public void setSV_OFC_YEAR_WORK(String SV_OFC_YEAR_WORK) {
        this.SV_OFC_YEAR_WORK = SV_OFC_YEAR_WORK;
    }

    public double getSV_OFC_INCOME_MARGIN() {
        return SV_OFC_INCOME_MARGIN;
    }

    public void setSV_OFC_INCOME_MARGIN(double SV_OFC_INCOME_MARGIN) {
        this.SV_OFC_INCOME_MARGIN = SV_OFC_INCOME_MARGIN;
    }
}
