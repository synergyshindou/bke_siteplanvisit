package com.apps.svi.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataSiteVisit implements Parcelable {

    public final static Parcelable.Creator<DataSiteVisit> CREATOR = new Creator<DataSiteVisit>() {


        @SuppressWarnings({
                "unchecked"
        })
        public DataSiteVisit createFromParcel(Parcel in) {
            DataSiteVisit instance = new DataSiteVisit();
            instance.APREGNO = ((String) in.readValue((String.class.getClassLoader())));
            instance.CUREF = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVDATE = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVNAME = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVTUJUAN = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVNASABAH = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVBANK = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVOFFICE = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVFACTORY = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVMANAGEMENT = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVPRODUKSI = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVPEMASARAN = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVKEUANGAN = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVAGUNAN = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVPERSOALAN = ((String) in.readValue((String.class.getClassLoader())));
            instance.TGDATE = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVTARGETDATE = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSINVESTIGASIDATE = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSPEMBERIKET1 = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSHUBPEMBERIKET1 = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSPEMBERIKET2 = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSHUBPEMBERIKET2 = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSNMPEMOHON1 = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSNMPEMOHON2 = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSNMPEMOHON3 = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSBIRTHDATE = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSSEXTYP = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSEMAIL = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSADDRKTP1 = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSADDRKTP2 = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSADDRKTP3 = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSZIPKTP = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSCITYCODEKTP = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSCITYKTP = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSADDR1 = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSADDR2 = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSADDR3 = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSZIP = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSCITYCODE = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSCITY = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSNOTLPAREA = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSNOTLP = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSNOHPAREA = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSNOHP = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSMARRIAGE = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSCOUPLENM1 = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSCOUPLENM2 = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSCOUPLENM3 = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSCOUPLEJOB = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSMOTHERNM1 = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSMOTHERNM2 = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSMOTHERNM3 = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSJMLTERTANGGUNG = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSDEBITURTYP = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSADDRPLUS1 = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSADDRPLUS2 = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSADDRPLUS3 = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSZIPPLUS = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSCITYCODEPLUS = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSCITYPLUS = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSNOTLPAREAPLUS = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSNOTLPPLUS = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSNOHPAREAPLUS = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSNOHPPLUS = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSNMFAMS1 = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSNMFAMS2 = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSNMFAMS3 = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSADDRFAMS1 = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSADDRFAMS2 = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSADDRFAMS3 = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSZIPFAMS = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSCITYCODEFAMS = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSCITYFAMS = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSNOTLPAREAFAMS = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSNOTLPFAMS = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSNOOFFICEAREAFAMS = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSNOOFFICEFAMS = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSNOOFFICEEXTFAMS = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSNOHPAREAFAMS = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSNOHPFAMS = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSHUBFAMS = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSSTSHOMESTAY = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSCEKARSIPSTAY = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSAGUNANSTAY = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSDAYSTAY = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSMONTHSTAY = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSBANGUNANTYPESTAY = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSBANGUNANLOKASISTAY = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSBANGUNANCONDSTAY = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSFASILITASSTAY = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSBARANGHOMESTAY = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSAKSESROADSTAY = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSLINGKUNGANSTAY = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSLUASTANAHSTAY = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSLUASBANGUNANSTAY = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSGARASISTAY = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSCARPORTSTAY = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSVEHICLESTAY = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSVEHICLETYPESTAY = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSVEHICLECOUNTSTAY = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSVEHICLECONDSTAY = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVHSKETERANGANSTAY = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVOFCPEMBERIKET1 = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVOFCPOSISIPEMBERIKET1 = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVOFCPEMBERIKET2 = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVOFCPOSISIPEMBERIKET2 = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVOFCTYPOFFICE = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVOFCNMOFFICE = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVOFCADDROFFICE1 = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVOFCADDROFFICE2 = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVOFCADDROFFICE3 = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVOFCZIPCODEOFFICE = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVOFCCITYCODEOFFICE = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVOFCCITYOFFICE = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVOFCN0TLPAREAOFFICE = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVOFCN0TLPOFFICE = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVOFCEXTOFFICE = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVOFCN0FAXAREAOFFICE = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVOFCN0FAXOFFICE = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVOFCYEAROFFICE = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVOFCUSAHAOFFICE = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVOFCSTAFOFFICE = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVOFCSCALEOFFICE = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVOFCBANGUNANOFFICE = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVOFCLOKASIOFFICE = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVOFCKONDISIOFFICE = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVOFCOWNEROFFICE = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVOFCINVESTIGASIDATE = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVOFCTYPOFFICEPLUS = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVOFCNMOFFICEPLUS = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVOFCPOSISIPEMOHONPLUS = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVOFCNOTLPAREAPLUS = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVOFCNOTLPPLUS = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVOFCNOFAXAREAPLUS = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVOFCNOFAXPLUS = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVOFCADDRPLUS1 = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVOFCADDRPLUS2 = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVOFCADDRPLUS3 = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVOFCZIPCODEPLUS = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVOFCCITYCODEPLUS = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVOFCCITYPLUS = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVOFCNAMEWORK = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVOFCPOSISIWORK = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVOFCYEARWORK = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVOFCMONTHWORK = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVOFCSTATUSWORK = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVOFCUNITWORK = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVOFCKINERJAWORK = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVOFCOFFICENMHISTORY = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVOFCNOTLPAREAHISTORY = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVOFCNOTLPHISTORY = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVOFCOFFICEYEARHISTORY = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVOFCOFFICEMONTHHISTORY = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVOFCINCOMEBRUTOPEMOHON = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVOFCINCOMENETTOPEMOHON = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVOFCOTHERINCOMEPEMOHON = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVOFCTOTALINCOMEPEMOHON = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVOFCPAYPEMOHON = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVOFCINCOMEBRUTOSPOUSE = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVOFCINCOMENETTOSPOUSE = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVOFCOTHERINCOMESPOUSE = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVOFCTOTALINCOMESPOUSE = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVOFCPAYSPOUSE = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVOFCINCOMEMARGIN = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVOFCPEMOHONSUMMARY = ((String) in.readValue((String.class.getClassLoader())));
            instance.SVOFCOTHERSUMMARY = ((String) in.readValue((String.class.getClassLoader())));
            instance.TIPEINCOME = ((String) in.readValue((String.class.getClassLoader())));
            instance.PERHARI = ((String) in.readValue((String.class.getClassLoader())));
            instance.PERBULAN = ((String) in.readValue((String.class.getClassLoader())));
            instance.PERTAHUN = ((String) in.readValue((String.class.getClassLoader())));
            instance.CASHFLOW = ((String) in.readValue((String.class.getClassLoader())));
            instance.REFLEKSIKEUANGAN = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public DataSiteVisit[] newArray(int size) {
            return (new DataSiteVisit[size]);
        }

    };
    @SerializedName("AP_REGNO")
    @Expose
    private String APREGNO;
    @SerializedName("CU_REF")
    @Expose
    private String CUREF;
    @SerializedName("SV_DATE")
    @Expose
    private String SVDATE;
    @SerializedName("SV_NAME")
    @Expose
    private String SVNAME;
    @SerializedName("SV_TUJUAN")
    @Expose
    private String SVTUJUAN;
    @SerializedName("SV_NASABAH")
    @Expose
    private String SVNASABAH;
    @SerializedName("SV_BANK")
    @Expose
    private String SVBANK;
    @SerializedName("SV_OFFICE")
    @Expose
    private String SVOFFICE;
    @SerializedName("SV_FACTORY")
    @Expose
    private String SVFACTORY;
    @SerializedName("SV_MANAGEMENT")
    @Expose
    private String SVMANAGEMENT;
    @SerializedName("SV_PRODUKSI")
    @Expose
    private String SVPRODUKSI;
    @SerializedName("SV_PEMASARAN")
    @Expose
    private String SVPEMASARAN;
    @SerializedName("SV_KEUANGAN")
    @Expose
    private String SVKEUANGAN;
    @SerializedName("SV_AGUNAN")
    @Expose
    private String SVAGUNAN;
    @SerializedName("SV_PERSOALAN")
    @Expose
    private String SVPERSOALAN;
    @SerializedName("TG_DATE")
    @Expose
    private String TGDATE;
    @SerializedName("SV_TARGETDATE")
    @Expose
    private String SVTARGETDATE;
    @SerializedName("SV_HS_INVESTIGASI_DATE")
    @Expose
    private String SVHSINVESTIGASIDATE;
    @SerializedName("SV_HS_PEMBERI_KET1")
    @Expose
    private String SVHSPEMBERIKET1;
    @SerializedName("SV_HS_HUB_PEMBERI_KET1")
    @Expose
    private String SVHSHUBPEMBERIKET1;
    @SerializedName("SV_HS_PEMBERI_KET2")
    @Expose
    private String SVHSPEMBERIKET2;
    @SerializedName("SV_HS_HUB_PEMBERI_KET2")
    @Expose
    private String SVHSHUBPEMBERIKET2;
    @SerializedName("SV_HS_NM_PEMOHON1")
    @Expose
    private String SVHSNMPEMOHON1;
    @SerializedName("SV_HS_NM_PEMOHON2")
    @Expose
    private String SVHSNMPEMOHON2;
    @SerializedName("SV_HS_NM_PEMOHON3")
    @Expose
    private String SVHSNMPEMOHON3;
    @SerializedName("SV_HS_BIRTH_DATE")
    @Expose
    private String SVHSBIRTHDATE;
    @SerializedName("SV_HS_SEX_TYP")
    @Expose
    private String SVHSSEXTYP;
    @SerializedName("SV_HS_EMAIL")
    @Expose
    private String SVHSEMAIL;
    @SerializedName("SV_HS_ADDR_KTP1")
    @Expose
    private String SVHSADDRKTP1;
    @SerializedName("SV_HS_ADDR_KTP2")
    @Expose
    private String SVHSADDRKTP2;
    @SerializedName("SV_HS_ADDR_KTP3")
    @Expose
    private String SVHSADDRKTP3;
    @SerializedName("SV_HS_ZIP_KTP")
    @Expose
    private String SVHSZIPKTP;
    @SerializedName("SV_HS_CITY_CODE_KTP")
    @Expose
    private String SVHSCITYCODEKTP;
    @SerializedName("SV_HS_CITY_KTP")
    @Expose
    private String SVHSCITYKTP;
    @SerializedName("SV_HS_ADDR1")
    @Expose
    private String SVHSADDR1;
    @SerializedName("SV_HS_ADDR2")
    @Expose
    private String SVHSADDR2;
    @SerializedName("SV_HS_ADDR3")
    @Expose
    private String SVHSADDR3;
    @SerializedName("SV_HS_ZIP")
    @Expose
    private String SVHSZIP;
    @SerializedName("SV_HS_CITY_CODE")
    @Expose
    private String SVHSCITYCODE;
    @SerializedName("SV_HS_CITY")
    @Expose
    private String SVHSCITY;
    @SerializedName("SV_HS_NO_TLP_AREA")
    @Expose
    private String SVHSNOTLPAREA;
    @SerializedName("SV_HS_NO_TLP")
    @Expose
    private String SVHSNOTLP;
    @SerializedName("SV_HS_NO_HP_AREA")
    @Expose
    private String SVHSNOHPAREA;
    @SerializedName("SV_HS_NO_HP")
    @Expose
    private String SVHSNOHP;
    @SerializedName("SV_HS_MARRIAGE")
    @Expose
    private String SVHSMARRIAGE;
    @SerializedName("SV_HS_COUPLE_NM1")
    @Expose
    private String SVHSCOUPLENM1;
    @SerializedName("SV_HS_COUPLE_NM2")
    @Expose
    private String SVHSCOUPLENM2;
    @SerializedName("SV_HS_COUPLE_NM3")
    @Expose
    private String SVHSCOUPLENM3;
    @SerializedName("SV_HS_COUPLE_JOB")
    @Expose
    private String SVHSCOUPLEJOB;
    @SerializedName("SV_HS_MOTHER_NM1")
    @Expose
    private String SVHSMOTHERNM1;
    @SerializedName("SV_HS_MOTHER_NM2")
    @Expose
    private String SVHSMOTHERNM2;
    @SerializedName("SV_HS_MOTHER_NM3")
    @Expose
    private String SVHSMOTHERNM3;
    @SerializedName("SV_HS_JML_TERTANGGUNG")
    @Expose
    private String SVHSJMLTERTANGGUNG;
    @SerializedName("SV_HS_DEBITUR_TYP")
    @Expose
    private String SVHSDEBITURTYP;
    @SerializedName("SV_HS_ADDR_PLUS1")
    @Expose
    private String SVHSADDRPLUS1;
    @SerializedName("SV_HS_ADDR_PLUS2")
    @Expose
    private String SVHSADDRPLUS2;
    @SerializedName("SV_HS_ADDR_PLUS3")
    @Expose
    private String SVHSADDRPLUS3;
    @SerializedName("SV_HS_ZIP_PLUS")
    @Expose
    private String SVHSZIPPLUS;
    @SerializedName("SV_HS_CITY_CODE_PLUS")
    @Expose
    private String SVHSCITYCODEPLUS;
    @SerializedName("SV_HS_CITY_PLUS")
    @Expose
    private String SVHSCITYPLUS;
    @SerializedName("SV_HS_NO_TLP_AREA_PLUS")
    @Expose
    private String SVHSNOTLPAREAPLUS;
    @SerializedName("SV_HS_NO_TLP_PLUS")
    @Expose
    private String SVHSNOTLPPLUS;
    @SerializedName("SV_HS_NO_HP_AREA_PLUS")
    @Expose
    private String SVHSNOHPAREAPLUS;
    @SerializedName("SV_HS_NO_HP_PLUS")
    @Expose
    private String SVHSNOHPPLUS;
    @SerializedName("SV_HS_NM_FAMS1")
    @Expose
    private String SVHSNMFAMS1;
    @SerializedName("SV_HS_NM_FAMS2")
    @Expose
    private String SVHSNMFAMS2;
    @SerializedName("SV_HS_NM_FAMS3")
    @Expose
    private String SVHSNMFAMS3;
    @SerializedName("SV_HS_ADDR_FAMS1")
    @Expose
    private String SVHSADDRFAMS1;
    @SerializedName("SV_HS_ADDR_FAMS2")
    @Expose
    private String SVHSADDRFAMS2;
    @SerializedName("SV_HS_ADDR_FAMS3")
    @Expose
    private String SVHSADDRFAMS3;
    @SerializedName("SV_HS_ZIP_FAMS")
    @Expose
    private String SVHSZIPFAMS;
    @SerializedName("SV_HS_CITY_CODE_FAMS")
    @Expose
    private String SVHSCITYCODEFAMS;
    @SerializedName("SV_HS_CITY_FAMS")
    @Expose
    private String SVHSCITYFAMS;
    @SerializedName("SV_HS_NO_TLP_AREA_FAMS")
    @Expose
    private String SVHSNOTLPAREAFAMS;
    @SerializedName("SV_HS_NO_TLP_FAMS")
    @Expose
    private String SVHSNOTLPFAMS;
    @SerializedName("SV_HS_NO_OFFICE_AREA_FAMS")
    @Expose
    private String SVHSNOOFFICEAREAFAMS;
    @SerializedName("SV_HS_NO_OFFICE_FAMS")
    @Expose
    private String SVHSNOOFFICEFAMS;
    @SerializedName("SV_HS_NO_OFFICE_EXT_FAMS")
    @Expose
    private String SVHSNOOFFICEEXTFAMS;
    @SerializedName("SV_HS_NO_HP_AREA_FAMS")
    @Expose
    private String SVHSNOHPAREAFAMS;
    @SerializedName("SV_HS_NO_HP_FAMS")
    @Expose
    private String SVHSNOHPFAMS;
    @SerializedName("SV_HS_HUB_FAMS")
    @Expose
    private String SVHSHUBFAMS;
    @SerializedName("SV_HS_STS_HOME_STAY")
    @Expose
    private String SVHSSTSHOMESTAY;
    @SerializedName("SV_HS_CEK_ARSIP_STAY")
    @Expose
    private String SVHSCEKARSIPSTAY;
    @SerializedName("SV_HS_AGUNAN_STAY")
    @Expose
    private String SVHSAGUNANSTAY;
    @SerializedName("SV_HS_DAY_STAY")
    @Expose
    private String SVHSDAYSTAY;
    @SerializedName("SV_HS_MONTH_STAY")
    @Expose
    private String SVHSMONTHSTAY;
    @SerializedName("SV_HS_BANGUNAN_TYPE_STAY")
    @Expose
    private String SVHSBANGUNANTYPESTAY;
    @SerializedName("SV_HS_BANGUNAN_LOKASI_STAY")
    @Expose
    private String SVHSBANGUNANLOKASISTAY;
    @SerializedName("SV_HS_BANGUNAN_COND_STAY")
    @Expose
    private String SVHSBANGUNANCONDSTAY;
    @SerializedName("SV_HS_FASILITAS_STAY")
    @Expose
    private String SVHSFASILITASSTAY;
    @SerializedName("SV_HS_BARANG_HOME_STAY")
    @Expose
    private String SVHSBARANGHOMESTAY;
    @SerializedName("SV_HS_AKSES_ROAD_STAY")
    @Expose
    private String SVHSAKSESROADSTAY;
    @SerializedName("SV_HS_LINGKUNGAN_STAY")
    @Expose
    private String SVHSLINGKUNGANSTAY;
    @SerializedName("SV_HS_LUAS_TANAH_STAY")
    @Expose
    private String SVHSLUASTANAHSTAY;
    @SerializedName("SV_HS_LUAS_BANGUNAN_STAY")
    @Expose
    private String SVHSLUASBANGUNANSTAY;
    @SerializedName("SV_HS_GARASI_STAY")
    @Expose
    private String SVHSGARASISTAY;
    @SerializedName("SV_HS_CARPORT_STAY")
    @Expose
    private String SVHSCARPORTSTAY;
    @SerializedName("SV_HS_VEHICLE_STAY")
    @Expose
    private String SVHSVEHICLESTAY;
    @SerializedName("SV_HS_VEHICLE_TYPE_STAY")
    @Expose
    private String SVHSVEHICLETYPESTAY;
    @SerializedName("SV_HS_VEHICLE_COUNT_STAY")
    @Expose
    private String SVHSVEHICLECOUNTSTAY;
    @SerializedName("SV_HS_VEHICLE_COND_STAY")
    @Expose
    private String SVHSVEHICLECONDSTAY;
    @SerializedName("SV_HS_KETERANGAN_STAY")
    @Expose
    private String SVHSKETERANGANSTAY;
    @SerializedName("SV_OFC_PEMBERI_KET1")
    @Expose
    private String SVOFCPEMBERIKET1;
    @SerializedName("SV_OFC_POSISI_PEMBERI_KET1")
    @Expose
    private String SVOFCPOSISIPEMBERIKET1;
    @SerializedName("SV_OFC_PEMBERI_KET2")
    @Expose
    private String SVOFCPEMBERIKET2;
    @SerializedName("SV_OFC_POSISI_PEMBERI_KET2")
    @Expose
    private String SVOFCPOSISIPEMBERIKET2;
    @SerializedName("SV_OFC_TYP_OFFICE")
    @Expose
    private String SVOFCTYPOFFICE;
    @SerializedName("SV_OFC_NM_OFFICE")
    @Expose
    private String SVOFCNMOFFICE;
    @SerializedName("SV_OFC_ADDR_OFFICE1")
    @Expose
    private String SVOFCADDROFFICE1;
    @SerializedName("SV_OFC_ADDR_OFFICE2")
    @Expose
    private String SVOFCADDROFFICE2;
    @SerializedName("SV_OFC_ADDR_OFFICE3")
    @Expose
    private String SVOFCADDROFFICE3;
    @SerializedName("SV_OFC_ZIPCODE_OFFICE")
    @Expose
    private String SVOFCZIPCODEOFFICE;
    @SerializedName("SV_OFC_CITY_CODE_OFFICE")
    @Expose
    private String SVOFCCITYCODEOFFICE;
    @SerializedName("SV_OFC_CITY_OFFICE")
    @Expose
    private String SVOFCCITYOFFICE;
    @SerializedName("SV_OFC_N0_TLP_AREA_OFFICE")
    @Expose
    private String SVOFCN0TLPAREAOFFICE;
    @SerializedName("SV_OFC_N0_TLP_OFFICE")
    @Expose
    private String SVOFCN0TLPOFFICE;
    @SerializedName("SV_OFC_EXT_OFFICE")
    @Expose
    private String SVOFCEXTOFFICE;
    @SerializedName("SV_OFC_N0_FAX_AREA_OFFICE")
    @Expose
    private String SVOFCN0FAXAREAOFFICE;
    @SerializedName("SV_OFC_N0_FAX_OFFICE")
    @Expose
    private String SVOFCN0FAXOFFICE;
    @SerializedName("SV_OFC_YEAR_OFFICE")
    @Expose
    private String SVOFCYEAROFFICE;
    @SerializedName("SV_OFC_USAHA_OFFICE")
    @Expose
    private String SVOFCUSAHAOFFICE;
    @SerializedName("SV_OFC_STAF_OFFICE")
    @Expose
    private String SVOFCSTAFOFFICE;
    @SerializedName("SV_OFC_SCALE_OFFICE")
    @Expose
    private String SVOFCSCALEOFFICE;
    @SerializedName("SV_OFC_BANGUNAN_OFFICE")
    @Expose
    private String SVOFCBANGUNANOFFICE;
    @SerializedName("SV_OFC_LOKASI_OFFICE")
    @Expose
    private String SVOFCLOKASIOFFICE;
    @SerializedName("SV_OFC_KONDISI_OFFICE")
    @Expose
    private String SVOFCKONDISIOFFICE;
    @SerializedName("SV_OFC_OWNER_OFFICE")
    @Expose
    private String SVOFCOWNEROFFICE;
    @SerializedName("SV_OFC_INVESTIGASI_DATE")
    @Expose
    private String SVOFCINVESTIGASIDATE;
    @SerializedName("SV_OFC_TYP_OFFICE_PLUS")
    @Expose
    private String SVOFCTYPOFFICEPLUS;
    @SerializedName("SV_OFC_NM_OFFICE_PLUS")
    @Expose
    private String SVOFCNMOFFICEPLUS;
    @SerializedName("SV_OFC_POSISI_PEMOHON_PLUS")
    @Expose
    private String SVOFCPOSISIPEMOHONPLUS;
    @SerializedName("SV_OFC_NO_TLP_AREA_PLUS")
    @Expose
    private String SVOFCNOTLPAREAPLUS;
    @SerializedName("SV_OFC_NO_TLP_PLUS")
    @Expose
    private String SVOFCNOTLPPLUS;
    @SerializedName("SV_OFC_NO_FAX_AREA_PLUS")
    @Expose
    private String SVOFCNOFAXAREAPLUS;
    @SerializedName("SV_OFC_NO_FAX_PLUS")
    @Expose
    private String SVOFCNOFAXPLUS;
    @SerializedName("SV_OFC_ADDR_PLUS1")
    @Expose
    private String SVOFCADDRPLUS1;
    @SerializedName("SV_OFC_ADDR_PLUS2")
    @Expose
    private String SVOFCADDRPLUS2;
    @SerializedName("SV_OFC_ADDR_PLUS3")
    @Expose
    private String SVOFCADDRPLUS3;
    @SerializedName("SV_OFC_ZIPCODE_PLUS")
    @Expose
    private String SVOFCZIPCODEPLUS;
    @SerializedName("SV_OFC_CITY_CODE_PLUS")
    @Expose
    private String SVOFCCITYCODEPLUS;
    @SerializedName("SV_OFC_CITY_PLUS")
    @Expose
    private String SVOFCCITYPLUS;
    @SerializedName("SV_OFC_NAME_WORK")
    @Expose
    private String SVOFCNAMEWORK;
    @SerializedName("SV_OFC_POSISI_WORK")
    @Expose
    private String SVOFCPOSISIWORK;
    @SerializedName("SV_OFC_YEAR_WORK")
    @Expose
    private String SVOFCYEARWORK;
    @SerializedName("SV_OFC_MONTH_WORK")
    @Expose
    private String SVOFCMONTHWORK;
    @SerializedName("SV_OFC_STATUS_WORK")
    @Expose
    private String SVOFCSTATUSWORK;
    @SerializedName("SV_OFC_UNIT_WORK")
    @Expose
    private String SVOFCUNITWORK;
    @SerializedName("SV_OFC_KINERJA_WORK")
    @Expose
    private String SVOFCKINERJAWORK;
    @SerializedName("SV_OFC_OFFICE_NM_HISTORY")
    @Expose
    private String SVOFCOFFICENMHISTORY;
    @SerializedName("SV_OFC_NO_TLP_AREA_HISTORY")
    @Expose
    private String SVOFCNOTLPAREAHISTORY;
    @SerializedName("SV_OFC_NO_TLP_HISTORY")
    @Expose
    private String SVOFCNOTLPHISTORY;
    @SerializedName("SV_OFC_OFFICE_YEAR_HISTORY")
    @Expose
    private String SVOFCOFFICEYEARHISTORY;
    @SerializedName("SV_OFC_OFFICE_MONTH_HISTORY")
    @Expose
    private String SVOFCOFFICEMONTHHISTORY;
    @SerializedName("SV_OFC_INCOME_BRUTO_PEMOHON")
    @Expose
    private String SVOFCINCOMEBRUTOPEMOHON;
    @SerializedName("SV_OFC_INCOME_NETTO_PEMOHON")
    @Expose
    private String SVOFCINCOMENETTOPEMOHON;
    @SerializedName("SV_OFC_OTHER_INCOME_PEMOHON")
    @Expose
    private String SVOFCOTHERINCOMEPEMOHON;
    @SerializedName("SV_OFC_TOTAL_INCOME_PEMOHON")
    @Expose
    private String SVOFCTOTALINCOMEPEMOHON;
    @SerializedName("SV_OFC_PAY_PEMOHON")
    @Expose
    private String SVOFCPAYPEMOHON;
    @SerializedName("SV_OFC_INCOME_BRUTO_SPOUSE")
    @Expose
    private String SVOFCINCOMEBRUTOSPOUSE;
    @SerializedName("SV_OFC_INCOME_NETTO_SPOUSE")
    @Expose
    private String SVOFCINCOMENETTOSPOUSE;
    @SerializedName("SV_OFC_OTHER_INCOME_SPOUSE")
    @Expose
    private String SVOFCOTHERINCOMESPOUSE;
    @SerializedName("SV_OFC_TOTAL_INCOME_SPOUSE")
    @Expose
    private String SVOFCTOTALINCOMESPOUSE;
    @SerializedName("SV_OFC_PAY_SPOUSE")
    @Expose
    private String SVOFCPAYSPOUSE;
    @SerializedName("SV_OFC_INCOME_MARGIN")
    @Expose
    private String SVOFCINCOMEMARGIN;
    @SerializedName("SV_OFC_PEMOHON_SUMMARY")
    @Expose
    private String SVOFCPEMOHONSUMMARY;
    @SerializedName("SV_OFC_OTHER_SUMMARY")
    @Expose
    private String SVOFCOTHERSUMMARY;
    @SerializedName("TIPE_INCOME")
    @Expose
    private String TIPEINCOME;
    @SerializedName("PER_HARI")
    @Expose
    private String PERHARI;
    @SerializedName("PER_BULAN")
    @Expose
    private String PERBULAN;
    @SerializedName("PER_TAHUN")
    @Expose
    private String PERTAHUN;
    @SerializedName("CASH_FLOW")
    @Expose
    private String CASHFLOW;
    @SerializedName("REFLEKSI_KEUANGAN")
    @Expose
    private String REFLEKSIKEUANGAN;

    /**
     * @return The APREGNO
     */
    public String getAPREGNO() {
        return APREGNO;
    }

    /**
     * @param APREGNO The AP_REGNO
     */
    public void setAPREGNO(String APREGNO) {
        this.APREGNO = APREGNO;
    }

    /**
     * @return The CUREF
     */
    public String getCUREF() {
        return CUREF;
    }

    /**
     * @param CUREF The CU_REF
     */
    public void setCUREF(String CUREF) {
        this.CUREF = CUREF;
    }

    /**
     * @return The SVDATE
     */
    public String getSVDATE() {
        return SVDATE;
    }

    /**
     * @param SVDATE The SV_DATE
     */
    public void setSVDATE(String SVDATE) {
        this.SVDATE = SVDATE;
    }

    /**
     * @return The SVNAME
     */
    public String getSVNAME() {
        return SVNAME;
    }

    /**
     * @param SVNAME The SV_NAME
     */
    public void setSVNAME(String SVNAME) {
        this.SVNAME = SVNAME;
    }

    /**
     * @return The SVTUJUAN
     */
    public String getSVTUJUAN() {
        return SVTUJUAN;
    }

    /**
     * @param SVTUJUAN The SV_TUJUAN
     */
    public void setSVTUJUAN(String SVTUJUAN) {
        this.SVTUJUAN = SVTUJUAN;
    }

    /**
     * @return The SVNASABAH
     */
    public String getSVNASABAH() {
        return SVNASABAH;
    }

    /**
     * @param SVNASABAH The SV_NASABAH
     */
    public void setSVNASABAH(String SVNASABAH) {
        this.SVNASABAH = SVNASABAH;
    }

    /**
     * @return The SVBANK
     */
    public String getSVBANK() {
        return SVBANK;
    }

    /**
     * @param SVBANK The SV_BANK
     */
    public void setSVBANK(String SVBANK) {
        this.SVBANK = SVBANK;
    }

    /**
     * @return The SVOFFICE
     */
    public String getSVOFFICE() {
        return SVOFFICE;
    }

    /**
     * @param SVOFFICE The SV_OFFICE
     */
    public void setSVOFFICE(String SVOFFICE) {
        this.SVOFFICE = SVOFFICE;
    }

    /**
     * @return The SVFACTORY
     */
    public String getSVFACTORY() {
        return SVFACTORY;
    }

    /**
     * @param SVFACTORY The SV_FACTORY
     */
    public void setSVFACTORY(String SVFACTORY) {
        this.SVFACTORY = SVFACTORY;
    }

    /**
     * @return The SVMANAGEMENT
     */
    public String getSVMANAGEMENT() {
        return SVMANAGEMENT;
    }

    /**
     * @param SVMANAGEMENT The SV_MANAGEMENT
     */
    public void setSVMANAGEMENT(String SVMANAGEMENT) {
        this.SVMANAGEMENT = SVMANAGEMENT;
    }

    /**
     * @return The SVPRODUKSI
     */
    public String getSVPRODUKSI() {
        return SVPRODUKSI;
    }

    /**
     * @param SVPRODUKSI The SV_PRODUKSI
     */
    public void setSVPRODUKSI(String SVPRODUKSI) {
        this.SVPRODUKSI = SVPRODUKSI;
    }

    /**
     * @return The SVPEMASARAN
     */
    public String getSVPEMASARAN() {
        return SVPEMASARAN;
    }

    /**
     * @param SVPEMASARAN The SV_PEMASARAN
     */
    public void setSVPEMASARAN(String SVPEMASARAN) {
        this.SVPEMASARAN = SVPEMASARAN;
    }

    /**
     * @return The SVKEUANGAN
     */
    public String getSVKEUANGAN() {
        return SVKEUANGAN;
    }

    /**
     * @param SVKEUANGAN The SV_KEUANGAN
     */
    public void setSVKEUANGAN(String SVKEUANGAN) {
        this.SVKEUANGAN = SVKEUANGAN;
    }

    /**
     * @return The SVAGUNAN
     */
    public String getSVAGUNAN() {
        return SVAGUNAN;
    }

    /**
     * @param SVAGUNAN The SV_AGUNAN
     */
    public void setSVAGUNAN(String SVAGUNAN) {
        this.SVAGUNAN = SVAGUNAN;
    }

    /**
     * @return The SVPERSOALAN
     */
    public String getSVPERSOALAN() {
        return SVPERSOALAN;
    }

    /**
     * @param SVPERSOALAN The SV_PERSOALAN
     */
    public void setSVPERSOALAN(String SVPERSOALAN) {
        this.SVPERSOALAN = SVPERSOALAN;
    }

    /**
     * @return The TGDATE
     */
    public String getTGDATE() {
        return TGDATE;
    }

    /**
     * @param TGDATE The TG_DATE
     */
    public void setTGDATE(String TGDATE) {
        this.TGDATE = TGDATE;
    }

    /**
     * @return The SVTARGETDATE
     */
    public String getSVTARGETDATE() {
        return SVTARGETDATE;
    }

    /**
     * @param SVTARGETDATE The SV_TARGETDATE
     */
    public void setSVTARGETDATE(String SVTARGETDATE) {
        this.SVTARGETDATE = SVTARGETDATE;
    }

    /**
     * @return The SVHSINVESTIGASIDATE
     */
    public String getSVHSINVESTIGASIDATE() {
        return SVHSINVESTIGASIDATE;
    }

    /**
     * @param SVHSINVESTIGASIDATE The SV_HS_INVESTIGASI_DATE
     */
    public void setSVHSINVESTIGASIDATE(String SVHSINVESTIGASIDATE) {
        this.SVHSINVESTIGASIDATE = SVHSINVESTIGASIDATE;
    }

    /**
     * @return The SVHSPEMBERIKET1
     */
    public String getSVHSPEMBERIKET1() {
        return SVHSPEMBERIKET1;
    }

    /**
     * @param SVHSPEMBERIKET1 The SV_HS_PEMBERI_KET1
     */
    public void setSVHSPEMBERIKET1(String SVHSPEMBERIKET1) {
        this.SVHSPEMBERIKET1 = SVHSPEMBERIKET1;
    }

    /**
     * @return The SVHSHUBPEMBERIKET1
     */
    public String getSVHSHUBPEMBERIKET1() {
        return SVHSHUBPEMBERIKET1;
    }

    /**
     * @param SVHSHUBPEMBERIKET1 The SV_HS_HUB_PEMBERI_KET1
     */
    public void setSVHSHUBPEMBERIKET1(String SVHSHUBPEMBERIKET1) {
        this.SVHSHUBPEMBERIKET1 = SVHSHUBPEMBERIKET1;
    }

    /**
     * @return The SVHSPEMBERIKET2
     */
    public String getSVHSPEMBERIKET2() {
        return SVHSPEMBERIKET2;
    }

    /**
     * @param SVHSPEMBERIKET2 The SV_HS_PEMBERI_KET2
     */
    public void setSVHSPEMBERIKET2(String SVHSPEMBERIKET2) {
        this.SVHSPEMBERIKET2 = SVHSPEMBERIKET2;
    }

    /**
     * @return The SVHSHUBPEMBERIKET2
     */
    public String getSVHSHUBPEMBERIKET2() {
        return SVHSHUBPEMBERIKET2;
    }

    /**
     * @param SVHSHUBPEMBERIKET2 The SV_HS_HUB_PEMBERI_KET2
     */
    public void setSVHSHUBPEMBERIKET2(String SVHSHUBPEMBERIKET2) {
        this.SVHSHUBPEMBERIKET2 = SVHSHUBPEMBERIKET2;
    }

    /**
     * @return The SVHSNMPEMOHON1
     */
    public String getSVHSNMPEMOHON1() {
        return SVHSNMPEMOHON1;
    }

    /**
     * @param SVHSNMPEMOHON1 The SV_HS_NM_PEMOHON1
     */
    public void setSVHSNMPEMOHON1(String SVHSNMPEMOHON1) {
        this.SVHSNMPEMOHON1 = SVHSNMPEMOHON1;
    }

    /**
     * @return The SVHSNMPEMOHON2
     */
    public String getSVHSNMPEMOHON2() {
        return SVHSNMPEMOHON2;
    }

    /**
     * @param SVHSNMPEMOHON2 The SV_HS_NM_PEMOHON2
     */
    public void setSVHSNMPEMOHON2(String SVHSNMPEMOHON2) {
        this.SVHSNMPEMOHON2 = SVHSNMPEMOHON2;
    }

    /**
     * @return The SVHSNMPEMOHON3
     */
    public String getSVHSNMPEMOHON3() {
        return SVHSNMPEMOHON3;
    }

    /**
     * @param SVHSNMPEMOHON3 The SV_HS_NM_PEMOHON3
     */
    public void setSVHSNMPEMOHON3(String SVHSNMPEMOHON3) {
        this.SVHSNMPEMOHON3 = SVHSNMPEMOHON3;
    }

    /**
     * @return The SVHSBIRTHDATE
     */
    public String getSVHSBIRTHDATE() {
        return SVHSBIRTHDATE;
    }

    /**
     * @param SVHSBIRTHDATE The SV_HS_BIRTH_DATE
     */
    public void setSVHSBIRTHDATE(String SVHSBIRTHDATE) {
        this.SVHSBIRTHDATE = SVHSBIRTHDATE;
    }

    /**
     * @return The SVHSSEXTYP
     */
    public String getSVHSSEXTYP() {
        return SVHSSEXTYP;
    }

    /**
     * @param SVHSSEXTYP The SV_HS_SEX_TYP
     */
    public void setSVHSSEXTYP(String SVHSSEXTYP) {
        this.SVHSSEXTYP = SVHSSEXTYP;
    }

    /**
     * @return The SVHSEMAIL
     */
    public String getSVHSEMAIL() {
        return SVHSEMAIL;
    }

    /**
     * @param SVHSEMAIL The SV_HS_EMAIL
     */
    public void setSVHSEMAIL(String SVHSEMAIL) {
        this.SVHSEMAIL = SVHSEMAIL;
    }

    /**
     * @return The SVHSADDRKTP1
     */
    public String getSVHSADDRKTP1() {
        return SVHSADDRKTP1;
    }

    /**
     * @param SVHSADDRKTP1 The SV_HS_ADDR_KTP1
     */
    public void setSVHSADDRKTP1(String SVHSADDRKTP1) {
        this.SVHSADDRKTP1 = SVHSADDRKTP1;
    }

    /**
     * @return The SVHSADDRKTP2
     */
    public String getSVHSADDRKTP2() {
        return SVHSADDRKTP2;
    }

    /**
     * @param SVHSADDRKTP2 The SV_HS_ADDR_KTP2
     */
    public void setSVHSADDRKTP2(String SVHSADDRKTP2) {
        this.SVHSADDRKTP2 = SVHSADDRKTP2;
    }

    /**
     * @return The SVHSADDRKTP3
     */
    public String getSVHSADDRKTP3() {
        return SVHSADDRKTP3;
    }

    /**
     * @param SVHSADDRKTP3 The SV_HS_ADDR_KTP3
     */
    public void setSVHSADDRKTP3(String SVHSADDRKTP3) {
        this.SVHSADDRKTP3 = SVHSADDRKTP3;
    }

    /**
     * @return The SVHSZIPKTP
     */
    public String getSVHSZIPKTP() {
        return SVHSZIPKTP;
    }

    /**
     * @param SVHSZIPKTP The SV_HS_ZIP_KTP
     */
    public void setSVHSZIPKTP(String SVHSZIPKTP) {
        this.SVHSZIPKTP = SVHSZIPKTP;
    }

    /**
     * @return The SVHSCITYCODEKTP
     */
    public String getSVHSCITYCODEKTP() {
        return SVHSCITYCODEKTP;
    }

    /**
     * @param SVHSCITYCODEKTP The SV_HS_CITY_CODE_KTP
     */
    public void setSVHSCITYCODEKTP(String SVHSCITYCODEKTP) {
        this.SVHSCITYCODEKTP = SVHSCITYCODEKTP;
    }

    /**
     * @return The SVHSCITYKTP
     */
    public String getSVHSCITYKTP() {
        return SVHSCITYKTP;
    }

    /**
     * @param SVHSCITYKTP The SV_HS_CITY_KTP
     */
    public void setSVHSCITYKTP(String SVHSCITYKTP) {
        this.SVHSCITYKTP = SVHSCITYKTP;
    }

    /**
     * @return The SVHSADDR1
     */
    public String getSVHSADDR1() {
        return SVHSADDR1;
    }

    /**
     * @param SVHSADDR1 The SV_HS_ADDR1
     */
    public void setSVHSADDR1(String SVHSADDR1) {
        this.SVHSADDR1 = SVHSADDR1;
    }

    /**
     * @return The SVHSADDR2
     */
    public String getSVHSADDR2() {
        return SVHSADDR2;
    }

    /**
     * @param SVHSADDR2 The SV_HS_ADDR2
     */
    public void setSVHSADDR2(String SVHSADDR2) {
        this.SVHSADDR2 = SVHSADDR2;
    }

    /**
     * @return The SVHSADDR3
     */
    public String getSVHSADDR3() {
        return SVHSADDR3;
    }

    /**
     * @param SVHSADDR3 The SV_HS_ADDR3
     */
    public void setSVHSADDR3(String SVHSADDR3) {
        this.SVHSADDR3 = SVHSADDR3;
    }

    /**
     * @return The SVHSZIP
     */
    public String getSVHSZIP() {
        return SVHSZIP;
    }

    /**
     * @param SVHSZIP The SV_HS_ZIP
     */
    public void setSVHSZIP(String SVHSZIP) {
        this.SVHSZIP = SVHSZIP;
    }

    /**
     * @return The SVHSCITYCODE
     */
    public String getSVHSCITYCODE() {
        return SVHSCITYCODE;
    }

    /**
     * @param SVHSCITYCODE The SV_HS_CITY_CODE
     */
    public void setSVHSCITYCODE(String SVHSCITYCODE) {
        this.SVHSCITYCODE = SVHSCITYCODE;
    }

    /**
     * @return The SVHSCITY
     */
    public String getSVHSCITY() {
        return SVHSCITY;
    }

    /**
     * @param SVHSCITY The SV_HS_CITY
     */
    public void setSVHSCITY(String SVHSCITY) {
        this.SVHSCITY = SVHSCITY;
    }

    /**
     * @return The SVHSNOTLPAREA
     */
    public String getSVHSNOTLPAREA() {
        return SVHSNOTLPAREA;
    }

    /**
     * @param SVHSNOTLPAREA The SV_HS_NO_TLP_AREA
     */
    public void setSVHSNOTLPAREA(String SVHSNOTLPAREA) {
        this.SVHSNOTLPAREA = SVHSNOTLPAREA;
    }

    /**
     * @return The SVHSNOTLP
     */
    public String getSVHSNOTLP() {
        return SVHSNOTLP;
    }

    /**
     * @param SVHSNOTLP The SV_HS_NO_TLP
     */
    public void setSVHSNOTLP(String SVHSNOTLP) {
        this.SVHSNOTLP = SVHSNOTLP;
    }

    /**
     * @return The SVHSNOHPAREA
     */
    public String getSVHSNOHPAREA() {
        return SVHSNOHPAREA;
    }

    /**
     * @param SVHSNOHPAREA The SV_HS_NO_HP_AREA
     */
    public void setSVHSNOHPAREA(String SVHSNOHPAREA) {
        this.SVHSNOHPAREA = SVHSNOHPAREA;
    }

    /**
     * @return The SVHSNOHP
     */
    public String getSVHSNOHP() {
        return SVHSNOHP;
    }

    /**
     * @param SVHSNOHP The SV_HS_NO_HP
     */
    public void setSVHSNOHP(String SVHSNOHP) {
        this.SVHSNOHP = SVHSNOHP;
    }

    /**
     * @return The SVHSMARRIAGE
     */
    public String getSVHSMARRIAGE() {
        return SVHSMARRIAGE;
    }

    /**
     * @param SVHSMARRIAGE The SV_HS_MARRIAGE
     */
    public void setSVHSMARRIAGE(String SVHSMARRIAGE) {
        this.SVHSMARRIAGE = SVHSMARRIAGE;
    }

    /**
     * @return The SVHSCOUPLENM1
     */
    public String getSVHSCOUPLENM1() {
        return SVHSCOUPLENM1;
    }

    /**
     * @param SVHSCOUPLENM1 The SV_HS_COUPLE_NM1
     */
    public void setSVHSCOUPLENM1(String SVHSCOUPLENM1) {
        this.SVHSCOUPLENM1 = SVHSCOUPLENM1;
    }

    /**
     * @return The SVHSCOUPLENM2
     */
    public String getSVHSCOUPLENM2() {
        return SVHSCOUPLENM2;
    }

    /**
     * @param SVHSCOUPLENM2 The SV_HS_COUPLE_NM2
     */
    public void setSVHSCOUPLENM2(String SVHSCOUPLENM2) {
        this.SVHSCOUPLENM2 = SVHSCOUPLENM2;
    }

    /**
     * @return The SVHSCOUPLENM3
     */
    public String getSVHSCOUPLENM3() {
        return SVHSCOUPLENM3;
    }

    /**
     * @param SVHSCOUPLENM3 The SV_HS_COUPLE_NM3
     */
    public void setSVHSCOUPLENM3(String SVHSCOUPLENM3) {
        this.SVHSCOUPLENM3 = SVHSCOUPLENM3;
    }

    /**
     * @return The SVHSCOUPLEJOB
     */
    public String getSVHSCOUPLEJOB() {
        return SVHSCOUPLEJOB;
    }

    /**
     * @param SVHSCOUPLEJOB The SV_HS_COUPLE_JOB
     */
    public void setSVHSCOUPLEJOB(String SVHSCOUPLEJOB) {
        this.SVHSCOUPLEJOB = SVHSCOUPLEJOB;
    }

    /**
     * @return The SVHSMOTHERNM1
     */
    public String getSVHSMOTHERNM1() {
        return SVHSMOTHERNM1;
    }

    /**
     * @param SVHSMOTHERNM1 The SV_HS_MOTHER_NM1
     */
    public void setSVHSMOTHERNM1(String SVHSMOTHERNM1) {
        this.SVHSMOTHERNM1 = SVHSMOTHERNM1;
    }

    /**
     * @return The SVHSMOTHERNM2
     */
    public String getSVHSMOTHERNM2() {
        return SVHSMOTHERNM2;
    }

    /**
     * @param SVHSMOTHERNM2 The SV_HS_MOTHER_NM2
     */
    public void setSVHSMOTHERNM2(String SVHSMOTHERNM2) {
        this.SVHSMOTHERNM2 = SVHSMOTHERNM2;
    }

    /**
     * @return The SVHSMOTHERNM3
     */
    public String getSVHSMOTHERNM3() {
        return SVHSMOTHERNM3;
    }

    /**
     * @param SVHSMOTHERNM3 The SV_HS_MOTHER_NM3
     */
    public void setSVHSMOTHERNM3(String SVHSMOTHERNM3) {
        this.SVHSMOTHERNM3 = SVHSMOTHERNM3;
    }

    /**
     * @return The SVHSJMLTERTANGGUNG
     */
    public String getSVHSJMLTERTANGGUNG() {
        return SVHSJMLTERTANGGUNG;
    }

    /**
     * @param SVHSJMLTERTANGGUNG The SV_HS_JML_TERTANGGUNG
     */
    public void setSVHSJMLTERTANGGUNG(String SVHSJMLTERTANGGUNG) {
        this.SVHSJMLTERTANGGUNG = SVHSJMLTERTANGGUNG;
    }

    /**
     * @return The SVHSDEBITURTYP
     */
    public String getSVHSDEBITURTYP() {
        return SVHSDEBITURTYP;
    }

    /**
     * @param SVHSDEBITURTYP The SV_HS_DEBITUR_TYP
     */
    public void setSVHSDEBITURTYP(String SVHSDEBITURTYP) {
        this.SVHSDEBITURTYP = SVHSDEBITURTYP;
    }

    /**
     * @return The SVHSADDRPLUS1
     */
    public String getSVHSADDRPLUS1() {
        return SVHSADDRPLUS1;
    }

    /**
     * @param SVHSADDRPLUS1 The SV_HS_ADDR_PLUS1
     */
    public void setSVHSADDRPLUS1(String SVHSADDRPLUS1) {
        this.SVHSADDRPLUS1 = SVHSADDRPLUS1;
    }

    /**
     * @return The SVHSADDRPLUS2
     */
    public String getSVHSADDRPLUS2() {
        return SVHSADDRPLUS2;
    }

    /**
     * @param SVHSADDRPLUS2 The SV_HS_ADDR_PLUS2
     */
    public void setSVHSADDRPLUS2(String SVHSADDRPLUS2) {
        this.SVHSADDRPLUS2 = SVHSADDRPLUS2;
    }

    /**
     * @return The SVHSADDRPLUS3
     */
    public String getSVHSADDRPLUS3() {
        return SVHSADDRPLUS3;
    }

    /**
     * @param SVHSADDRPLUS3 The SV_HS_ADDR_PLUS3
     */
    public void setSVHSADDRPLUS3(String SVHSADDRPLUS3) {
        this.SVHSADDRPLUS3 = SVHSADDRPLUS3;
    }

    /**
     * @return The SVHSZIPPLUS
     */
    public String getSVHSZIPPLUS() {
        return SVHSZIPPLUS;
    }

    /**
     * @param SVHSZIPPLUS The SV_HS_ZIP_PLUS
     */
    public void setSVHSZIPPLUS(String SVHSZIPPLUS) {
        this.SVHSZIPPLUS = SVHSZIPPLUS;
    }

    /**
     * @return The SVHSCITYCODEPLUS
     */
    public String getSVHSCITYCODEPLUS() {
        return SVHSCITYCODEPLUS;
    }

    /**
     * @param SVHSCITYCODEPLUS The SV_HS_CITY_CODE_PLUS
     */
    public void setSVHSCITYCODEPLUS(String SVHSCITYCODEPLUS) {
        this.SVHSCITYCODEPLUS = SVHSCITYCODEPLUS;
    }

    /**
     * @return The SVHSCITYPLUS
     */
    public String getSVHSCITYPLUS() {
        return SVHSCITYPLUS;
    }

    /**
     * @param SVHSCITYPLUS The SV_HS_CITY_PLUS
     */
    public void setSVHSCITYPLUS(String SVHSCITYPLUS) {
        this.SVHSCITYPLUS = SVHSCITYPLUS;
    }

    /**
     * @return The SVHSNOTLPAREAPLUS
     */
    public String getSVHSNOTLPAREAPLUS() {
        return SVHSNOTLPAREAPLUS;
    }

    /**
     * @param SVHSNOTLPAREAPLUS The SV_HS_NO_TLP_AREA_PLUS
     */
    public void setSVHSNOTLPAREAPLUS(String SVHSNOTLPAREAPLUS) {
        this.SVHSNOTLPAREAPLUS = SVHSNOTLPAREAPLUS;
    }

    /**
     * @return The SVHSNOTLPPLUS
     */
    public String getSVHSNOTLPPLUS() {
        return SVHSNOTLPPLUS;
    }

    /**
     * @param SVHSNOTLPPLUS The SV_HS_NO_TLP_PLUS
     */
    public void setSVHSNOTLPPLUS(String SVHSNOTLPPLUS) {
        this.SVHSNOTLPPLUS = SVHSNOTLPPLUS;
    }

    /**
     * @return The SVHSNOHPAREAPLUS
     */
    public String getSVHSNOHPAREAPLUS() {
        return SVHSNOHPAREAPLUS;
    }

    /**
     * @param SVHSNOHPAREAPLUS The SV_HS_NO_HP_AREA_PLUS
     */
    public void setSVHSNOHPAREAPLUS(String SVHSNOHPAREAPLUS) {
        this.SVHSNOHPAREAPLUS = SVHSNOHPAREAPLUS;
    }

    /**
     * @return The SVHSNOHPPLUS
     */
    public String getSVHSNOHPPLUS() {
        return SVHSNOHPPLUS;
    }

    /**
     * @param SVHSNOHPPLUS The SV_HS_NO_HP_PLUS
     */
    public void setSVHSNOHPPLUS(String SVHSNOHPPLUS) {
        this.SVHSNOHPPLUS = SVHSNOHPPLUS;
    }

    /**
     * @return The SVHSNMFAMS1
     */
    public String getSVHSNMFAMS1() {
        return SVHSNMFAMS1;
    }

    /**
     * @param SVHSNMFAMS1 The SV_HS_NM_FAMS1
     */
    public void setSVHSNMFAMS1(String SVHSNMFAMS1) {
        this.SVHSNMFAMS1 = SVHSNMFAMS1;
    }

    /**
     * @return The SVHSNMFAMS2
     */
    public String getSVHSNMFAMS2() {
        return SVHSNMFAMS2;
    }

    /**
     * @param SVHSNMFAMS2 The SV_HS_NM_FAMS2
     */
    public void setSVHSNMFAMS2(String SVHSNMFAMS2) {
        this.SVHSNMFAMS2 = SVHSNMFAMS2;
    }

    /**
     * @return The SVHSNMFAMS3
     */
    public String getSVHSNMFAMS3() {
        return SVHSNMFAMS3;
    }

    /**
     * @param SVHSNMFAMS3 The SV_HS_NM_FAMS3
     */
    public void setSVHSNMFAMS3(String SVHSNMFAMS3) {
        this.SVHSNMFAMS3 = SVHSNMFAMS3;
    }

    /**
     * @return The SVHSADDRFAMS1
     */
    public String getSVHSADDRFAMS1() {
        return SVHSADDRFAMS1;
    }

    /**
     * @param SVHSADDRFAMS1 The SV_HS_ADDR_FAMS1
     */
    public void setSVHSADDRFAMS1(String SVHSADDRFAMS1) {
        this.SVHSADDRFAMS1 = SVHSADDRFAMS1;
    }

    /**
     * @return The SVHSADDRFAMS2
     */
    public String getSVHSADDRFAMS2() {
        return SVHSADDRFAMS2;
    }

    /**
     * @param SVHSADDRFAMS2 The SV_HS_ADDR_FAMS2
     */
    public void setSVHSADDRFAMS2(String SVHSADDRFAMS2) {
        this.SVHSADDRFAMS2 = SVHSADDRFAMS2;
    }

    /**
     * @return The SVHSADDRFAMS3
     */
    public String getSVHSADDRFAMS3() {
        return SVHSADDRFAMS3;
    }

    /**
     * @param SVHSADDRFAMS3 The SV_HS_ADDR_FAMS3
     */
    public void setSVHSADDRFAMS3(String SVHSADDRFAMS3) {
        this.SVHSADDRFAMS3 = SVHSADDRFAMS3;
    }

    /**
     * @return The SVHSZIPFAMS
     */
    public String getSVHSZIPFAMS() {
        return SVHSZIPFAMS;
    }

    /**
     * @param SVHSZIPFAMS The SV_HS_ZIP_FAMS
     */
    public void setSVHSZIPFAMS(String SVHSZIPFAMS) {
        this.SVHSZIPFAMS = SVHSZIPFAMS;
    }

    /**
     * @return The SVHSCITYCODEFAMS
     */
    public String getSVHSCITYCODEFAMS() {
        return SVHSCITYCODEFAMS;
    }

    /**
     * @param SVHSCITYCODEFAMS The SV_HS_CITY_CODE_FAMS
     */
    public void setSVHSCITYCODEFAMS(String SVHSCITYCODEFAMS) {
        this.SVHSCITYCODEFAMS = SVHSCITYCODEFAMS;
    }

    /**
     * @return The SVHSCITYFAMS
     */
    public String getSVHSCITYFAMS() {
        return SVHSCITYFAMS;
    }

    /**
     * @param SVHSCITYFAMS The SV_HS_CITY_FAMS
     */
    public void setSVHSCITYFAMS(String SVHSCITYFAMS) {
        this.SVHSCITYFAMS = SVHSCITYFAMS;
    }

    /**
     * @return The SVHSNOTLPAREAFAMS
     */
    public String getSVHSNOTLPAREAFAMS() {
        return SVHSNOTLPAREAFAMS;
    }

    /**
     * @param SVHSNOTLPAREAFAMS The SV_HS_NO_TLP_AREA_FAMS
     */
    public void setSVHSNOTLPAREAFAMS(String SVHSNOTLPAREAFAMS) {
        this.SVHSNOTLPAREAFAMS = SVHSNOTLPAREAFAMS;
    }

    /**
     * @return The SVHSNOTLPFAMS
     */
    public String getSVHSNOTLPFAMS() {
        return SVHSNOTLPFAMS;
    }

    /**
     * @param SVHSNOTLPFAMS The SV_HS_NO_TLP_FAMS
     */
    public void setSVHSNOTLPFAMS(String SVHSNOTLPFAMS) {
        this.SVHSNOTLPFAMS = SVHSNOTLPFAMS;
    }

    /**
     * @return The SVHSNOOFFICEAREAFAMS
     */
    public String getSVHSNOOFFICEAREAFAMS() {
        return SVHSNOOFFICEAREAFAMS;
    }

    /**
     * @param SVHSNOOFFICEAREAFAMS The SV_HS_NO_OFFICE_AREA_FAMS
     */
    public void setSVHSNOOFFICEAREAFAMS(String SVHSNOOFFICEAREAFAMS) {
        this.SVHSNOOFFICEAREAFAMS = SVHSNOOFFICEAREAFAMS;
    }

    /**
     * @return The SVHSNOOFFICEFAMS
     */
    public String getSVHSNOOFFICEFAMS() {
        return SVHSNOOFFICEFAMS;
    }

    /**
     * @param SVHSNOOFFICEFAMS The SV_HS_NO_OFFICE_FAMS
     */
    public void setSVHSNOOFFICEFAMS(String SVHSNOOFFICEFAMS) {
        this.SVHSNOOFFICEFAMS = SVHSNOOFFICEFAMS;
    }

    /**
     * @return The SVHSNOOFFICEEXTFAMS
     */
    public String getSVHSNOOFFICEEXTFAMS() {
        return SVHSNOOFFICEEXTFAMS;
    }

    /**
     * @param SVHSNOOFFICEEXTFAMS The SV_HS_NO_OFFICE_EXT_FAMS
     */
    public void setSVHSNOOFFICEEXTFAMS(String SVHSNOOFFICEEXTFAMS) {
        this.SVHSNOOFFICEEXTFAMS = SVHSNOOFFICEEXTFAMS;
    }

    /**
     * @return The SVHSNOHPAREAFAMS
     */
    public String getSVHSNOHPAREAFAMS() {
        return SVHSNOHPAREAFAMS;
    }

    /**
     * @param SVHSNOHPAREAFAMS The SV_HS_NO_HP_AREA_FAMS
     */
    public void setSVHSNOHPAREAFAMS(String SVHSNOHPAREAFAMS) {
        this.SVHSNOHPAREAFAMS = SVHSNOHPAREAFAMS;
    }

    /**
     * @return The SVHSNOHPFAMS
     */
    public String getSVHSNOHPFAMS() {
        return SVHSNOHPFAMS;
    }

    /**
     * @param SVHSNOHPFAMS The SV_HS_NO_HP_FAMS
     */
    public void setSVHSNOHPFAMS(String SVHSNOHPFAMS) {
        this.SVHSNOHPFAMS = SVHSNOHPFAMS;
    }

    /**
     * @return The SVHSHUBFAMS
     */
    public String getSVHSHUBFAMS() {
        return SVHSHUBFAMS;
    }

    /**
     * @param SVHSHUBFAMS The SV_HS_HUB_FAMS
     */
    public void setSVHSHUBFAMS(String SVHSHUBFAMS) {
        this.SVHSHUBFAMS = SVHSHUBFAMS;
    }

    /**
     * @return The SVHSSTSHOMESTAY
     */
    public String getSVHSSTSHOMESTAY() {
        return SVHSSTSHOMESTAY;
    }

    /**
     * @param SVHSSTSHOMESTAY The SV_HS_STS_HOME_STAY
     */
    public void setSVHSSTSHOMESTAY(String SVHSSTSHOMESTAY) {
        this.SVHSSTSHOMESTAY = SVHSSTSHOMESTAY;
    }

    /**
     * @return The SVHSCEKARSIPSTAY
     */
    public String getSVHSCEKARSIPSTAY() {
        return SVHSCEKARSIPSTAY;
    }

    /**
     * @param SVHSCEKARSIPSTAY The SV_HS_CEK_ARSIP_STAY
     */
    public void setSVHSCEKARSIPSTAY(String SVHSCEKARSIPSTAY) {
        this.SVHSCEKARSIPSTAY = SVHSCEKARSIPSTAY;
    }

    /**
     * @return The SVHSAGUNANSTAY
     */
    public String getSVHSAGUNANSTAY() {
        return SVHSAGUNANSTAY;
    }

    /**
     * @param SVHSAGUNANSTAY The SV_HS_AGUNAN_STAY
     */
    public void setSVHSAGUNANSTAY(String SVHSAGUNANSTAY) {
        this.SVHSAGUNANSTAY = SVHSAGUNANSTAY;
    }

    /**
     * @return The SVHSDAYSTAY
     */
    public String getSVHSDAYSTAY() {
        return SVHSDAYSTAY;
    }

    /**
     * @param SVHSDAYSTAY The SV_HS_DAY_STAY
     */
    public void setSVHSDAYSTAY(String SVHSDAYSTAY) {
        this.SVHSDAYSTAY = SVHSDAYSTAY;
    }

    /**
     * @return The SVHSMONTHSTAY
     */
    public String getSVHSMONTHSTAY() {
        return SVHSMONTHSTAY;
    }

    /**
     * @param SVHSMONTHSTAY The SV_HS_MONTH_STAY
     */
    public void setSVHSMONTHSTAY(String SVHSMONTHSTAY) {
        this.SVHSMONTHSTAY = SVHSMONTHSTAY;
    }

    /**
     * @return The SVHSBANGUNANTYPESTAY
     */
    public String getSVHSBANGUNANTYPESTAY() {
        return SVHSBANGUNANTYPESTAY;
    }

    /**
     * @param SVHSBANGUNANTYPESTAY The SV_HS_BANGUNAN_TYPE_STAY
     */
    public void setSVHSBANGUNANTYPESTAY(String SVHSBANGUNANTYPESTAY) {
        this.SVHSBANGUNANTYPESTAY = SVHSBANGUNANTYPESTAY;
    }

    /**
     * @return The SVHSBANGUNANLOKASISTAY
     */
    public String getSVHSBANGUNANLOKASISTAY() {
        return SVHSBANGUNANLOKASISTAY;
    }

    /**
     * @param SVHSBANGUNANLOKASISTAY The SV_HS_BANGUNAN_LOKASI_STAY
     */
    public void setSVHSBANGUNANLOKASISTAY(String SVHSBANGUNANLOKASISTAY) {
        this.SVHSBANGUNANLOKASISTAY = SVHSBANGUNANLOKASISTAY;
    }

    /**
     * @return The SVHSBANGUNANCONDSTAY
     */
    public String getSVHSBANGUNANCONDSTAY() {
        return SVHSBANGUNANCONDSTAY;
    }

    /**
     * @param SVHSBANGUNANCONDSTAY The SV_HS_BANGUNAN_COND_STAY
     */
    public void setSVHSBANGUNANCONDSTAY(String SVHSBANGUNANCONDSTAY) {
        this.SVHSBANGUNANCONDSTAY = SVHSBANGUNANCONDSTAY;
    }

    /**
     * @return The SVHSFASILITASSTAY
     */
    public String getSVHSFASILITASSTAY() {
        return SVHSFASILITASSTAY;
    }

    /**
     * @param SVHSFASILITASSTAY The SV_HS_FASILITAS_STAY
     */
    public void setSVHSFASILITASSTAY(String SVHSFASILITASSTAY) {
        this.SVHSFASILITASSTAY = SVHSFASILITASSTAY;
    }

    /**
     * @return The SVHSBARANGHOMESTAY
     */
    public String getSVHSBARANGHOMESTAY() {
        return SVHSBARANGHOMESTAY;
    }

    /**
     * @param SVHSBARANGHOMESTAY The SV_HS_BARANG_HOME_STAY
     */
    public void setSVHSBARANGHOMESTAY(String SVHSBARANGHOMESTAY) {
        this.SVHSBARANGHOMESTAY = SVHSBARANGHOMESTAY;
    }

    /**
     * @return The SVHSAKSESROADSTAY
     */
    public String getSVHSAKSESROADSTAY() {
        return SVHSAKSESROADSTAY;
    }

    /**
     * @param SVHSAKSESROADSTAY The SV_HS_AKSES_ROAD_STAY
     */
    public void setSVHSAKSESROADSTAY(String SVHSAKSESROADSTAY) {
        this.SVHSAKSESROADSTAY = SVHSAKSESROADSTAY;
    }

    /**
     * @return The SVHSLINGKUNGANSTAY
     */
    public String getSVHSLINGKUNGANSTAY() {
        return SVHSLINGKUNGANSTAY;
    }

    /**
     * @param SVHSLINGKUNGANSTAY The SV_HS_LINGKUNGAN_STAY
     */
    public void setSVHSLINGKUNGANSTAY(String SVHSLINGKUNGANSTAY) {
        this.SVHSLINGKUNGANSTAY = SVHSLINGKUNGANSTAY;
    }

    /**
     * @return The SVHSLUASTANAHSTAY
     */
    public String getSVHSLUASTANAHSTAY() {
        return SVHSLUASTANAHSTAY;
    }

    /**
     * @param SVHSLUASTANAHSTAY The SV_HS_LUAS_TANAH_STAY
     */
    public void setSVHSLUASTANAHSTAY(String SVHSLUASTANAHSTAY) {
        this.SVHSLUASTANAHSTAY = SVHSLUASTANAHSTAY;
    }

    /**
     * @return The SVHSLUASBANGUNANSTAY
     */
    public String getSVHSLUASBANGUNANSTAY() {
        return SVHSLUASBANGUNANSTAY;
    }

    /**
     * @param SVHSLUASBANGUNANSTAY The SV_HS_LUAS_BANGUNAN_STAY
     */
    public void setSVHSLUASBANGUNANSTAY(String SVHSLUASBANGUNANSTAY) {
        this.SVHSLUASBANGUNANSTAY = SVHSLUASBANGUNANSTAY;
    }

    /**
     * @return The SVHSGARASISTAY
     */
    public String getSVHSGARASISTAY() {
        return SVHSGARASISTAY;
    }

    /**
     * @param SVHSGARASISTAY The SV_HS_GARASI_STAY
     */
    public void setSVHSGARASISTAY(String SVHSGARASISTAY) {
        this.SVHSGARASISTAY = SVHSGARASISTAY;
    }

    /**
     * @return The SVHSCARPORTSTAY
     */
    public String getSVHSCARPORTSTAY() {
        return SVHSCARPORTSTAY;
    }

    /**
     * @param SVHSCARPORTSTAY The SV_HS_CARPORT_STAY
     */
    public void setSVHSCARPORTSTAY(String SVHSCARPORTSTAY) {
        this.SVHSCARPORTSTAY = SVHSCARPORTSTAY;
    }

    /**
     * @return The SVHSVEHICLESTAY
     */
    public String getSVHSVEHICLESTAY() {
        return SVHSVEHICLESTAY;
    }

    /**
     * @param SVHSVEHICLESTAY The SV_HS_VEHICLE_STAY
     */
    public void setSVHSVEHICLESTAY(String SVHSVEHICLESTAY) {
        this.SVHSVEHICLESTAY = SVHSVEHICLESTAY;
    }

    /**
     * @return The SVHSVEHICLETYPESTAY
     */
    public String getSVHSVEHICLETYPESTAY() {
        return SVHSVEHICLETYPESTAY;
    }

    /**
     * @param SVHSVEHICLETYPESTAY The SV_HS_VEHICLE_TYPE_STAY
     */
    public void setSVHSVEHICLETYPESTAY(String SVHSVEHICLETYPESTAY) {
        this.SVHSVEHICLETYPESTAY = SVHSVEHICLETYPESTAY;
    }

    /**
     * @return The SVHSVEHICLECOUNTSTAY
     */
    public String getSVHSVEHICLECOUNTSTAY() {
        return SVHSVEHICLECOUNTSTAY;
    }

    /**
     * @param SVHSVEHICLECOUNTSTAY The SV_HS_VEHICLE_COUNT_STAY
     */
    public void setSVHSVEHICLECOUNTSTAY(String SVHSVEHICLECOUNTSTAY) {
        this.SVHSVEHICLECOUNTSTAY = SVHSVEHICLECOUNTSTAY;
    }

    /**
     * @return The SVHSVEHICLECONDSTAY
     */
    public String getSVHSVEHICLECONDSTAY() {
        return SVHSVEHICLECONDSTAY;
    }

    /**
     * @param SVHSVEHICLECONDSTAY The SV_HS_VEHICLE_COND_STAY
     */
    public void setSVHSVEHICLECONDSTAY(String SVHSVEHICLECONDSTAY) {
        this.SVHSVEHICLECONDSTAY = SVHSVEHICLECONDSTAY;
    }

    /**
     * @return The SVHSKETERANGANSTAY
     */
    public String getSVHSKETERANGANSTAY() {
        return SVHSKETERANGANSTAY;
    }

    /**
     * @param SVHSKETERANGANSTAY The SV_HS_KETERANGAN_STAY
     */
    public void setSVHSKETERANGANSTAY(String SVHSKETERANGANSTAY) {
        this.SVHSKETERANGANSTAY = SVHSKETERANGANSTAY;
    }

    /**
     * @return The SVOFCPEMBERIKET1
     */
    public String getSVOFCPEMBERIKET1() {
        return SVOFCPEMBERIKET1;
    }

    /**
     * @param SVOFCPEMBERIKET1 The SV_OFC_PEMBERI_KET1
     */
    public void setSVOFCPEMBERIKET1(String SVOFCPEMBERIKET1) {
        this.SVOFCPEMBERIKET1 = SVOFCPEMBERIKET1;
    }

    /**
     * @return The SVOFCPOSISIPEMBERIKET1
     */
    public String getSVOFCPOSISIPEMBERIKET1() {
        return SVOFCPOSISIPEMBERIKET1;
    }

    /**
     * @param SVOFCPOSISIPEMBERIKET1 The SV_OFC_POSISI_PEMBERI_KET1
     */
    public void setSVOFCPOSISIPEMBERIKET1(String SVOFCPOSISIPEMBERIKET1) {
        this.SVOFCPOSISIPEMBERIKET1 = SVOFCPOSISIPEMBERIKET1;
    }

    /**
     * @return The SVOFCPEMBERIKET2
     */
    public String getSVOFCPEMBERIKET2() {
        return SVOFCPEMBERIKET2;
    }

    /**
     * @param SVOFCPEMBERIKET2 The SV_OFC_PEMBERI_KET2
     */
    public void setSVOFCPEMBERIKET2(String SVOFCPEMBERIKET2) {
        this.SVOFCPEMBERIKET2 = SVOFCPEMBERIKET2;
    }

    /**
     * @return The SVOFCPOSISIPEMBERIKET2
     */
    public String getSVOFCPOSISIPEMBERIKET2() {
        return SVOFCPOSISIPEMBERIKET2;
    }

    /**
     * @param SVOFCPOSISIPEMBERIKET2 The SV_OFC_POSISI_PEMBERI_KET2
     */
    public void setSVOFCPOSISIPEMBERIKET2(String SVOFCPOSISIPEMBERIKET2) {
        this.SVOFCPOSISIPEMBERIKET2 = SVOFCPOSISIPEMBERIKET2;
    }

    /**
     * @return The SVOFCTYPOFFICE
     */
    public String getSVOFCTYPOFFICE() {
        return SVOFCTYPOFFICE;
    }

    /**
     * @param SVOFCTYPOFFICE The SV_OFC_TYP_OFFICE
     */
    public void setSVOFCTYPOFFICE(String SVOFCTYPOFFICE) {
        this.SVOFCTYPOFFICE = SVOFCTYPOFFICE;
    }

    /**
     * @return The SVOFCNMOFFICE
     */
    public String getSVOFCNMOFFICE() {
        return SVOFCNMOFFICE;
    }

    /**
     * @param SVOFCNMOFFICE The SV_OFC_NM_OFFICE
     */
    public void setSVOFCNMOFFICE(String SVOFCNMOFFICE) {
        this.SVOFCNMOFFICE = SVOFCNMOFFICE;
    }

    /**
     * @return The SVOFCADDROFFICE1
     */
    public String getSVOFCADDROFFICE1() {
        return SVOFCADDROFFICE1;
    }

    /**
     * @param SVOFCADDROFFICE1 The SV_OFC_ADDR_OFFICE1
     */
    public void setSVOFCADDROFFICE1(String SVOFCADDROFFICE1) {
        this.SVOFCADDROFFICE1 = SVOFCADDROFFICE1;
    }

    /**
     * @return The SVOFCADDROFFICE2
     */
    public String getSVOFCADDROFFICE2() {
        return SVOFCADDROFFICE2;
    }

    /**
     * @param SVOFCADDROFFICE2 The SV_OFC_ADDR_OFFICE2
     */
    public void setSVOFCADDROFFICE2(String SVOFCADDROFFICE2) {
        this.SVOFCADDROFFICE2 = SVOFCADDROFFICE2;
    }

    /**
     * @return The SVOFCADDROFFICE3
     */
    public String getSVOFCADDROFFICE3() {
        return SVOFCADDROFFICE3;
    }

    /**
     * @param SVOFCADDROFFICE3 The SV_OFC_ADDR_OFFICE3
     */
    public void setSVOFCADDROFFICE3(String SVOFCADDROFFICE3) {
        this.SVOFCADDROFFICE3 = SVOFCADDROFFICE3;
    }

    /**
     * @return The SVOFCZIPCODEOFFICE
     */
    public String getSVOFCZIPCODEOFFICE() {
        return SVOFCZIPCODEOFFICE;
    }

    /**
     * @param SVOFCZIPCODEOFFICE The SV_OFC_ZIPCODE_OFFICE
     */
    public void setSVOFCZIPCODEOFFICE(String SVOFCZIPCODEOFFICE) {
        this.SVOFCZIPCODEOFFICE = SVOFCZIPCODEOFFICE;
    }

    /**
     * @return The SVOFCCITYCODEOFFICE
     */
    public String getSVOFCCITYCODEOFFICE() {
        return SVOFCCITYCODEOFFICE;
    }

    /**
     * @param SVOFCCITYCODEOFFICE The SV_OFC_CITY_CODE_OFFICE
     */
    public void setSVOFCCITYCODEOFFICE(String SVOFCCITYCODEOFFICE) {
        this.SVOFCCITYCODEOFFICE = SVOFCCITYCODEOFFICE;
    }

    /**
     * @return The SVOFCCITYOFFICE
     */
    public String getSVOFCCITYOFFICE() {
        return SVOFCCITYOFFICE;
    }

    /**
     * @param SVOFCCITYOFFICE The SV_OFC_CITY_OFFICE
     */
    public void setSVOFCCITYOFFICE(String SVOFCCITYOFFICE) {
        this.SVOFCCITYOFFICE = SVOFCCITYOFFICE;
    }

    /**
     * @return The SVOFCN0TLPAREAOFFICE
     */
    public String getSVOFCN0TLPAREAOFFICE() {
        return SVOFCN0TLPAREAOFFICE;
    }

    /**
     * @param SVOFCN0TLPAREAOFFICE The SV_OFC_N0_TLP_AREA_OFFICE
     */
    public void setSVOFCN0TLPAREAOFFICE(String SVOFCN0TLPAREAOFFICE) {
        this.SVOFCN0TLPAREAOFFICE = SVOFCN0TLPAREAOFFICE;
    }

    /**
     * @return The SVOFCN0TLPOFFICE
     */
    public String getSVOFCN0TLPOFFICE() {
        return SVOFCN0TLPOFFICE;
    }

    /**
     * @param SVOFCN0TLPOFFICE The SV_OFC_N0_TLP_OFFICE
     */
    public void setSVOFCN0TLPOFFICE(String SVOFCN0TLPOFFICE) {
        this.SVOFCN0TLPOFFICE = SVOFCN0TLPOFFICE;
    }

    /**
     * @return The SVOFCEXTOFFICE
     */
    public String getSVOFCEXTOFFICE() {
        return SVOFCEXTOFFICE;
    }

    /**
     * @param SVOFCEXTOFFICE The SV_OFC_EXT_OFFICE
     */
    public void setSVOFCEXTOFFICE(String SVOFCEXTOFFICE) {
        this.SVOFCEXTOFFICE = SVOFCEXTOFFICE;
    }

    /**
     * @return The SVOFCN0FAXAREAOFFICE
     */
    public String getSVOFCN0FAXAREAOFFICE() {
        return SVOFCN0FAXAREAOFFICE;
    }

    /**
     * @param SVOFCN0FAXAREAOFFICE The SV_OFC_N0_FAX_AREA_OFFICE
     */
    public void setSVOFCN0FAXAREAOFFICE(String SVOFCN0FAXAREAOFFICE) {
        this.SVOFCN0FAXAREAOFFICE = SVOFCN0FAXAREAOFFICE;
    }

    /**
     * @return The SVOFCN0FAXOFFICE
     */
    public String getSVOFCN0FAXOFFICE() {
        return SVOFCN0FAXOFFICE;
    }

    /**
     * @param SVOFCN0FAXOFFICE The SV_OFC_N0_FAX_OFFICE
     */
    public void setSVOFCN0FAXOFFICE(String SVOFCN0FAXOFFICE) {
        this.SVOFCN0FAXOFFICE = SVOFCN0FAXOFFICE;
    }

    /**
     * @return The SVOFCYEAROFFICE
     */
    public String getSVOFCYEAROFFICE() {
        return SVOFCYEAROFFICE;
    }

    /**
     * @param SVOFCYEAROFFICE The SV_OFC_YEAR_OFFICE
     */
    public void setSVOFCYEAROFFICE(String SVOFCYEAROFFICE) {
        this.SVOFCYEAROFFICE = SVOFCYEAROFFICE;
    }

    /**
     * @return The SVOFCUSAHAOFFICE
     */
    public String getSVOFCUSAHAOFFICE() {
        return SVOFCUSAHAOFFICE;
    }

    /**
     * @param SVOFCUSAHAOFFICE The SV_OFC_USAHA_OFFICE
     */
    public void setSVOFCUSAHAOFFICE(String SVOFCUSAHAOFFICE) {
        this.SVOFCUSAHAOFFICE = SVOFCUSAHAOFFICE;
    }

    /**
     * @return The SVOFCSTAFOFFICE
     */
    public String getSVOFCSTAFOFFICE() {
        return SVOFCSTAFOFFICE;
    }

    /**
     * @param SVOFCSTAFOFFICE The SV_OFC_STAF_OFFICE
     */
    public void setSVOFCSTAFOFFICE(String SVOFCSTAFOFFICE) {
        this.SVOFCSTAFOFFICE = SVOFCSTAFOFFICE;
    }

    /**
     * @return The SVOFCSCALEOFFICE
     */
    public String getSVOFCSCALEOFFICE() {
        return SVOFCSCALEOFFICE;
    }

    /**
     * @param SVOFCSCALEOFFICE The SV_OFC_SCALE_OFFICE
     */
    public void setSVOFCSCALEOFFICE(String SVOFCSCALEOFFICE) {
        this.SVOFCSCALEOFFICE = SVOFCSCALEOFFICE;
    }

    /**
     * @return The SVOFCBANGUNANOFFICE
     */
    public String getSVOFCBANGUNANOFFICE() {
        return SVOFCBANGUNANOFFICE;
    }

    /**
     * @param SVOFCBANGUNANOFFICE The SV_OFC_BANGUNAN_OFFICE
     */
    public void setSVOFCBANGUNANOFFICE(String SVOFCBANGUNANOFFICE) {
        this.SVOFCBANGUNANOFFICE = SVOFCBANGUNANOFFICE;
    }

    /**
     * @return The SVOFCLOKASIOFFICE
     */
    public String getSVOFCLOKASIOFFICE() {
        return SVOFCLOKASIOFFICE;
    }

    /**
     * @param SVOFCLOKASIOFFICE The SV_OFC_LOKASI_OFFICE
     */
    public void setSVOFCLOKASIOFFICE(String SVOFCLOKASIOFFICE) {
        this.SVOFCLOKASIOFFICE = SVOFCLOKASIOFFICE;
    }

    /**
     * @return The SVOFCKONDISIOFFICE
     */
    public String getSVOFCKONDISIOFFICE() {
        return SVOFCKONDISIOFFICE;
    }

    /**
     * @param SVOFCKONDISIOFFICE The SV_OFC_KONDISI_OFFICE
     */
    public void setSVOFCKONDISIOFFICE(String SVOFCKONDISIOFFICE) {
        this.SVOFCKONDISIOFFICE = SVOFCKONDISIOFFICE;
    }

    /**
     * @return The SVOFCOWNEROFFICE
     */
    public String getSVOFCOWNEROFFICE() {
        return SVOFCOWNEROFFICE;
    }

    /**
     * @param SVOFCOWNEROFFICE The SV_OFC_OWNER_OFFICE
     */
    public void setSVOFCOWNEROFFICE(String SVOFCOWNEROFFICE) {
        this.SVOFCOWNEROFFICE = SVOFCOWNEROFFICE;
    }

    /**
     * @return The SVOFCINVESTIGASIDATE
     */
    public String getSVOFCINVESTIGASIDATE() {
        return SVOFCINVESTIGASIDATE;
    }

    /**
     * @param SVOFCINVESTIGASIDATE The SV_OFC_INVESTIGASI_DATE
     */
    public void setSVOFCINVESTIGASIDATE(String SVOFCINVESTIGASIDATE) {
        this.SVOFCINVESTIGASIDATE = SVOFCINVESTIGASIDATE;
    }

    /**
     * @return The SVOFCTYPOFFICEPLUS
     */
    public String getSVOFCTYPOFFICEPLUS() {
        return SVOFCTYPOFFICEPLUS;
    }

    /**
     * @param SVOFCTYPOFFICEPLUS The SV_OFC_TYP_OFFICE_PLUS
     */
    public void setSVOFCTYPOFFICEPLUS(String SVOFCTYPOFFICEPLUS) {
        this.SVOFCTYPOFFICEPLUS = SVOFCTYPOFFICEPLUS;
    }

    /**
     * @return The SVOFCNMOFFICEPLUS
     */
    public String getSVOFCNMOFFICEPLUS() {
        return SVOFCNMOFFICEPLUS;
    }

    /**
     * @param SVOFCNMOFFICEPLUS The SV_OFC_NM_OFFICE_PLUS
     */
    public void setSVOFCNMOFFICEPLUS(String SVOFCNMOFFICEPLUS) {
        this.SVOFCNMOFFICEPLUS = SVOFCNMOFFICEPLUS;
    }

    /**
     * @return The SVOFCPOSISIPEMOHONPLUS
     */
    public String getSVOFCPOSISIPEMOHONPLUS() {
        return SVOFCPOSISIPEMOHONPLUS;
    }

    /**
     * @param SVOFCPOSISIPEMOHONPLUS The SV_OFC_POSISI_PEMOHON_PLUS
     */
    public void setSVOFCPOSISIPEMOHONPLUS(String SVOFCPOSISIPEMOHONPLUS) {
        this.SVOFCPOSISIPEMOHONPLUS = SVOFCPOSISIPEMOHONPLUS;
    }

    /**
     * @return The SVOFCNOTLPAREAPLUS
     */
    public String getSVOFCNOTLPAREAPLUS() {
        return SVOFCNOTLPAREAPLUS;
    }

    /**
     * @param SVOFCNOTLPAREAPLUS The SV_OFC_NO_TLP_AREA_PLUS
     */
    public void setSVOFCNOTLPAREAPLUS(String SVOFCNOTLPAREAPLUS) {
        this.SVOFCNOTLPAREAPLUS = SVOFCNOTLPAREAPLUS;
    }

    /**
     * @return The SVOFCNOTLPPLUS
     */
    public String getSVOFCNOTLPPLUS() {
        return SVOFCNOTLPPLUS;
    }

    /**
     * @param SVOFCNOTLPPLUS The SV_OFC_NO_TLP_PLUS
     */
    public void setSVOFCNOTLPPLUS(String SVOFCNOTLPPLUS) {
        this.SVOFCNOTLPPLUS = SVOFCNOTLPPLUS;
    }

    /**
     * @return The SVOFCNOFAXAREAPLUS
     */
    public String getSVOFCNOFAXAREAPLUS() {
        return SVOFCNOFAXAREAPLUS;
    }

    /**
     * @param SVOFCNOFAXAREAPLUS The SV_OFC_NO_FAX_AREA_PLUS
     */
    public void setSVOFCNOFAXAREAPLUS(String SVOFCNOFAXAREAPLUS) {
        this.SVOFCNOFAXAREAPLUS = SVOFCNOFAXAREAPLUS;
    }

    /**
     * @return The SVOFCNOFAXPLUS
     */
    public String getSVOFCNOFAXPLUS() {
        return SVOFCNOFAXPLUS;
    }

    /**
     * @param SVOFCNOFAXPLUS The SV_OFC_NO_FAX_PLUS
     */
    public void setSVOFCNOFAXPLUS(String SVOFCNOFAXPLUS) {
        this.SVOFCNOFAXPLUS = SVOFCNOFAXPLUS;
    }

    /**
     * @return The SVOFCADDRPLUS1
     */
    public String getSVOFCADDRPLUS1() {
        return SVOFCADDRPLUS1;
    }

    /**
     * @param SVOFCADDRPLUS1 The SV_OFC_ADDR_PLUS1
     */
    public void setSVOFCADDRPLUS1(String SVOFCADDRPLUS1) {
        this.SVOFCADDRPLUS1 = SVOFCADDRPLUS1;
    }

    /**
     * @return The SVOFCADDRPLUS2
     */
    public String getSVOFCADDRPLUS2() {
        return SVOFCADDRPLUS2;
    }

    /**
     * @param SVOFCADDRPLUS2 The SV_OFC_ADDR_PLUS2
     */
    public void setSVOFCADDRPLUS2(String SVOFCADDRPLUS2) {
        this.SVOFCADDRPLUS2 = SVOFCADDRPLUS2;
    }

    /**
     * @return The SVOFCADDRPLUS3
     */
    public String getSVOFCADDRPLUS3() {
        return SVOFCADDRPLUS3;
    }

    /**
     * @param SVOFCADDRPLUS3 The SV_OFC_ADDR_PLUS3
     */
    public void setSVOFCADDRPLUS3(String SVOFCADDRPLUS3) {
        this.SVOFCADDRPLUS3 = SVOFCADDRPLUS3;
    }

    /**
     * @return The SVOFCZIPCODEPLUS
     */
    public String getSVOFCZIPCODEPLUS() {
        return SVOFCZIPCODEPLUS;
    }

    /**
     * @param SVOFCZIPCODEPLUS The SV_OFC_ZIPCODE_PLUS
     */
    public void setSVOFCZIPCODEPLUS(String SVOFCZIPCODEPLUS) {
        this.SVOFCZIPCODEPLUS = SVOFCZIPCODEPLUS;
    }

    /**
     * @return The SVOFCCITYCODEPLUS
     */
    public String getSVOFCCITYCODEPLUS() {
        return SVOFCCITYCODEPLUS;
    }

    /**
     * @param SVOFCCITYCODEPLUS The SV_OFC_CITY_CODE_PLUS
     */
    public void setSVOFCCITYCODEPLUS(String SVOFCCITYCODEPLUS) {
        this.SVOFCCITYCODEPLUS = SVOFCCITYCODEPLUS;
    }

    /**
     * @return The SVOFCCITYPLUS
     */
    public String getSVOFCCITYPLUS() {
        return SVOFCCITYPLUS;
    }

    /**
     * @param SVOFCCITYPLUS The SV_OFC_CITY_PLUS
     */
    public void setSVOFCCITYPLUS(String SVOFCCITYPLUS) {
        this.SVOFCCITYPLUS = SVOFCCITYPLUS;
    }

    /**
     * @return The SVOFCNAMEWORK
     */
    public String getSVOFCNAMEWORK() {
        return SVOFCNAMEWORK;
    }

    /**
     * @param SVOFCNAMEWORK The SV_OFC_NAME_WORK
     */
    public void setSVOFCNAMEWORK(String SVOFCNAMEWORK) {
        this.SVOFCNAMEWORK = SVOFCNAMEWORK;
    }

    /**
     * @return The SVOFCPOSISIWORK
     */
    public String getSVOFCPOSISIWORK() {
        return SVOFCPOSISIWORK;
    }

    /**
     * @param SVOFCPOSISIWORK The SV_OFC_POSISI_WORK
     */
    public void setSVOFCPOSISIWORK(String SVOFCPOSISIWORK) {
        this.SVOFCPOSISIWORK = SVOFCPOSISIWORK;
    }

    /**
     * @return The SVOFCYEARWORK
     */
    public String getSVOFCYEARWORK() {
        return SVOFCYEARWORK;
    }

    /**
     * @param SVOFCYEARWORK The SV_OFC_YEAR_WORK
     */
    public void setSVOFCYEARWORK(String SVOFCYEARWORK) {
        this.SVOFCYEARWORK = SVOFCYEARWORK;
    }

    /**
     * @return The SVOFCMONTHWORK
     */
    public String getSVOFCMONTHWORK() {
        return SVOFCMONTHWORK;
    }

    /**
     * @param SVOFCMONTHWORK The SV_OFC_MONTH_WORK
     */
    public void setSVOFCMONTHWORK(String SVOFCMONTHWORK) {
        this.SVOFCMONTHWORK = SVOFCMONTHWORK;
    }

    /**
     * @return The SVOFCSTATUSWORK
     */
    public String getSVOFCSTATUSWORK() {
        return SVOFCSTATUSWORK;
    }

    /**
     * @param SVOFCSTATUSWORK The SV_OFC_STATUS_WORK
     */
    public void setSVOFCSTATUSWORK(String SVOFCSTATUSWORK) {
        this.SVOFCSTATUSWORK = SVOFCSTATUSWORK;
    }

    /**
     * @return The SVOFCUNITWORK
     */
    public String getSVOFCUNITWORK() {
        return SVOFCUNITWORK;
    }

    /**
     * @param SVOFCUNITWORK The SV_OFC_UNIT_WORK
     */
    public void setSVOFCUNITWORK(String SVOFCUNITWORK) {
        this.SVOFCUNITWORK = SVOFCUNITWORK;
    }

    /**
     * @return The SVOFCKINERJAWORK
     */
    public String getSVOFCKINERJAWORK() {
        return SVOFCKINERJAWORK;
    }

    /**
     * @param SVOFCKINERJAWORK The SV_OFC_KINERJA_WORK
     */
    public void setSVOFCKINERJAWORK(String SVOFCKINERJAWORK) {
        this.SVOFCKINERJAWORK = SVOFCKINERJAWORK;
    }

    /**
     * @return The SVOFCOFFICENMHISTORY
     */
    public String getSVOFCOFFICENMHISTORY() {
        return SVOFCOFFICENMHISTORY;
    }

    /**
     * @param SVOFCOFFICENMHISTORY The SV_OFC_OFFICE_NM_HISTORY
     */
    public void setSVOFCOFFICENMHISTORY(String SVOFCOFFICENMHISTORY) {
        this.SVOFCOFFICENMHISTORY = SVOFCOFFICENMHISTORY;
    }

    /**
     * @return The SVOFCNOTLPAREAHISTORY
     */
    public String getSVOFCNOTLPAREAHISTORY() {
        return SVOFCNOTLPAREAHISTORY;
    }

    /**
     * @param SVOFCNOTLPAREAHISTORY The SV_OFC_NO_TLP_AREA_HISTORY
     */
    public void setSVOFCNOTLPAREAHISTORY(String SVOFCNOTLPAREAHISTORY) {
        this.SVOFCNOTLPAREAHISTORY = SVOFCNOTLPAREAHISTORY;
    }

    /**
     * @return The SVOFCNOTLPHISTORY
     */
    public String getSVOFCNOTLPHISTORY() {
        return SVOFCNOTLPHISTORY;
    }

    /**
     * @param SVOFCNOTLPHISTORY The SV_OFC_NO_TLP_HISTORY
     */
    public void setSVOFCNOTLPHISTORY(String SVOFCNOTLPHISTORY) {
        this.SVOFCNOTLPHISTORY = SVOFCNOTLPHISTORY;
    }

    /**
     * @return The SVOFCOFFICEYEARHISTORY
     */
    public String getSVOFCOFFICEYEARHISTORY() {
        return SVOFCOFFICEYEARHISTORY;
    }

    /**
     * @param SVOFCOFFICEYEARHISTORY The SV_OFC_OFFICE_YEAR_HISTORY
     */
    public void setSVOFCOFFICEYEARHISTORY(String SVOFCOFFICEYEARHISTORY) {
        this.SVOFCOFFICEYEARHISTORY = SVOFCOFFICEYEARHISTORY;
    }

    /**
     * @return The SVOFCOFFICEMONTHHISTORY
     */
    public String getSVOFCOFFICEMONTHHISTORY() {
        return SVOFCOFFICEMONTHHISTORY;
    }

    /**
     * @param SVOFCOFFICEMONTHHISTORY The SV_OFC_OFFICE_MONTH_HISTORY
     */
    public void setSVOFCOFFICEMONTHHISTORY(String SVOFCOFFICEMONTHHISTORY) {
        this.SVOFCOFFICEMONTHHISTORY = SVOFCOFFICEMONTHHISTORY;
    }

    /**
     * @return The SVOFCINCOMEBRUTOPEMOHON
     */
    public String getSVOFCINCOMEBRUTOPEMOHON() {
        return SVOFCINCOMEBRUTOPEMOHON;
    }

    /**
     * @param SVOFCINCOMEBRUTOPEMOHON The SV_OFC_INCOME_BRUTO_PEMOHON
     */
    public void setSVOFCINCOMEBRUTOPEMOHON(String SVOFCINCOMEBRUTOPEMOHON) {
        this.SVOFCINCOMEBRUTOPEMOHON = SVOFCINCOMEBRUTOPEMOHON;
    }

    /**
     * @return The SVOFCINCOMENETTOPEMOHON
     */
    public String getSVOFCINCOMENETTOPEMOHON() {
        return SVOFCINCOMENETTOPEMOHON;
    }

    /**
     * @param SVOFCINCOMENETTOPEMOHON The SV_OFC_INCOME_NETTO_PEMOHON
     */
    public void setSVOFCINCOMENETTOPEMOHON(String SVOFCINCOMENETTOPEMOHON) {
        this.SVOFCINCOMENETTOPEMOHON = SVOFCINCOMENETTOPEMOHON;
    }

    /**
     * @return The SVOFCOTHERINCOMEPEMOHON
     */
    public String getSVOFCOTHERINCOMEPEMOHON() {
        return SVOFCOTHERINCOMEPEMOHON;
    }

    /**
     * @param SVOFCOTHERINCOMEPEMOHON The SV_OFC_OTHER_INCOME_PEMOHON
     */
    public void setSVOFCOTHERINCOMEPEMOHON(String SVOFCOTHERINCOMEPEMOHON) {
        this.SVOFCOTHERINCOMEPEMOHON = SVOFCOTHERINCOMEPEMOHON;
    }

    /**
     * @return The SVOFCTOTALINCOMEPEMOHON
     */
    public String getSVOFCTOTALINCOMEPEMOHON() {
        return SVOFCTOTALINCOMEPEMOHON;
    }

    /**
     * @param SVOFCTOTALINCOMEPEMOHON The SV_OFC_TOTAL_INCOME_PEMOHON
     */
    public void setSVOFCTOTALINCOMEPEMOHON(String SVOFCTOTALINCOMEPEMOHON) {
        this.SVOFCTOTALINCOMEPEMOHON = SVOFCTOTALINCOMEPEMOHON;
    }

    /**
     * @return The SVOFCPAYPEMOHON
     */
    public String getSVOFCPAYPEMOHON() {
        return SVOFCPAYPEMOHON;
    }

    /**
     * @param SVOFCPAYPEMOHON The SV_OFC_PAY_PEMOHON
     */
    public void setSVOFCPAYPEMOHON(String SVOFCPAYPEMOHON) {
        this.SVOFCPAYPEMOHON = SVOFCPAYPEMOHON;
    }

    /**
     * @return The SVOFCINCOMEBRUTOSPOUSE
     */
    public String getSVOFCINCOMEBRUTOSPOUSE() {
        return SVOFCINCOMEBRUTOSPOUSE;
    }

    /**
     * @param SVOFCINCOMEBRUTOSPOUSE The SV_OFC_INCOME_BRUTO_SPOUSE
     */
    public void setSVOFCINCOMEBRUTOSPOUSE(String SVOFCINCOMEBRUTOSPOUSE) {
        this.SVOFCINCOMEBRUTOSPOUSE = SVOFCINCOMEBRUTOSPOUSE;
    }

    /**
     * @return The SVOFCINCOMENETTOSPOUSE
     */
    public String getSVOFCINCOMENETTOSPOUSE() {
        return SVOFCINCOMENETTOSPOUSE;
    }

    /**
     * @param SVOFCINCOMENETTOSPOUSE The SV_OFC_INCOME_NETTO_SPOUSE
     */
    public void setSVOFCINCOMENETTOSPOUSE(String SVOFCINCOMENETTOSPOUSE) {
        this.SVOFCINCOMENETTOSPOUSE = SVOFCINCOMENETTOSPOUSE;
    }

    /**
     * @return The SVOFCOTHERINCOMESPOUSE
     */
    public String getSVOFCOTHERINCOMESPOUSE() {
        return SVOFCOTHERINCOMESPOUSE;
    }

    /**
     * @param SVOFCOTHERINCOMESPOUSE The SV_OFC_OTHER_INCOME_SPOUSE
     */
    public void setSVOFCOTHERINCOMESPOUSE(String SVOFCOTHERINCOMESPOUSE) {
        this.SVOFCOTHERINCOMESPOUSE = SVOFCOTHERINCOMESPOUSE;
    }

    /**
     * @return The SVOFCTOTALINCOMESPOUSE
     */
    public String getSVOFCTOTALINCOMESPOUSE() {
        return SVOFCTOTALINCOMESPOUSE;
    }

    /**
     * @param SVOFCTOTALINCOMESPOUSE The SV_OFC_TOTAL_INCOME_SPOUSE
     */
    public void setSVOFCTOTALINCOMESPOUSE(String SVOFCTOTALINCOMESPOUSE) {
        this.SVOFCTOTALINCOMESPOUSE = SVOFCTOTALINCOMESPOUSE;
    }

    /**
     * @return The SVOFCPAYSPOUSE
     */
    public String getSVOFCPAYSPOUSE() {
        return SVOFCPAYSPOUSE;
    }

    /**
     * @param SVOFCPAYSPOUSE The SV_OFC_PAY_SPOUSE
     */
    public void setSVOFCPAYSPOUSE(String SVOFCPAYSPOUSE) {
        this.SVOFCPAYSPOUSE = SVOFCPAYSPOUSE;
    }

    /**
     * @return The SVOFCINCOMEMARGIN
     */
    public String getSVOFCINCOMEMARGIN() {
        return SVOFCINCOMEMARGIN;
    }

    /**
     * @param SVOFCINCOMEMARGIN The SV_OFC_INCOME_MARGIN
     */
    public void setSVOFCINCOMEMARGIN(String SVOFCINCOMEMARGIN) {
        this.SVOFCINCOMEMARGIN = SVOFCINCOMEMARGIN;
    }

    /**
     * @return The SVOFCPEMOHONSUMMARY
     */
    public String getSVOFCPEMOHONSUMMARY() {
        return SVOFCPEMOHONSUMMARY;
    }

    /**
     * @param SVOFCPEMOHONSUMMARY The SV_OFC_PEMOHON_SUMMARY
     */
    public void setSVOFCPEMOHONSUMMARY(String SVOFCPEMOHONSUMMARY) {
        this.SVOFCPEMOHONSUMMARY = SVOFCPEMOHONSUMMARY;
    }

    /**
     * @return The SVOFCOTHERSUMMARY
     */
    public String getSVOFCOTHERSUMMARY() {
        return SVOFCOTHERSUMMARY;
    }

    /**
     * @param SVOFCOTHERSUMMARY The SV_OFC_OTHER_SUMMARY
     */
    public void setSVOFCOTHERSUMMARY(String SVOFCOTHERSUMMARY) {
        this.SVOFCOTHERSUMMARY = SVOFCOTHERSUMMARY;
    }

    /**
     * @return The TIPEINCOME
     */
    public String getTIPEINCOME() {
        return TIPEINCOME;
    }

    /**
     * @param TIPEINCOME The TIPE_INCOME
     */
    public void setTIPEINCOME(String TIPEINCOME) {
        this.TIPEINCOME = TIPEINCOME;
    }

    /**
     * @return The PERHARI
     */
    public String getPERHARI() {
        return PERHARI;
    }

    /**
     * @param PERHARI The PER_HARI
     */
    public void setPERHARI(String PERHARI) {
        this.PERHARI = PERHARI;
    }

    /**
     * @return The PERBULAN
     */
    public String getPERBULAN() {
        return PERBULAN;
    }

    /**
     * @param PERBULAN The PER_BULAN
     */
    public void setPERBULAN(String PERBULAN) {
        this.PERBULAN = PERBULAN;
    }

    /**
     * @return The PERTAHUN
     */
    public String getPERTAHUN() {
        return PERTAHUN;
    }

    /**
     * @param PERTAHUN The PER_TAHUN
     */
    public void setPERTAHUN(String PERTAHUN) {
        this.PERTAHUN = PERTAHUN;
    }

    /**
     * @return The CASHFLOW
     */
    public String getCASHFLOW() {
        return CASHFLOW;
    }

    /**
     * @param CASHFLOW The CASH_FLOW
     */
    public void setCASHFLOW(String CASHFLOW) {
        this.CASHFLOW = CASHFLOW;
    }

    /**
     * @return The REFLEKSIKEUANGAN
     */
    public String getREFLEKSIKEUANGAN() {
        return REFLEKSIKEUANGAN;
    }

    /**
     * @param REFLEKSIKEUANGAN The REFLEKSI_KEUANGAN
     */
    public void setREFLEKSIKEUANGAN(String REFLEKSIKEUANGAN) {
        this.REFLEKSIKEUANGAN = REFLEKSIKEUANGAN;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(APREGNO);
        dest.writeValue(CUREF);
        dest.writeValue(SVDATE);
        dest.writeValue(SVNAME);
        dest.writeValue(SVTUJUAN);
        dest.writeValue(SVNASABAH);
        dest.writeValue(SVBANK);
        dest.writeValue(SVOFFICE);
        dest.writeValue(SVFACTORY);
        dest.writeValue(SVMANAGEMENT);
        dest.writeValue(SVPRODUKSI);
        dest.writeValue(SVPEMASARAN);
        dest.writeValue(SVKEUANGAN);
        dest.writeValue(SVAGUNAN);
        dest.writeValue(SVPERSOALAN);
        dest.writeValue(TGDATE);
        dest.writeValue(SVTARGETDATE);
        dest.writeValue(SVHSINVESTIGASIDATE);
        dest.writeValue(SVHSPEMBERIKET1);
        dest.writeValue(SVHSHUBPEMBERIKET1);
        dest.writeValue(SVHSPEMBERIKET2);
        dest.writeValue(SVHSHUBPEMBERIKET2);
        dest.writeValue(SVHSNMPEMOHON1);
        dest.writeValue(SVHSNMPEMOHON2);
        dest.writeValue(SVHSNMPEMOHON3);
        dest.writeValue(SVHSBIRTHDATE);
        dest.writeValue(SVHSSEXTYP);
        dest.writeValue(SVHSEMAIL);
        dest.writeValue(SVHSADDRKTP1);
        dest.writeValue(SVHSADDRKTP2);
        dest.writeValue(SVHSADDRKTP3);
        dest.writeValue(SVHSZIPKTP);
        dest.writeValue(SVHSCITYCODEKTP);
        dest.writeValue(SVHSCITYKTP);
        dest.writeValue(SVHSADDR1);
        dest.writeValue(SVHSADDR2);
        dest.writeValue(SVHSADDR3);
        dest.writeValue(SVHSZIP);
        dest.writeValue(SVHSCITYCODE);
        dest.writeValue(SVHSCITY);
        dest.writeValue(SVHSNOTLPAREA);
        dest.writeValue(SVHSNOTLP);
        dest.writeValue(SVHSNOHPAREA);
        dest.writeValue(SVHSNOHP);
        dest.writeValue(SVHSMARRIAGE);
        dest.writeValue(SVHSCOUPLENM1);
        dest.writeValue(SVHSCOUPLENM2);
        dest.writeValue(SVHSCOUPLENM3);
        dest.writeValue(SVHSCOUPLEJOB);
        dest.writeValue(SVHSMOTHERNM1);
        dest.writeValue(SVHSMOTHERNM2);
        dest.writeValue(SVHSMOTHERNM3);
        dest.writeValue(SVHSJMLTERTANGGUNG);
        dest.writeValue(SVHSDEBITURTYP);
        dest.writeValue(SVHSADDRPLUS1);
        dest.writeValue(SVHSADDRPLUS2);
        dest.writeValue(SVHSADDRPLUS3);
        dest.writeValue(SVHSZIPPLUS);
        dest.writeValue(SVHSCITYCODEPLUS);
        dest.writeValue(SVHSCITYPLUS);
        dest.writeValue(SVHSNOTLPAREAPLUS);
        dest.writeValue(SVHSNOTLPPLUS);
        dest.writeValue(SVHSNOHPAREAPLUS);
        dest.writeValue(SVHSNOHPPLUS);
        dest.writeValue(SVHSNMFAMS1);
        dest.writeValue(SVHSNMFAMS2);
        dest.writeValue(SVHSNMFAMS3);
        dest.writeValue(SVHSADDRFAMS1);
        dest.writeValue(SVHSADDRFAMS2);
        dest.writeValue(SVHSADDRFAMS3);
        dest.writeValue(SVHSZIPFAMS);
        dest.writeValue(SVHSCITYCODEFAMS);
        dest.writeValue(SVHSCITYFAMS);
        dest.writeValue(SVHSNOTLPAREAFAMS);
        dest.writeValue(SVHSNOTLPFAMS);
        dest.writeValue(SVHSNOOFFICEAREAFAMS);
        dest.writeValue(SVHSNOOFFICEFAMS);
        dest.writeValue(SVHSNOOFFICEEXTFAMS);
        dest.writeValue(SVHSNOHPAREAFAMS);
        dest.writeValue(SVHSNOHPFAMS);
        dest.writeValue(SVHSHUBFAMS);
        dest.writeValue(SVHSSTSHOMESTAY);
        dest.writeValue(SVHSCEKARSIPSTAY);
        dest.writeValue(SVHSAGUNANSTAY);
        dest.writeValue(SVHSDAYSTAY);
        dest.writeValue(SVHSMONTHSTAY);
        dest.writeValue(SVHSBANGUNANTYPESTAY);
        dest.writeValue(SVHSBANGUNANLOKASISTAY);
        dest.writeValue(SVHSBANGUNANCONDSTAY);
        dest.writeValue(SVHSFASILITASSTAY);
        dest.writeValue(SVHSBARANGHOMESTAY);
        dest.writeValue(SVHSAKSESROADSTAY);
        dest.writeValue(SVHSLINGKUNGANSTAY);
        dest.writeValue(SVHSLUASTANAHSTAY);
        dest.writeValue(SVHSLUASBANGUNANSTAY);
        dest.writeValue(SVHSGARASISTAY);
        dest.writeValue(SVHSCARPORTSTAY);
        dest.writeValue(SVHSVEHICLESTAY);
        dest.writeValue(SVHSVEHICLETYPESTAY);
        dest.writeValue(SVHSVEHICLECOUNTSTAY);
        dest.writeValue(SVHSVEHICLECONDSTAY);
        dest.writeValue(SVHSKETERANGANSTAY);
        dest.writeValue(SVOFCPEMBERIKET1);
        dest.writeValue(SVOFCPOSISIPEMBERIKET1);
        dest.writeValue(SVOFCPEMBERIKET2);
        dest.writeValue(SVOFCPOSISIPEMBERIKET2);
        dest.writeValue(SVOFCTYPOFFICE);
        dest.writeValue(SVOFCNMOFFICE);
        dest.writeValue(SVOFCADDROFFICE1);
        dest.writeValue(SVOFCADDROFFICE2);
        dest.writeValue(SVOFCADDROFFICE3);
        dest.writeValue(SVOFCZIPCODEOFFICE);
        dest.writeValue(SVOFCCITYCODEOFFICE);
        dest.writeValue(SVOFCCITYOFFICE);
        dest.writeValue(SVOFCN0TLPAREAOFFICE);
        dest.writeValue(SVOFCN0TLPOFFICE);
        dest.writeValue(SVOFCEXTOFFICE);
        dest.writeValue(SVOFCN0FAXAREAOFFICE);
        dest.writeValue(SVOFCN0FAXOFFICE);
        dest.writeValue(SVOFCYEAROFFICE);
        dest.writeValue(SVOFCUSAHAOFFICE);
        dest.writeValue(SVOFCSTAFOFFICE);
        dest.writeValue(SVOFCSCALEOFFICE);
        dest.writeValue(SVOFCBANGUNANOFFICE);
        dest.writeValue(SVOFCLOKASIOFFICE);
        dest.writeValue(SVOFCKONDISIOFFICE);
        dest.writeValue(SVOFCOWNEROFFICE);
        dest.writeValue(SVOFCINVESTIGASIDATE);
        dest.writeValue(SVOFCTYPOFFICEPLUS);
        dest.writeValue(SVOFCNMOFFICEPLUS);
        dest.writeValue(SVOFCPOSISIPEMOHONPLUS);
        dest.writeValue(SVOFCNOTLPAREAPLUS);
        dest.writeValue(SVOFCNOTLPPLUS);
        dest.writeValue(SVOFCNOFAXAREAPLUS);
        dest.writeValue(SVOFCNOFAXPLUS);
        dest.writeValue(SVOFCADDRPLUS1);
        dest.writeValue(SVOFCADDRPLUS2);
        dest.writeValue(SVOFCADDRPLUS3);
        dest.writeValue(SVOFCZIPCODEPLUS);
        dest.writeValue(SVOFCCITYCODEPLUS);
        dest.writeValue(SVOFCCITYPLUS);
        dest.writeValue(SVOFCNAMEWORK);
        dest.writeValue(SVOFCPOSISIWORK);
        dest.writeValue(SVOFCYEARWORK);
        dest.writeValue(SVOFCMONTHWORK);
        dest.writeValue(SVOFCSTATUSWORK);
        dest.writeValue(SVOFCUNITWORK);
        dest.writeValue(SVOFCKINERJAWORK);
        dest.writeValue(SVOFCOFFICENMHISTORY);
        dest.writeValue(SVOFCNOTLPAREAHISTORY);
        dest.writeValue(SVOFCNOTLPHISTORY);
        dest.writeValue(SVOFCOFFICEYEARHISTORY);
        dest.writeValue(SVOFCOFFICEMONTHHISTORY);
        dest.writeValue(SVOFCINCOMEBRUTOPEMOHON);
        dest.writeValue(SVOFCINCOMENETTOPEMOHON);
        dest.writeValue(SVOFCOTHERINCOMEPEMOHON);
        dest.writeValue(SVOFCTOTALINCOMEPEMOHON);
        dest.writeValue(SVOFCPAYPEMOHON);
        dest.writeValue(SVOFCINCOMEBRUTOSPOUSE);
        dest.writeValue(SVOFCINCOMENETTOSPOUSE);
        dest.writeValue(SVOFCOTHERINCOMESPOUSE);
        dest.writeValue(SVOFCTOTALINCOMESPOUSE);
        dest.writeValue(SVOFCPAYSPOUSE);
        dest.writeValue(SVOFCINCOMEMARGIN);
        dest.writeValue(SVOFCPEMOHONSUMMARY);
        dest.writeValue(SVOFCOTHERSUMMARY);
        dest.writeValue(TIPEINCOME);
        dest.writeValue(PERHARI);
        dest.writeValue(PERBULAN);
        dest.writeValue(PERTAHUN);
        dest.writeValue(CASHFLOW);
        dest.writeValue(REFLEKSIKEUANGAN);
    }

    public int describeContents() {
        return 0;
    }

}
