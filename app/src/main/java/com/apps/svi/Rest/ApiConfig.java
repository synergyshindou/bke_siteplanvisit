package com.apps.svi.Rest;

import com.apps.svi.Model.CompanySiteVisitEntry;
import com.apps.svi.Model.GlobalResponse;
import com.apps.svi.Model.HomeStayResponse;
import com.apps.svi.Model.LoginEntry;
import com.apps.svi.Model.LoginResponse;
import com.apps.svi.Model.NasabahEntry;
import com.apps.svi.Model.PersonnalSiteVisitEntry;
import com.apps.svi.Model.RegisterEntry;
import com.apps.svi.Model.SearchApplicationResponse;
import com.apps.svi.Model.SearchNasabahResponse;
import com.apps.svi.Model.SearchSiteVisitResponse;
import com.apps.svi.Model.UploadImageEntry;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;


public interface ApiConfig {

    @POST("nasabah/searchnasabah")
    Call<SearchNasabahResponse> searchNasabah(@Body NasabahEntry nasabahEntry);

    @POST("sitevisit/companysitevisit")
    Call<GlobalResponse> applyCompany(@Body CompanySiteVisitEntry companySiteVisitEntry);

    @POST("sitevisit/personalsitevisit")
    Call<GlobalResponse> applyPersonnal(@Body PersonnalSiteVisitEntry personnalSiteVisitEntry);

    @GET("sitevisit/customersitevisit")
    Call<SearchSiteVisitResponse> searchSiteVisit(@Query("CU_REF") String curef);

    @GET("application/searchapplication")
    Call<SearchApplicationResponse> searchApplication(@Query("CU_REF") String curef);

    @GET("homestay/retreivehomestay")
    Call<HomeStayResponse> getHomestay();

    @POST("account/registeraccount")
    Call<GlobalResponse> registerAccount(@Body RegisterEntry registerEntry);

    @Multipart
    @POST("appraisal/appraisalfileupload")
    Call<GlobalResponse> uploadImage(@Part("AP_REGNO") RequestBody AP_REGNO,
                                     @Part("CU_REF") RequestBody CU_REF,
                                     @Part MultipartBody.Part image);


    @POST("docupload/docfileupload")
    Call<GlobalResponse> uploadImageb64(@Body UploadImageEntry uploadImageEntry);

    @POST("account/login")
    Call<LoginResponse> login(@Body LoginEntry loginEntry);

}