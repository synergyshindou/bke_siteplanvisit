package com.apps.svi.Fragment;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.apps.svi.Adapter.AdapterNasabah;
import com.apps.svi.Adapter.AdapterSiteVisit;
import com.apps.svi.FormVisitActivity;
import com.apps.svi.MainActivity;
import com.apps.svi.Model.DataNasabah;
import com.apps.svi.Model.DataSiteVisit;
import com.apps.svi.Model.SearchNasabahResponse;
import com.apps.svi.Model.SearchSiteVisitResponse;
import com.apps.svi.R;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.w3c.dom.Text;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
@SuppressLint("ValidFragment")
public class ListSiteVisitFragment extends Fragment {

    private static final String TAG = "ListNasabahFragment";

    private View root_view;

    RecyclerView recyclerView;

    public DataNasabah dataNasabah;

    MainActivity activity;

    ProgressBar progressBar;

    TextView tvNodata;

    Button btnAddSiteVisit;

    public ListSiteVisitFragment() {

    }

    public ListSiteVisitFragment(DataNasabah dataNasabahs) {
        // Required empty public constructor
        dataNasabah = dataNasabahs;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root_view = inflater.inflate(R.layout.fragment_list_site_visit, container, false);
        initToolbar("List Site Visit");
        initView(root_view);

        activity = (MainActivity) getActivity();

        getSiteVisit(dataNasabah.getCUREF());

        return root_view;
    }

    void initToolbar(String title) {
        TextView tv = root_view.findViewById(R.id.tvTitle);
        tv.setText(title);
        TextView tvN = root_view.findViewById(R.id.tvName);
        if (dataNasabah != null)
            tvN.setText(dataNasabah.getNAMAPEMOHON());
        tvN.setVisibility(View.VISIBLE);
    }

    void initView(View v) {
        recyclerView = v.findViewById(R.id.rvSiteVisit);
        progressBar = v.findViewById(R.id.d_progress);
        tvNodata = v.findViewById(R.id.tvNodata);
        btnAddSiteVisit = v.findViewById(R.id.btnLihats);
        btnAddSiteVisit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, FormVisitActivity.class);
                intent.putExtra("CUREF", dataNasabah.getCUREF());
                intent.putExtra("TYPE", dataNasabah.getCUSTTYPE());
                activity.startActivityForResult(intent, 10);
            }
        });
    }

    public void getSiteVisit(String v) {
        showProgress(true);
        activity.restAPI.searchSiteVisit(v).enqueue(new Callback<SearchSiteVisitResponse>() {
            @Override
            public void onResponse(Call<SearchSiteVisitResponse> call, Response<SearchSiteVisitResponse> response) {
                Log.i(TAG, "onResponse: H " + response.headers().toString());
                Log.i(TAG, "onResponse: M " + response.message());
                if (response.code() == 400) {
                    Gson gson = new GsonBuilder().create();
                    try {

                        showProgress(false);
                        SearchNasabahResponse mError = gson.fromJson(response.errorBody().string(), SearchNasabahResponse.class);
                        if (mError.getResponseText() != null) {
                            activity.DoDialog(mError.getResponseText());
                        } else
                            activity.DoDialog("Koneksi bermasalah, silahkan coba lagi");
                        Log.v("Error code 400", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                        showProgress(false);
                    }
                } else if (response.code() == 200) {
                    if (response.body() != null) {
                        Log.i(TAG, "onResponse: B " + response.body().toString());
                        if (response.body().getResponse().equals("1")) {
//                                DoDialog(response.body().getResponseText()+"\n"+response.body().getDataNasabah().get(0).getNAMAPEMOHON());
//                            showDialogFullscreen(response.body().getDataNasabah());
                            if (response.body().getDataSiteVisit() == null) {
                                showProgressNull(false);
                            } else {
                                showProgress(false);
                                tvNodata.setVisibility(View.GONE);
                                recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                                AdapterSiteVisit _adapter = new AdapterSiteVisit(getActivity(), response.body().getDataSiteVisit());
                                recyclerView.setAdapter(_adapter);
                                _adapter.setOnItemClickListener(new AdapterSiteVisit.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(View view, DataSiteVisit obj, int position) {

                                    }
                                });
                            }
                        } else {
                            activity.DoDialog(response.body().getResponseText());
                        }
                    } else
                        activity.DoDialog("Koneksi bermasalah, silahkan coba lagi");
                } else {
                    showProgress(false);
                }
            }

            @Override
            public void onFailure(Call<SearchSiteVisitResponse> call, Throwable t) {

                Log.e(TAG, "onFailure: " + t.getMessage());
                activity.DoDialog("Tidak terhubung dengan internet, silahkan coba lagi.");
            }
        });
    }

    void showProgress(boolean shows) {
        recyclerView.setVisibility(shows ? View.GONE : View.VISIBLE);
        progressBar.setVisibility(shows ? View.VISIBLE : View.GONE);
    }

    void showProgressNull(boolean shows) {
        tvNodata.setVisibility(shows ? View.GONE : View.VISIBLE);
        progressBar.setVisibility(shows ? View.VISIBLE : View.GONE);
    }
}
