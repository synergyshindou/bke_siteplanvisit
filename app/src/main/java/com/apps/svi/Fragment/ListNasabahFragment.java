package com.apps.svi.Fragment;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.apps.svi.Adapter.AdapterNasabah;
import com.apps.svi.Model.DataNasabah;
import com.apps.svi.R;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
@SuppressLint("ValidFragment")
public class ListNasabahFragment extends DialogFragment {

    private static final String TAG = "ListNasabahFragment";
    RecyclerView recyclerView;
    List<DataNasabah> dataNasabah;
    private View root_view;

    public ListNasabahFragment() {
        // Required empty public constructor
    }

    public ListNasabahFragment(List<DataNasabah> dataNasabahz) {
        // Required empty public constructor
        dataNasabah = dataNasabahz;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root_view = inflater.inflate(R.layout.fragment_list_nasabah, container, false);

        initToolbar("List Nasabah");
        initView(root_view);

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        AdapterNasabah _adapter = new AdapterNasabah(getActivity(), dataNasabah);
        recyclerView.setAdapter(_adapter);
        _adapter.setOnItemClickListener(new AdapterNasabah.OnItemClickListener() {
            @Override
            public void onItemClick(View view, DataNasabah obj, int position) {
                showDialogFullscreen(obj);
            }
        });

        return root_view;
    }

    void initToolbar(String title) {
        TextView tv = root_view.findViewById(R.id.tvTitle);
        tv.setText(title);
    }

    void initView(View v) {
        recyclerView = v.findViewById(R.id.rvNasabah);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    private void showDialogFullscreen(DataNasabah d) {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        ListSiteVisitFragment newFragment = new ListSiteVisitFragment(d);
//        newFragment.setRequestCode(DIALOG_QUEST_CODE);
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.add(android.R.id.content, newFragment, "LIST").addToBackStack(null).commit();

    }
}
